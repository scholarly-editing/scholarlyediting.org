const path = require(`path`)
const fs = require(`fs`)
const glob = require(`glob`)
const {slugify} = require(`./src/utils/slugify`)
const webvtt = require('node-webvtt')
const { createRemoteFileNode } = require(`gatsby-source-filesystem`);
const {microEditions} = require("./gatsby-config")

exports.createSchemaCustomization = ({ actions }) => {
  const { createTypes } = actions
  // Explicitly defining types that need to be optional.
  const typeDefs = `
    type MarkdownRemark implements Node {
      frontmatter: Frontmatter
    }
    type Frontmatter {
      doi: String
    }
  `
  createTypes(typeDefs)
}

exports.sourceNodes = ({ actions, createNodeId, createContentDigest }) => {
  const { createNode } = actions
  
  // Retrieve microeditions metadata and store as nodes
  try {
    const files = glob.sync("static/issues/*/*/metadata.json")
    for (const file of files) {
      const metadata = fs.readFileSync(file, {encoding:'utf8'})
        const nodeContent = metadata
        const data = JSON.parse(metadata)
        data.path = file.match(/\/([^\/]+)\/metadata\.json/)[1]
      
        const nodeMeta = {
          id: createNodeId(data.doi), 
          parent: null,
          children: [],
          internal: {
            type: `MicroEditionMeta`,
            content: nodeContent,
            contentDigest: createContentDigest(data)
          }
        }
      
        const node = Object.assign({}, data, nodeMeta)
        createNode(node)
    }
  } catch (error) {
    throw 'Could not locate or parse microedition metadata files.'
  }

  // Store vtt files as nodes
  try {
    const files = glob.sync("static/audio/*.vtt")
    for (const file of files) {
      const vttdata = fs.readFileSync(file, {encoding:'utf8'})
        const nodeContent = vttdata
        const data = webvtt.parse(vttdata)
        data.filename = file.replace(/static\/audio\/(.*?.vtt)/, "$1")

        // copy start and end to starTime and endTime
        data.cues.forEach(cue => {
          cue.startTime = cue.start
          cue.endTime = cue.end
        })
      
        const nodeMeta = {
          id: createNodeId(data.filename),
          parent: null,
          children: [],
          internal: {
            type: `WebVTT`,
            content: nodeContent,
            contentDigest: createContentDigest(data)
          }
        }
      
        const node = Object.assign({}, data, nodeMeta)
        createNode(node)
    }
  } catch (error) {
    throw 'Could not locate or parse webvtt files (webvtt may not be valid).'
  }
}

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage, createRedirect } = actions
  await makePages(createPage, graphql)
  await makeIssueTocs(createPage, graphql)
  await makeIssueContents(createPage, graphql)

  // redirects
  createRedirect({
    fromPath: `/editors_call`,
    toPath: `/editorscall`,
    isPermanent: true,
  })
}

async function makePages(createPage, graphql, reporter) {
  const homePageTemplate = path.resolve(`src/templates/homePage.tsx`)
  const pageTemplate = path.resolve(`src/templates/pageTemplate.tsx`)

  const result = await graphql(`
    query {
      allFile(
        filter: {sourceInstanceName: {eq: "pages"}}
      ) {
        nodes {
          childMarkdownRemark {
            frontmatter {
                path
              }
          }
          mtime
        }
      }
      site {
        siteMetadata {
          issn
          description,
          doiPrefix,
          doiSuffix
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }

  const {issn, description, doiPrefix, doiSuffix} = result.data.site.siteMetadata

  for (const node of result.data.allFile.nodes) {
    // Home page has its own template.
    if (node.childMarkdownRemark.frontmatter.path === "/") {
      createPage({
        path: node.childMarkdownRemark.frontmatter.path,
        component: homePageTemplate,
        context: {
          issn,
          description,
          doi: `${doiPrefix}/${doiSuffix}`,
          modifiedTime: node.mtime
        }
      })
    } else {
      // Other pages.
      createPage({
        path: node.childMarkdownRemark.frontmatter.path,
        component: pageTemplate,
        context: {
          issn,
          description,
          doi: `${doiPrefix}/${doiSuffix}`,
          modifiedTime: node.mtime
        }
      })
    }
  }
}

async function makeIssueTocs(createPage, graphql, reporter) {
  const pageTemplate = path.resolve(`src/templates/issueToc.tsx`)

  const result = await graphql(`
    {
      allFile(filter: {sourceInstanceName: {eq: "issues"}, extension: {eq: "md"}}) {
        group(field: {relativeDirectory: SELECT}) {
          nodes {
            childMarkdownRemark {
              frontmatter {
                issue
                group
                group_order
                authors{
                  first
                  middle
                  last
                  affiliations
                }
                subtitle
                title
              }
            }
          }
          fieldValue
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  result.data.allFile.group.forEach((group) => {
    const issueName = group.nodes[0].childMarkdownRemark.frontmatter.issue
    const toc = group.nodes.reduce((acc, node) => {
      const {group, group_order, title, subtitle, authors} = node.childMarkdownRemark.frontmatter

      if (!acc[group]) {
        acc[group] = []
      }

      acc[group].push({
        group_order,
        title,
        subtitle,
        authors,
        path: slugify(title)
      })

      return acc
      
    }, {})
    
    Object.keys(toc).map(group => {
      toc[group].sort((a, b) => (a.group_order > b.group_order) ? 1 : -1)
    })

    createPage({
      path: `issues/${group.fieldValue}/`,
      component: pageTemplate,
      context: {
        toc,
        issueName
      },
    })
  })
}

async function makeIssueContents(createPage, graphql, reporter) {
  const pageTemplate = path.resolve(`src/templates/contentTemplate.tsx`)

  const result = await graphql(`
    query {
      allFile(
        filter: {sourceInstanceName: {eq: "issues"}, extension: {eq: "md"}}
      ) {
        nodes {
          childMarkdownRemark {
            frontmatter {
              issue
              title
              subtitle
              group
              authors {
                first
                middle
                last
                affiliations
                orcid
              }
              doi
              keywords
              abstract
              audio {
                file
                transcript
                chapters
              }
            }
            html
          }
          mtime
          relativeDirectory
        }
      }
      orcid: allFile(filter: {relativePath: {eq: "orcid.png"}}) {
        nodes {
          childImageSharp {
            gatsbyImageData(width: 16)
          }
        }
      }
      site {
        siteMetadata {
          issn
        }
      }
      allIssuesJson {
        nodes {
          date
          issueDir
        }
      }
    }
  `)
  if (result.errors) {
    console.log(result.errors)
    reporter.panicOnBuild(`Error while running GraphQL query.`)
    return
  }
  result.data.allFile.nodes.forEach((node) => {
    const {issue, title, subtitle, authors, doi, group, keywords, abstract, audio} = node.childMarkdownRemark.frontmatter
    const slug = slugify(title)
    createPage({
      path: `issues/${node.relativeDirectory}/${slug}`,
      component: pageTemplate,
      context: {
        slug,
        title,
        subtitle,
        authors,
        issue,
        issueDate: result.data.allIssuesJson.nodes.filter(i => i.issueDir === node.relativeDirectory)[0].date,
        doi,
        group,
        keywords,
        abstract,
        issuePath: node.relativeDirectory,
        html: node.childMarkdownRemark.html,
        modifiedTime: node.mtime,
        orcidImg: result.data.orcid.nodes[0].childImageSharp.gatsbyImageData,
        issn: result.data.site.siteMetadata.issn,
        audio
      },
    })
  })

}

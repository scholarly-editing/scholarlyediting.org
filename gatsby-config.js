module.exports = {
  pathPrefix: process.env.PATH_PREFIX || "",
  siteMetadata: {
    version: process.env.npm_package_version,
    doiPrefix: `10.55520`,
    doiSuffix: `6ZH06EW2`,
    issn: `2167-1257`,
    title: `Scholarly Editing`,
    subtitle: `The Annual of the Association for Documentary Editing`,
    description: `Scholarly Editing is the annual of the Association for Documentary Editing`,
    author: `Scholarly Editing Editors`,
    menuLinks: [
      {
        name: 'home',
        link: '/'
      },
      {
        name: 'issues',
        link: '/issues'
      },
      {
        name: 'about',
        link: '/about'
      },
      {
        name: 'contributing',
        link: '/contributing'
      },
      {
        name: 'rolling call',
        link: '/call'
      },
      {
        name: 'editors call',
        link: '/editorscall'
      },
    ]
  },
  plugins: [
    {
      resolve: `se-microeditions-metadata`,
      options: {
        microeditions: [
          "a-prototype-for-a-digital-edition-of-antonio-de-leon-pinelos-epitome-de-la-biblioteca-oriental-y-occidental-nautica-y-geografica-1629",
          "john-broughams-columbus-burlesque",
          "kinship-and-longing",
          "mabel-dodge-luhans-whirling-around-mexico-a-selection",
          "marm-the-doctor-mill-children-and-the-american-dream-in-one-way-to-get-an-education",
          "paper-bullets",
          "selections-from-the-revue-des-colonies-july-1834-and-july-1835"
        ],
        basePath: "https://archive.scholarlyediting.org/micro-editions/"
      }
    },
    `gatsby-plugin-image`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Scholarly Editing`,
        short_name: `Scholarly Editing`,
        start_url: `/`,
        icon: `src/images/se-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `pages`,
        path: `${__dirname}/src/contents/pages`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `issues`,
        path: `${__dirname}/src/contents/issues`,
      },
    },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          `gatsby-remark-autolink-headers`,
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 1000,
              linkImagesToOriginal: false,
              showCaptions: true,
              markdownCaptions: true,
              quality: 100
            },
          },
          `gatsby-accessible-remark-footnotes`
        ],
      },
    },
    "gatsby-transformer-json",
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-174640285-1`,
        anonymize: true,
      },
    },
    {
      resolve: `gatsby-plugin-google-gtag`,
      options: {
        trackingIds: [
          "UA-174640285-1"
        ],
        gtagConfig: {
          anonymize_ip: true,
          cookie_expires: 0,
        },
        pluginConfig: {
          // Puts tracking script in the head instead of the body
          head: false,
          // Setting this parameter is also optional
          respectDNT: true,
        },
      },
    },
    `gatsby-plugin-client-side-redirect` // keep it in last in list
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.dev/offline
    // `gatsby-plugin-offline`,
  ],
}

import React from 'react'
import Transcript from './transcript'
import { Box, Grid, Typography, Skeleton, Switch } from '@mui/material'
import useMediaQuery from "@mui/material/useMediaQuery"
import { graphql, useStaticQuery } from 'gatsby'
import theme from '../../theme'

interface Props {
  audio: string
  transcript: string
  chapters?: string
  preload?: boolean
  query?: string
}

export interface WebVttCue extends TextTrackCue {
  text: string
  title?: string
}

interface WebVttNode {
  cues: {
    text: string
    startTime: string
    endTime: string
    identifier: string
    styles: string
  }
  filename : string
}

const Player = ({ audio, transcript, chapters, preload }: Props) => {
  const [isClient, setClient] = React.useState(false);
  const key = isClient ? "client" : "server";
  // Load the transcript file for SSG and SEO
  const vtts = useStaticQuery(graphql`
    query vtts {
      allWebVtt {
        nodes {
          cues {
            text
            startTime
            endTime
            identifier
            styles
          }
          filename
        }
      }
    }
  `)
  const transcriptData = vtts.allWebVtt.nodes.filter((n: WebVttNode) => n.filename === transcript)[0]
  const chapterData = chapters ? vtts.allWebVtt.nodes.filter((n: WebVttNode) => n.filename === chapters)[0] : null

  const [loaded, setLoaded] = React.useState(false)
  const [waiting, setWaiting] = React.useState(false)
  const [showTimeStamps, setShowTimeStamps] = React.useState(false)

  const trackRef = React.useRef<HTMLTrackElement>(null)
  const chTrackRef = React.useRef<HTMLTrackElement>(null)
  const audioRef = React.useRef<HTMLAudioElement>(null)

  const isScreenSmall = useMediaQuery(theme.breakpoints.down('md'))

  const checkIfLoaded = (tries = 0) => {
    tries += 1
    // check all tracks (TODO: optimize if more tracks are needed)
    const e = trackRef.current as unknown as HTMLTrackElement
    const ce = chTrackRef.current as unknown as HTMLTrackElement
    if (e && e.track && e.track.cues && e.track.cues.length > 0
      && ce && ce.track && ce.track.cues && ce.track.cues.length > 0
      ) {
      setLoaded(true)
    } else if (!loaded) {
      const wait = 25 * Math.pow(tries, 2)
      setTimeout(checkIfLoaded, wait, tries)
    }
  }

  const seek = (secs: number, autoplay = true) => {
    if (audioRef.current) {
      const a = audioRef.current
      a.play() // NB! This is necessary to make sure the seek is consistent and successful.
      a.currentTime = secs
      if (!autoplay) {
        a.pause()
      }
    }
  }

  const handleWaiting = () => {
    setWaiting(true)
  }

  const handlePlaying = () => {
    setWaiting(false)
  }

  React.useEffect(() => {
    setClient(true);
    if (audioRef.current) {
      audioRef.current.addEventListener('waiting', handleWaiting)
    }
    if (audioRef.current) {
      audioRef.current.addEventListener('playing', handlePlaying)
    }
    checkIfLoaded()
    return () => {
      if (audioRef.current) {
        audioRef.current.removeEventListener('waiting', handleWaiting)
        audioRef.current.removeEventListener('playing', handlePlaying)
      }
    }
  }, [])

  const handleHashChange = () => {
    if (loaded && chapters && window.location.hash && window.location.hash !== "#pagecontent") {
      const el = document.getElementById(window.location.hash.substring(1))
      if (el) {
        el.scrollIntoView({ behavior: "smooth", inline: "nearest", block: "center"})
      }
      const chaptersTrack = (chTrackRef.current as unknown as HTMLTrackElement).track
      const cue = chaptersTrack.cues?.getCueById(window.location.hash.substring(1).toLowerCase())
      if (cue && cue.startTime) {
        seek(cue.startTime + 0.01, false)
      }
    }
  }

  React.useEffect(() => {
    // show hash location if present and set audio to that time
    window.addEventListener("hashchange", handleHashChange)
    if (loaded && chapters && window.location.hash) {
      handleHashChange()
    }
    return () => {
      window.removeEventListener("hashchange", handleHashChange)
    }
  }, [loaded])

  let transcriptEl = <>
    <Skeleton/><Skeleton/><Skeleton/>
    <Box sx={{display: "none"}}>
      <Transcript
        seek={seek}
        track={transcriptData}
        chapters={chapterData}
        showTimeStamps={showTimeStamps}
        />
    </Box>
  </>

  if (isClient && loaded) {
    transcriptEl = <Transcript
      seek={seek}
      track={(trackRef.current as unknown as HTMLTrackElement).track}
      chapters={(chTrackRef.current as unknown as HTMLTrackElement).track} 
      showTimeStamps={showTimeStamps} />
  }

  const transcriptBox = <Grid item xs={12} md={8}>
    {transcriptEl}
  </Grid>

  const stickyStyle = {position: "sticky", top: "0", paddingTop: "25px", zIndex: "99"}
  const audioBox = <Grid item xs={12} md={4} sx={Object.assign(isScreenSmall ? stickyStyle : {}, {backgroundColor: "#fff"})}>
    <Box sx={!isScreenSmall ? stickyStyle : {}}>
      <audio
        controls
        crossOrigin="anonymous"
        onLoad={() => setLoaded(true)}
        preload={preload ? "true" : "false"}
        ref={audioRef}>
        <source src={`/audio/${audio}`} />
        <track default
          kind="subtitles"
          src={`/audio/${transcript}`}
          ref={trackRef} />
        <track default
          kind="chapters"
          src={`/audio/${chapters}`}
          ref={chTrackRef} />
      </audio>
      <Grid container>
        <Grid item xs={2}><Switch checked={showTimeStamps} onChange={() => setShowTimeStamps(!showTimeStamps)}/></Grid>
        <Grid item xs={10}><Typography variant="body1" component="p" sx={{marginTop: "5px"}}>Show Timestamps</Typography></Grid>
      </Grid>
      {waiting && <Grid container>
        <Grid item xs={2}><Skeleton variant="circular"  width={40} height={40} sx={{display: "inline-block"}}/></Grid>
        <Grid item xs={10}><Typography variant="body1" component="p" sx={{marginTop: "5px"}}>Buffering...</Typography></Grid>
      </Grid>}
    </Box>
  </Grid>

  if ( !isClient ) return null;
  return (<div key={key}>
    <Grid container sx={{marginBottom: "2rem"}}>
      {isScreenSmall ? <>{audioBox}{transcriptBox}</> : <>{transcriptBox}{audioBox}</>}
    </Grid>
  </div>)
}


export default Player
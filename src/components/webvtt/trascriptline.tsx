import React from 'react'
import { Box, useMediaQuery } from '@mui/material'
import type { WebVttCue } from './player'

import theme from '../../theme'

interface Props {
  cue: WebVttCue
  seek: (secs: number) => void
  showTimeStamps: boolean
}

const TranscriptLine = ({ cue, seek, showTimeStamps }: Props) => {
  const [active, setActive] = React.useState(false)
  const isScreenSmall = useMediaQuery(theme.breakpoints.down('lg'))

  const cueEl = React.useRef<HTMLDivElement>(null)

  React.useEffect(() => {
    cue.onenter = () =>  setActive(true)
    cue.onexit = () => setActive(false)
  }, [])

  React.useEffect(() => {
    if (active) {
      if (cueEl && cueEl.current) {
        cueEl.current.scrollIntoView({ behavior: "smooth", inline: "nearest", block: "start"})
      }
    }
  }, [active])

  const handleClick = () => {
    seek(cue.startTime)
  }

  const startTime = () => {
    return formatSeconds(cue.startTime)
  }

  const endTime = () => {
    return formatSeconds(cue.endTime)
  }

  const formatSeconds = (t: number) => {
    const mins = Math.floor(t / 60)
    let minsString = mins.toString()
    if (mins < 10) {
      minsString = `0${mins}`
    }

    const secs = Math.floor(t % 60)
    let secsString = secs.toString()
    if (secs < 10) {
      secsString = `0${secs}`
    }

    return `${minsString}:${secsString}`
  }

  const floatStyle = {
    float: "left",
    marginLeft: "-8rem"
  }

  return <Box ref={cueEl} sx={{
    scrollMarginTop: isScreenSmall ? "145px" : "20px",
    backgroundColor: active ? theme.palette.secondary.main: "",
    marginRight: ".5rem",
    cursor: "pointer"
  }} onClick={handleClick} component="div">
    {showTimeStamps && <Box sx={Object.assign({fontStyle: "italic"}, !isScreenSmall ? floatStyle : {})}>
      [{startTime()} - {endTime()}]
    </Box>}
    <div
      dangerouslySetInnerHTML={{__html: cue.text}} />
  </Box>
}


export default TranscriptLine
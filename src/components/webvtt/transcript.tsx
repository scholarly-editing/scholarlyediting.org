import React from 'react'
import TranscriptLine from './trascriptline'
import Typography from '@mui/material/Typography'
import type { WebVttCue } from './player'
import slugify from 'slugify'

interface Props {
  track: TextTrack
  chapters?: TextTrack
  seek: (secs: number) => void,
  showTimeStamps: boolean
}

const Transcript = ({ track, chapters, seek, showTimeStamps }: Props) => {
  
  const locateChapter = (starttime: number) => {
    if (chapters && chapters.cues) {
      for (let i = 0; i < chapters.cues.length; i++) {
        const cue = chapters.cues[i] as WebVttCue
        if (cue.startTime === starttime) {
          return (<Typography key={`line-${i}-ch`} variant="h4" component="h2" id={slugify(cue.text)} sx={{position: "relative", marginTop: "1rem"}}>
            <a href={`#${slugify(cue.text)}`} aria-label={`${slugify(cue.text)} permalink`} className="anchor before"><svg aria-hidden="true" focusable="false" height="16" version="1.1" viewBox="0 0 16 16" width="16"><path fillRule="evenodd" d="M4 9h1v1H4c-1.5 0-3-1.69-3-3.5S2.55 3 4 3h4c1.45 0 3 1.69 3 3.5 0 1.41-.91 2.72-2 3.25V8.59c.58-.45 1-1.27 1-2.09C10 5.22 8.98 4 8 4H4c-.98 0-2 1.22-2 2.5S3 9 4 9zm9-3h-1v1h1c1 0 2 1.22 2 2.5S13.98 12 13 12H9c-.98 0-2-1.22-2-2.5 0-.83.42-1.64 1-2.09V6.25c-1.09.53-2 1.84-2 3.25C6 11.31 7.55 13 9 13h4c1.45 0 3-1.69 3-3.5S14.5 6 13 6z"></path></svg></a>
            {cue.text}
          </Typography>)
        }
      }
    }
    return null
  }


  const lines = []
  if (track && track.cues) {
    for (let i = 0; i < track.cues.length; i++) {
      const cue = track.cues[i] as WebVttCue
      // Add chapter if needed
      const ch = locateChapter(cue.startTime)
      if (ch) {
        lines.push(ch)
      }
      // Process line
      lines.push(
        <TranscriptLine
        showTimeStamps={showTimeStamps}
          key={`line-${i}`}
          cue={cue} 
          seek={seek} />
      )
    }
  }
  return (
    <div>
      {lines}
    </div>
  )
}


export default Transcript
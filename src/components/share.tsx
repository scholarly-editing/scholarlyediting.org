import * as React from 'react';
import ShareIcon from '@mui/icons-material/Share';
import FacebookIcon from '@mui/icons-material/Facebook';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import LinkIcon from '@mui/icons-material/Link';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Snackbar, { SnackbarCloseReason } from '@mui/material/Snackbar';
import { Button } from "@mui/material";

const BlueskySVG = () => (
  <svg width="15" height="15" viewBox="0 0 568 501" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M123.121 33.6637C188.241 82.5526 258.281 181.681 284 234.873C309.719 181.681 379.759 82.5526 444.879 33.6637C491.866 -1.61183 568 -28.9064 568 57.9464C568 75.2916 558.055 203.659 552.222 224.501C531.947 296.954 458.067 315.434 392.347 304.249C507.222 323.8 536.444 388.56 473.333 453.32C353.473 576.312 301.061 422.461 287.631 383.039C285.169 375.812 284.017 372.431 284 375.306C283.983 372.431 282.831 375.812 280.369 383.039C266.939 422.461 214.527 576.312 94.6667 453.32C31.5556 388.56 60.7778 323.8 175.653 304.249C109.933 315.434 36.0535 296.954 15.7778 224.501C9.94525 203.659 0 75.2916 0 57.9464C0 -28.9064 76.1345 -1.61183 123.121 33.6637Z" fill="black"/>
  </svg>
);

const Share = ({url}: {url: string}) => {
  const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
  const [snackbarOpen, snackbarSetOpen] = React.useState(false);
  const open = Boolean(anchorEl);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const handleCopyLink = () => {
    navigator.clipboard.writeText(url);
    snackbarSetOpen(true);
    setAnchorEl(null);
  };

  const handleCopyLinkClose = (
    event: React.SyntheticEvent | Event,
    reason?: SnackbarCloseReason,
  ) => {
    if (reason === 'clickaway') {
      return;
    }
    snackbarSetOpen(false);
  };

  const handleShare = (e: React.MouseEvent<SVGSVGElement, MouseEvent>, dest: string) => {
    e.preventDefault();
	  const encodedAhref = encodeURIComponent(url);
    let shareLink = "";

    if (dest === "fb") {
      shareLink = `https://www.facebook.com/sharer/sharer.php?u=${encodedAhref}`;
    } else if (dest === "li") {
      shareLink = `https://www.linkedin.com/shareArticle?url=${encodedAhref}&Source=${encodedAhref}`;
    } else if (dest === "bluesky") {
      const message = `Check this out: ${url}`; 
      const encodedMessage = encodeURIComponent(message);
      shareLink = `https://bsky.app/intent/compose?text=${encodedMessage}`;
    } 
    
    window.open(shareLink, "_blank")
  };
  return (
    <>
      <Button
        id="basic-button"
        aria-controls={open ? 'basic-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
        startIcon={<ShareIcon />}
        sx={{
          color: '#444',
          fontSize: '1rem',
          borderRadius: '8px', 
        }}
      >
        Share
      </Button>
      <Menu
        id="basic-menu"
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        MenuListProps={{
          'aria-labelledby': 'basic-button',
        }}
      >
        <MenuItem onClick={(e) => handleShare(e, "fb")} sx={{ display: 'flex', alignItems: 'center', gap: '8px' }}>
          <FacebookIcon fontSize="small"/> Facebook
        </MenuItem>
        <MenuItem onClick={(e) => handleShare(e, "li")} sx={{ display: 'flex', alignItems: 'center', gap: '8px' }}>
          <LinkedInIcon fontSize="small"/> LinkedIn
        </MenuItem>
        <MenuItem onClick={(e) => handleShare(e, "bluesky")} sx={{ display: 'flex', alignItems: 'center', gap: '12px', marginLeft: '1px' }}>
          <BlueskySVG /> BlueSky
        </MenuItem>
        <MenuItem onClick={(handleCopyLink)} sx={{ display: 'flex', alignItems: 'center', gap: '8px' }}>
          <LinkIcon fontSize="small"/> Copy Link
        </MenuItem>
      </Menu>

      <Snackbar
        open={snackbarOpen}
        autoHideDuration={3000}
        onClose={handleCopyLinkClose}
        message="Link copied to clickboard!"
        sx={{
        '& .MuiSnackbarContent-root': {
          background: 'rgba(220, 53, 34, 0.8)',
          fontWeight: 500,
          letterSpacing: '0.5px'
        },
        }}
      />
    </>
  );
};

export { BlueskySVG };
export default Share;
import React from "react"
import { navigate, withPrefix } from "gatsby"
import Grid from "@mui/material/Grid"
import Container from "@mui/material/Container"
import Button from "@mui/material/Button"
import MenuItem from "@mui/material/MenuItem"
import useMediaQuery from "@mui/material/useMediaQuery"

import theme from "../theme"
import DisplayOptionsMenu from "./displayOptionsMenu"

// Style
const styles = {
  nav: {
    "& .MuiGrid-item": {
      padding: "0 2.1rem 0 0",
    },
  },
  navBtn: {
    borderBottom: "3px solid transparent",
    borderBottomColor: "transparent",
    borderRadius: 0,
    boxShadow: "none",
    "&:hover, &:focus": {
      backgroundColor: "transparent",
      borderBottomColor: theme.palette.primary.main,
    },
  },
}

// Component

interface Links {
  name: string
  link: string
}

interface Props {
  location: string
  menuLinks: Links[]
}

const Layout = ({ location, menuLinks }: Props) => {
  const isScreenSmall = useMediaQuery(theme.breakpoints.down('md'))
  const isHome = location.toString() === withPrefix("/")

  const isActive = (link: string) => {
    // Issues needs to be active within issue pages.
    const loc = location.toString().replace(/\/+$/, "")
    // Issues needs to be active within issue pages.
    if (
      (isHome && link === "/") || 
      (link.includes('issues') && loc.includes('issues') ) ||
      loc.endsWith(link)
      ) {
        return true
      }
    return false
  }


  const handleNavClick = (here: boolean, dest: string) => {
    if (here) {
      return null
    }
    navigate(dest)
  }

  const makeMenuButton = (dest: string, label: string) => {
    const here = (location.toString() === "home" && isHome) || location.toString().replace(/\//g, '') === dest.replace(/\//g, '')
    return <MenuItem onClick={() => handleNavClick(false, dest)} sx={{
      fontWeight: "500",
      fontSize: "0.875rem",
      textTransform: "uppercase",
      paddingLeft: "1.5rem",
      color: here ? theme.palette.primary.main : 'inherit'
    }}>
      {label}
    </MenuItem>
  }

  const makeNavButton = (dest: string, label: string) => {
    const here = !dest || location.toString() === dest
    const active = {
      borderBottomColor: isActive(dest)
        ? theme.palette.primary.main
        : "transparent"
    }
    const buttonStyle = {...styles.navBtn, ...active}
    return (<Grid item={true} key={label} xs={6} sm="auto" md="auto" component="li">
      <Button
        color="default"
        size="large"
        sx={buttonStyle}
        aria-current={isActive(dest) ? "page" : "false"}
        onClick={() => handleNavClick(here, dest)}
      >
        {label}
      </Button>
    </Grid>)
  }

  const sections = isScreenSmall ? <DisplayOptionsMenu label="Menu" forceLabel color="default">
      <>{
        menuLinks.map(link => makeMenuButton(link.link, link.name))
      }</>      
    </DisplayOptionsMenu>
    : <>{
      menuLinks.map(link => makeNavButton(link.link, link.name))
    }</>

  return (
    <Container maxWidth="md" sx={styles.nav} component="nav">
      <Grid container={true} component="ul" sx={{
        listStyleType: "none",
        marginBlockStart: 0,
        marginBlockEnd: 0,
        paddingInlineStart: 0
      }}>
        {sections}
      </Grid>
    </Container>
  )
}

export default Layout

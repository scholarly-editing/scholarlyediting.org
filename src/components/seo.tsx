import React from "react"

interface Props {
  children: any
}

function SEO({ children }: Props) {
  return (
    <>
      <meta property="og:image" content="https://scholarlyediting.org/favicon-32x32.png" />
      <link
        href="https://fonts.googleapis.com/css2?family=EB+Garamond:ital,wght@0,400;0,500;1,400;1,500&display=swap"
        rel="stylesheet"/>
      {children}
    </>
  )
}

export default SEO

import React from "react"
 
interface Props {
  value: string
  numberOnly?: boolean
}

const Doi = ({value, numberOnly}: Props) => (
  <>{!numberOnly && 'DOI: '}<a href={`https://doi.org/${value}`}>{value}</a></>
)

export default Doi
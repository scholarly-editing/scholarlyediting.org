/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/use-static-query/
 */

import React from "react"
import { useStaticQuery, graphql } from "gatsby"
import CssBaseline from "@mui/material/CssBaseline"
import { ThemeProvider, StyledEngineProvider } from "@mui/material/styles"
import Grid from "@mui/material/Grid"
import Container from "@mui/material/Container"

import theme from "../theme"
import Header from "./header"
import { Button } from "@mui/material"

// import Head from "./head"

// Style
interface Style {
  [key: string]: React.CSSProperties | Style
}

const styles: Style = {
  main: {
    paddingBottom: "1.45rem",
    minHeight: "60vh",
    "& h2, & h3": {
      paddingBottom: "1rem",
    },
  },
  footer: {
    backgroundColor: "#efefef",
    padding: "1rem 0",
    borderTop: "1px solid #dadada",
  },
  logo: {
    textAlign: "right",
  },
  skip: {
    position: 'absolute',
    top: '134px',
    padding: '12px',
    zIndex: '99',
    backgroundColor: '#dc3522',
    color: 'white !important',
    border: '2px solid white',
    left: '12px',
    clip: 'rect(0 0 0 0)',
    "&:focus": {
      clip: 'unset'
    }
  }
}

// Component

interface Props {
  location?: string
  children?: any
}

const Layout = ({ location, children }: Props) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          version
          subtitle
          issn
          doiPrefix
          doiSuffix
          menuLinks {
            name
            link
          }
        }
      }
    }
  `)

  return (
    <StyledEngineProvider injectFirst>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <Button component="a" href="#pagecontent" sx={styles.skip}>Skip to content.</Button>
        <Header
          location={location || ""}
          siteTitle={data.site.siteMetadata.subtitle}
          menuLinks={data.site.siteMetadata.menuLinks}
        />
        <Container component="main" maxWidth="md" sx={styles.main} id="pagecontent">
          {children}
        </Container>
        <footer style={styles.footer}>
          <Container maxWidth="lg">
            <Grid container={true}>
              <Grid item={true} xs={9}>
                <a
                  rel="license"
                  href="http://creativecommons.org/licenses/by-nc-sa/3.0/"
                >
                  <img
                    alt="Creative Commons License"
                    src="http://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"
                  />
                </a>
                <br />
                This work is licensed under a{" "}
                <a
                  rel="license"
                  href="https://creativecommons.org/licenses/by-nc-sa/4.0/"
                >
                  Creative Commons Attribution-NonCommercial-ShareAlike 4.0
                  International License
                </a>
                . <br /> © {new Date().getFullYear()} Scholarly Editing.
                <br /> ISSN {data.site.siteMetadata.issn} | DOI{" "} 
                <a href={`https://doi.org/${data.site.siteMetadata.doiPrefix}/${data.site.siteMetadata.doiSuffix}`}>{
                  `${data.site.siteMetadata.doiPrefix}/${data.site.siteMetadata.doiSuffix}`
                }</a> | Site version {data.site.siteMetadata.version}
              </Grid>
              <Grid item={true} xs={3} sx={styles.logo}>
                <a href="http://www.documentaryediting.org">
                  <img
                    src="https://archive.scholarlyediting.org/se-archive/template_images/adelogo.png"
                    alt="Logo of the Association for Documentary Editing. Veritas ex documentis."
                  />
                </a>
              </Grid>
            </Grid>
          </Container>
        </footer>
      </ThemeProvider>
    </StyledEngineProvider>
  )
}

export default Layout

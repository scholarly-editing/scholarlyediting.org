export interface Author {
  first: string
  middle?: string
  last: string
  role?: string
  affiliations: string[]
  orcid?: string
}

export default function printAuthors(authors: Author[]) {
  return authors.map((a, i) => {
    const l = authors.length
    const c = l === 1 || i === 0 ? '' : i === l - 1 ? ' and ' : ', '
    return `${c}${a.first} ${a.middle || ''} ${a.last}`
  }).join('')
}
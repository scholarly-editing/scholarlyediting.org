// This is in CommonJS to allow import from gastby-node.js

exports.slugify = (text) => {
  return text.toLowerCase()
    .replace(/<[^>]+>/g, '') // remove html tags
    .replace(/ /g,'-') // spaces become -
    .replace(/-+/g, '-') // no repeated -
    .replace(/[^\w-]+/g,'') // remove all non word or - characters
}

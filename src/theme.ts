import { createTheme } from "@mui/material/styles"

// Extend colors and allow extensions on Button

declare module '@mui/material/styles' {
  interface Palette {
    default: Palette['primary'];
  }
  interface PaletteOptions {
    default: PaletteOptions['primary'];
  }
}

declare module '@mui/material/Button' {
  interface ButtonPropsColorOverrides {
    default: true;
  }
}


const mainColor = "#dc3522"
const textColor = "#444"
const secondaryColor = "#fbebda"

// A custom theme for Scholarly Editing
const theme = createTheme({
  typography: {
    fontFamily: "EB Garamond, Serif",
    body1: {
      fontSize: "1.25rem",
      paddingBottom: "1.25rem",
    },
    body2: {
      fontSize: "1rem"
    },
    subtitle1: {
      fontSize: "1.4rem",
    }
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        "@font-face": [
          {
            fontFamily: "EB Garamond",
            fontStyle: "normal",
            fontDisplay: "swap",
            fontWeight: 400,
          },
        ],
        "::selection": { 
          background: mainColor,
          color: secondaryColor
        },
        "a, a:visited, a:hover, a:active": {
          color: mainColor,
        },
        body: {
          color: textColor,
        },
        "h1, h2, h3, h4, h5, h6": {
          color: "#333",
        },
        figCaption: {
          textAlign: "center",
          fontSize: "1rem",
          fontWeight: "400",
          lineHeight: "1.66",
          letterSpacing: "0.03333em"
        },
        table: {
          width: "100%"
        },
        "ol li a": {
          wordWrap: "break-word"
        }
      },
    },
  },
  palette: {
    default: {
      main: "#444",
    },
    text: {
      primary: "#444",
    },
    primary: {
      main: mainColor,
    },
    secondary: {
      main: secondaryColor,
    },
    background: {
      default: "#fff",
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1280,
      xl: 1536,
    }
  }
})

export default theme

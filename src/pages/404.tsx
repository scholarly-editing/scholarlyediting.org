import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

interface Props {
  location: any
}

const NotFoundPage = ({ location }: Props) => (
  <Layout location={location}>
    <h1>PAGE NOT FOUND</h1>
  </Layout>
)

export default NotFoundPage

export const Head = () => {
  const title = "404: Not found | Scholarly Editing"
  return (
  <SEO>
    <html lang="en" />
    <title>{title}</title>
    <meta name="og:title" content={title}/>
    <meta name="twitter:title" content={title}/>
  </SEO>
)}

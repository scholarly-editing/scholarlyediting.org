import React from "react"
import Typography from "@mui/material/Typography"
import {Link, graphql} from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"

interface Props {
  location: {
    pathname: string
  }
  data: {
    allIssuesJson: {
      nodes: {
        doi: string
        date: string
        issueDir: string
        name: string
        desc: string
      }[]
    }
  }
}

const styles = {
  IssueLinks: {
    "& a, & a:visited": {
      textDecoration: "none"
    },
    "& a:hover, & a:active": {
      textDecoration: "underline"
    }
  }
}

const IssuesPage = ({ location, data }: Props) => {
  return (
    <Layout location={location.pathname}>
      <Typography variant="h3" component="h2">
        Issues
      </Typography>
      <div>
        {data.allIssuesJson.nodes.sort((a,b) => a.name > b.name ? -1 : 1).map(issue => {
          const {issueDir, name, date, desc} = issue
          return (<div key={issueDir}>
            <Typography variant="h4" component="h3" sx={styles.IssueLinks}>
              <Link to={issueDir}>{name}</Link>
            </Typography>
            <Typography variant="button">{date}</Typography>
            <Typography variant="body1" dangerouslySetInnerHTML={{__html: desc}} />
          </div>)
        })}
        <Typography variant="h4" component="h3" sx={styles.IssueLinks}>
          <a href="/se.index.issues.html">Archive: 2012 – 2017</a>
        </Typography>
        <Typography variant="body1">
          Volumes 33 – 38 issued between 2012 and 2017 are now archived. By
          clicking the link above you will be taken to the old website.
        </Typography>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query HomePageQuery {
    allIssuesJson {
      nodes {
        issueDir
        name
        doi
        date
        desc
      }
    }
  }
`

export default IssuesPage

export const Head = () => {
  const title = "Issues | Scholarly Editing"
  return (
  <SEO>
    <html lang="en" />
    <title>{title}</title>
    <meta name="og:title" content={title}/>
    <meta name="twitter:title" content={title}/>
  </SEO>
)}
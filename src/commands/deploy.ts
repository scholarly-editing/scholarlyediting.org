import fs from 'fs';
import path from 'path';
import { exec } from 'child_process';

// This script runs rsync to upload the build static site to our servers.

async function deploy() {
  // Check that rsync is available
  exec('rsync --version', (error) => {
    if (error) {
        console.error('rsync is not available or cannot be executed');
        process.exit(1);
    }
  });
  
  // Make sure public exists
  if (!fs.existsSync(path.join(__dirname, "../../public"))) {
    console.error(`"public" folder does not exist. Build the site first.`)
    process.exit(1)
  }

  // Infer location based on directory name
  const homeDir = "/home/scholar3/";
  const publicationDir = __dirname.includes("-prepub") ? "prepub.scholarlyediting.org" : "public_html";
  const deployDir = homeDir + publicationDir;

  // Run rsync

  const rsyncCommand = `rsync -avzh --delete \
  --exclude='issues/39/john-broughams-columbus-burlesque' \
  --exclude='issues/39/mabel-dodge-luhans-whirling-around-mexico-a-selection' \
  --exclude='issues/39/a-prototype-for-a-digital-edition-of-antonio-de-leon-pinelos-epitome-de-la-biblioteca-oriental-y-occidental-nautica-y-geografica-1629' \
  --exclude='issues/40/selections-from-the-revue-des-colonies-july-1834-and-july-1835' \
  --exclude='issues/40/marm-the-doctor-mill-children-and-the-american-dream-in-one-way-to-get-an-education' \
  --exclude='issues/41/paper-bullets' \
  --exclude='issues/41/kinship-and-longing' \
  --exclude=".htaccess" \
  public/ scholar3@scholarlyediting.reclaim.hosting:${deployDir}`
  console.log(`Running ${rsyncCommand}`)
  exec(rsyncCommand, (error, stdout, stderr) => {
    if (error) {
        console.error('rsync failed');
        console.error(stderr);
        process.exit(1);
    } else {
      console.log(stdout);
    }
  });
  
}
deploy()

import fs from 'fs'
import path from 'path'
import { program } from 'commander'
import yaml from 'js-yaml'
import crypto from 'crypto'
import generateDoi from './doi'
import * as CRT from './crtemplates'
import * as config from '../../gatsby-config'
import { slugify } from '../utils/slugify'

export interface contentMeta {
  title: string
  subtitle?: string
  authors: CRT.Author[]
  doi?: string
  group?: string
}

interface Microedition extends contentMeta {
  doi: string
  path: string
}

export interface Issue {
  sections: string[]
  issueDir: string
  name: string
  doi?: string
  date: string
  microeditions?: Microedition[]
}

interface Opts {
  basePath?: string
  issuesPath?: string
  microedPath?: string
  force?: boolean
  skipCR?: boolean
}

const combineUrl = (base: string, ...parts: string[]) => {
  const rest = parts.map(p => (
    p
      .replace(/^\//, '')
      .replace(/\/\/+/g, '/')
      .replace(/\/$/, '')
  )).join('/')
  return `${base.replace(/\/$/, '')}/${rest}`
}

const makeIssueDois = async (issue: string, opts: Opts = {}): Promise<void> => {

  const basePath = opts.basePath ? opts.basePath : path.join(__dirname, '../contents/issues/')
  const issuesPath = opts.issuesPath ? opts.issuesPath : path.join(__dirname, '../contents/issues/issues.json')
  const microedPath = opts.microedPath ? opts.microedPath : path.join(__dirname, '../../static/issues/')
  const force = opts.force ? opts.force : false
  const skipCR = opts.skipCR ? opts.skipCR : false
  const xmlOutput = path.join(__dirname, 'output')

  const baseURL = 'https://scholarlyediting.org/issues/'

  // Locate issue data from metadata file.
  let issues: Issue[] = []
  try {
    issues = JSON.parse(fs.readFileSync(issuesPath, 'utf8'))
  } catch (error) {
    console.error(`Could not read issues metadata file at ${issuesPath}`)
    process.exit(1)
  }
  
  const issueData: Issue | undefined = issues?.filter(i => i.issueDir === issue)[0]
  if (!issueData) {
    console.error(`No metadata found for ${issue}. Add metadata to \`/src/contents/issues/issues.json\`.`)
    process.exit(1)
  }

  // Check if DOI is already set.
  if (issueData.doi && !force) {
    console.error(`A DOI already exists for ${issue}. Use --force to regenerate DOIs.`)
    process.exit(1)
  }

  // Generate issue DOI.
  const { doiPrefix } = config.siteMetadata
  if (!doiPrefix) {
    console.error('Could not find DOI prefix in gatsby-config.js')
    process.exit(1)
  }
  console.log(`Generating DOI for issue ${issue}.`)
  const issueDoi = await generateDoi(doiPrefix)
  issueData.doi = issueDoi

  // Scaffold crossref XML
  const metaId = crypto.randomUUID()
  let xml = CRT.head(metaId)

  // Add issue to XML
  const issueUrl = combineUrl(baseURL, issue)
  xml += CRT.issue(issueDoi, issueUrl, issueData.name, issue)
  
  // Generate DOIs and XML for microeditions if available
  const microedDirs = fs.readdirSync(path.join(microedPath, issue), { withFileTypes: true }).filter(i => i.isDirectory()).map(i => i.name)
  if (microedDirs.length > 0) {
    for (const microedDir of microedDirs) {
      try {
        const microedMetaString = fs.readFileSync(path.join(microedPath, issue, microedDir, 'metadata.json'), {encoding: 'utf-8'})
        const microedMeta: Microedition = JSON.parse(microedMetaString)
        // Microedition metadata must have a doi already
        // Add XML
        const authors = microedMeta.authors.map(a => Object.assign({role: 'editor'}, a))
        const url = combineUrl(baseURL, issue, microedDir)
        xml += CRT.article(microedMeta.doi, url, authors, microedMeta.title, microedMeta.subtitle)
      } catch (error) {
        console.error('Could not locate or parse microedition metadata.')
        throw error
      }
    }
  }

  // Updated JSON metadata file
  fs.writeFile(issuesPath, JSON.stringify(issues, null, 2), (error) => {
    if (error) {
      console.error('Could not update issues metadata file.')
      throw error
    }
  })

  const issueDir = path.join(basePath, issue)
  if (!fs.existsSync(issueDir)) {
    console.error(`${issueDir} does not exist.`)
    process.exit(1)
  }

  const files = fs.readdirSync(issueDir).filter(f => f.endsWith('md'))
  
  if (files.length === 0) {
    console.error(`No markdown files found in ${issueDir}`)
    process.exit(1)
  }

  // Generate DOIs and XML for markdown contents frontmatter
  for (const file of files) {
    const filePath = path.join(issueDir, file)
    try {
      const md = fs.readFileSync(filePath).toString()
      const fmExpr = new RegExp(/---(.*?)---/s)
      const frontmatter = md.match(fmExpr)
      let fm: contentMeta
      if (frontmatter) {
        fm = yaml.load(frontmatter[1]) as contentMeta
      } else {
        console.error(`Could not retrieve YAML metadata from markdown file: ${file}`)
        process.exit(1)
      }

      if (!fm.doi || force) {
        console.log(`Generating DOI for file: ${file}`)
        const doi = await generateDoi(doiPrefix)
        fm.doi = doi
      }

      const updatedMd = md.replace(fmExpr, `---\n${yaml.dump(fm)}---`)
      fs.writeFile(filePath, updatedMd, (error) => {
        if (error) {
          console.error(`Could not update markdown file: ${file}.`)
          throw error
        }
      })

      // Update XML
      const url = combineUrl(baseURL, issue, slugify(fm.title))
      const authors = fm.authors.map(a => Object.assign({role: 'author'}, a))
      const safeTitle = fm.title.replace(/&/g, '&amp;')
      const safeSubTitle = fm.title.replace(/&/g, '&amp;')
      xml += CRT.article(fm.doi, url, authors, safeTitle, safeSubTitle)

      
    } catch (error) {
      console.error(`Could not read markdown file: ${file}.`)
      console.error(error)
      process.exit(1)
    }
  }

  // Output XML
  if (skipCR) {
    return
  }
  
  if (!fs.existsSync(xmlOutput)) {
    fs.mkdirSync(xmlOutput)
  }

  xml += CRT.tail()
  fs.writeFile(`${xmlOutput}/${metaId}.xml`, xml, (error) => {
    if (error) {
      console.error('Could not write XML metadata file.')
      process.exit(1)
    }
    console.log(`Wrote CrossRef XML for issue ${issue}.`)
  })

}

// CLI.
program
  .argument('<name>', 'Issue directory name')
  .option('--basePath <path>', 'Path to issues contents')
  .option('--issuesPath <path>', 'Path to issues metadata JSON file')
  .option('--microedPath <path>', 'Path to microedition contents')
  .option('--force', 'Force DOI regeneration')
  .option('--skipCR', 'Skip CrossRef XML metadata generation')

if (!process.argv[2]) {
  program.help()
}
program.parse()
const opts: Opts = program.opts()
makeIssueDois(program.args[0], opts)

// Export for programmatic use.
export default makeIssueDois
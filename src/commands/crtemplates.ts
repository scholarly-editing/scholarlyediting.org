export const head = (id: string) => {
  return `<?xml version="1.0" encoding="UTF-8"?>
<doi_batch version="4.4.2" xmlns="http://www.crossref.org/schema/4.4.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:jats="http://www.ncbi.nlm.nih.gov/JATS1" xsi:schemaLocation="http://www.crossref.org/schema/4.4.2 http://www.crossref.org/schema/deposit/crossref4.4.2.xsd">
  <head>
    <doi_batch_id>se${id}</doi_batch_id>
    <timestamp>${new Date().toISOString().replace(/[-TZ:\.]/g, '')}</timestamp>
    <depositor>
      <depositor_name>adei</depositor_name> 
      <email_address>info@scholarlyediting.org</email_address>
    </depositor>
    <registrant>Scholarly Editing</registrant> 
  </head>
  <body>
    <journal>
      <journal_metadata>   
        <full_title>Scholarly Editing</full_title>   
        <abbrev_title>SE</abbrev_title>   
        <issn media_type='electronic'>21671257</issn>   
        <doi_data>     
          <doi>10.55520/6ZH06EW2</doi>     
          <resource>https://scholarlyediting.org/</resource>   
        </doi_data> 
      </journal_metadata>
`
}

export const tail = () => {
  return `  
    </journal>
  </body>
</doi_batch>
`
}

export const issue = (doi: string, url: string, title: string, volume: string, isSpecial: boolean = false) => {
  const today = new Date().toISOString().substring(0,10).split('-')
  let number = `<journal_volume>
          <volume>${volume}</volume>
        </journal_volume>`
  if (isSpecial) {
    number = `<special_numbering>${volume}</special_numbering>`
  }

  return `
      <journal_issue>
        <titles>
          <title>${title}</title>
        </titles>
        <publication_date media_type="online">
          <month>${today[1]}</month>
          <day>${today[2]}</day>
          <year>${today[0]}</year>
        </publication_date>
        ${number}
        <doi_data>
          <doi>${doi}</doi>
          <resource>${url}</resource>
        </doi_data>
      </journal_issue>
`
}

export interface Author {
  first: string
  middle?: string
  last: string
  affiliations: string[]
  role: string
  orcid?: string
}

export const article = (doi: string, url: string, authors: Author[], title: string, subtitle: string = '') => {
  const today = new Date().toISOString().substring(0,10).split('-')
  const subtitleEl = subtitle === '' ? '' : `<subtitle>${subtitle}</subtitle>`
  const contributors = authors.map((a, i) => {
    const affiliations = a.affiliations.map(aff => {
      return `<affiliation>${aff}</affiliation>`
    }).join('\n            ')
    const orcid = a.orcid ? `<ORCID>https://orcid.org/${a.orcid}</ORCID>` : ''
    return `
          <person_name contributor_role="${a.role}" sequence="${i === 0 ? 'first' : 'additional'}">
            <given_name>${a.first}</given_name>
            <surname>${a.last}</surname>
            ${affiliations}
            ${orcid}
          </person_name>` 
  }).join('\n          ')
  return `
      <journal_article publication_type="full_text">
        <titles>
          <title>${title}</title>
          ${subtitleEl}
        </titles>
        <contributors>${contributors}</contributors>
        <publication_date media_type="online">
          <month>${today[1]}</month>
          <day>${today[2]}</day>
          <year>${today[0]}</year>
        </publication_date>
        <doi_data>
          <doi>${doi}</doi>
          <resource>${url}</resource>
        </doi_data>
      </journal_article>
`
}

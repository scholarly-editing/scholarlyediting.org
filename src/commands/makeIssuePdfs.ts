import fs from 'fs'
import path from 'path'
import { program } from 'commander'
import yaml from 'js-yaml'
import { Remarkable } from 'remarkable'
import { linkify } from 'remarkable/linkify'
import puppeteer from 'puppeteer'
import * as config from '../../gatsby-config'
import { Issue, contentMeta } from './makeIssueDois'
import { slugify } from '../utils/slugify'
import { Author } from './crtemplates'

interface Opts {
  basePath?: string
  issuesPath?: string
}

const makeIssuePdfs = async (issue: string, opts: Opts = {}): Promise<void> => {
  const basePath = opts.basePath ? opts.basePath : path.join(__dirname, '../contents/issues/')
  const issuesPath = opts.issuesPath ? opts.issuesPath : path.join(__dirname, '../contents/issues/issues.json')

  let issues: Issue[] = []
  try {
    issues = JSON.parse(fs.readFileSync(issuesPath, 'utf8'))
  } catch (error) {
    console.error(`Could not read issues metadata file at ${issuesPath}`)
    process.exit(1)
  }
  
  const issueData: Issue | undefined = issues?.filter(i => i.issueDir === issue)[0]
  if (!issueData) {
    console.error(`No metadata found for ${issue}. Add metadata to \`/src/contents/issues/issues.json\`.`)
    process.exit(1)
  }

  const issueDir = path.join(basePath, issue)
  if (!fs.existsSync(issueDir)) {
    console.error(`${issueDir} does not exist.`)
    process.exit(1)
  }

  const files = fs.readdirSync(issueDir).filter(f => f.endsWith('md'))
  
  if (files.length === 0) {
    console.error(`No markdown files found in ${issueDir}`)
    process.exit(1)
  }

  const logo = fs.readFileSync(path.join(__dirname, "../images/se-title.png")).toString('base64')
  const orcidLogo = fs.readFileSync(path.join(__dirname, "../images/orcid.png")).toString('base64')

  const printAuthors = (authors: Author[]) => (
    authors.map((a) => {
      const orcid = a.orcid ? 
        `<a href="https://orcid.org/${a.orcid}"><img src="data:image/png;base64,${orcidLogo}" alt="ORCID logo" class="orcid"/></a>`
      : ''
      return (
        `<strong>${a.first} ${a.middle || ''}${a.last ? ` ${a.last}` : ''}</strong>, ${a.affiliations.join('; ')}${orcid}`
      )
    }).join('<br/>')
  )

  const r = new Remarkable({html: true}).use(linkify)
  // Read CSS
  const css = fs.readFileSync(path.join(__dirname, 'pdf.css'))

  for (const file of files) {
    const filePath = path.join(issueDir, file)

    try {
      const md = fs.readFileSync(filePath).toString()
      const fmExpr = new RegExp(/---(.*?)---/s)
      const frontmatter = md.match(fmExpr)
      let fm: contentMeta
      if (frontmatter) {
        fm = yaml.load(frontmatter[1]) as contentMeta
      } else {
        console.error(`Could not retrieve YAML metadata from markdown file: ${file}`)
        process.exit(1)
      }

      const title = slugify(fm.title)

      // Remove yaml
      let processedmd = md.replace(fmExpr, '')

      // Generate HTML
      let html = r.render(processedmd)

      // Add stuff to HTML

      const isReview = fm.group === "Reviews"

      html = `<div${isReview ? ' class="review"' : ''}>${html}</div>`
      
      // Embed images as Base64
      html = html.replace(/<img src="([^"]+)"\s+alt="([^"]+)"(.*?>)/g, (match, p1, p2, p3) => {
        const caption = p2.replace(/&lt;/g, "<").replace(/&gt;/g, ">")
        match
        // Get image and convert to base64
        const image = fs.readFileSync(path.join(issueDir, p1)).toString('base64')
        return `<p class="fig"><img src="data:image/png;base64,${image}" alt="${p2}"${p3}</p><figcaption>${caption}</figcaption>`
      } )

      const subtitle = fm.subtitle ? `<h2 class="subtitle">${fm.subtitle}</h2>` : ""

      // Title and authors
      html = `<h1>${fm.title}</h1>${subtitle}
        <div class="piece_info">
        <span>${isReview ? "Reviewed by " : ''}${printAuthors(fm.authors)}</span>
        <span>DOI: <a href="https://doi.org/${fm.doi}">${fm.doi}</a></span>
        </div>${html}`

      // Scholarly Editing Header
      html = `<div class="header">
      <div class="logos">
        <div><img src="data:image/png;base64,${logo}" alt="Scholarly Editing" id="se_logo"/></div>
        <div><img src="https://archive.scholarlyediting.org/se-archive/template_images/adelogo.png" alt="ADE logo"/></div>
      </div>
      <div class="info">
      <p>
        <em>Scholarly Editing: The Annual of the Association for Documentary Editing</em>
        <br/>Noelle A. Baker and Kathryn Tomasek, Co-editors in Chief
        <br/>ISSN: ${config.siteMetadata.issn} | DOI: <a href="https://doi.org/${config.siteMetadata.doiPrefix}/${config.siteMetadata.doiSuffix}">
        ${config.siteMetadata.doiPrefix}/${config.siteMetadata.doiSuffix}</a>
      </p>
      <p style="margin-bottom: 0"><strong>${issueData.name}</strong>, ${issueData.date}, DOI: <a href="https://doi.org/${issueData.doi}">${issueData.doi}</a></p>
      <p>
      This work is licensed under a <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.
      © ${issueData.date.split('-')[0]} Scholarly Editing.
      </p>
      </div>
      </div>${html}`


      // Add CSS
      html = `<style>${css}</style>${html}`

      // For debugging the HTML
      // fs.writeFile(path.join(__dirname, `output/${title}.html`), html, (error) => {
      //   if (error) {
      //     console.error(`Could not update markdown file: ${file}.`)
      //     throw error
      //   }
      // })

      // Clean up footnotes
      html = html.replace(/\[(\d+)\]/g, '$1')

      // Generate PDF
      const browser = await puppeteer.launch(
        {args: ['--no-sandbox', '--disable-setuid-sandbox'],}
      )
      const page = await browser.newPage()

      await page.setContent(html, {
        waitUntil: 'networkidle0', // wait for page to load completely
      })

      await page.pdf({
        path: path.join(__dirname, `output/${title}.pdf`),
        format: 'letter',
        margin: {
          left: ".75in",
          right: ".75in",
          top: ".75in",
          bottom: ".75in"
        }
      })

      browser.close()
      
    } catch (error) {
      console.error(`Could not process markdown file: ${file}.`)
      console.error(error)
      process.exit(1)
    }
  }
}

// CLI.
program
  .argument('<name>', 'Issue directory name')
  .option('--basePath <path>', 'Path to issues contents')
  .option('--issuesPath <path>', 'Path to issues metadata JSON file')

if (!process.argv[2]) {
  program.help()
}
program.parse()
const opts: Opts = program.opts()
makeIssuePdfs(program.args[0], opts)

// Export for programmatic use.
export default makeIssuePdfs
import React from "react"
import { graphql, navigate } from "gatsby"
import Grid from "@mui/material/Grid"
import months from "../utils/months"
import Typography from "@mui/material/Typography"
import Button from "@mui/material/Button"
import { IGatsbyImageData } from "gatsby-plugin-image"

import SideCard from "./sideCard"
import Layout from "../components/layout"
import SEO from "../components/seo"

interface SideData {
  img: IGatsbyImageData
  title: string
  desc: string
  link: string
}

interface Props {
  location: any
  data: {
    markdownRemark: {
      frontmatter: {
        title: string
        date: string
        subtitle: string
        side: SideData[]
      }
      html: string
    }
  }
  pageContext: {
    modifiedTime: string
    issn: string
    description: string
    doi: string
  }
}

const styles = {
  title: {
    fontStyle: "italic",
  },
  quickLinks: {
    "& button": {
      marginBottom: "1.25rem",
    },
  }
}

export default function HomePage({ location, data, pageContext }: Props) {
  const { modifiedTime } = pageContext
  const { markdownRemark } = data
  const { frontmatter, html } = markdownRemark
  const { title, subtitle } = frontmatter

  const modifiedDate = new Date(modifiedTime)
  const date = `${modifiedDate.getDate()} ${
    months[modifiedDate.getMonth()]
  } ${modifiedDate.getFullYear()}`

  const side = frontmatter.side.map(s => {
    return <SideCard {...s} key={s.title} />
  })

  return (
    <Layout location={location.pathname}>
      <Grid container={true} spacing={10}>
        <Grid item={true} xs={12} sm={8}>
          <Typography
            variant="h3"
            component="h2"
            gutterBottom={false}
            sx={styles.title}
          >
            {title}
          </Typography>
          <Typography variant="subtitle1" component="h3" gutterBottom={true}>
            {subtitle}
          </Typography>
          <Typography
            variant="body1"
            gutterBottom={true}
            component="div"
            dangerouslySetInnerHTML={{ __html: html }}
          />
          <Grid container={true} sx={styles.quickLinks}>
            <Grid item={true} xs={12} sm={6}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => navigate("/about")}
              >
                Read more about the annual
              </Button>
            </Grid>
            <Grid item={true} xs={12} sm={6}>
              <Button
                variant="contained"
                color="primary"
                onClick={() => navigate("/issues")}
              >
                Explore the issues
              </Button>
            </Grid>
          </Grid>
          <Typography variant="h6" component="h2" sx={{fontWeight: "bolder", marginBottom: "-1em"}}>
          Image credits
          </Typography>
          <p>British Museum, Am2006,Drg.85</p>
          <p>
            Rolling Call for Contributions Image: photograph of the vellum
            manuscript for the Constitution of Vermont. Wikimedia Commons
            contributors, "
            <a href="https://commons.wikimedia.org/w/index.php?title=File:VtConstitution.png&amp;oldid=356790493">
              File:VtConstitution.png
            </a>
            ," Wikimedia Commons, the free media repository.
          </p>
          <p>
            Snail woodcut by Dora Carrington,
            appearing at the end of the Hogarth Press 1917 version of Virginia
            Woolf's "
            <a href="http://scholarlyediting.org/2014/editions/markonthewall_ho17.html">
              The Mark on the Wall
            </a>
            ." 
            British Library, Cup.401.f.24. Virginia Woolf: © The Society of Authors as the Literary Representative 
            of the Estate of Virginia Woolf. Leonard Woolf: © The University of Sussex and The Society of Authors 
            as the Literary Representative of the Estate of Leonard Woolf.
          </p>
        </Grid>
        <Grid item={true} xs={12} sm={4}>
          {side}
        </Grid>
      </Grid>
      <div>Last updated: {date}</div>
    </Layout>
  )
}

export const pageQuery = graphql`
  query($path: String!) {
    markdownRemark(frontmatter: { path: { eq: $path } }) {
      html
      frontmatter {
        subtitle
        path
        title
        side {
          img {
            childImageSharp {
              gatsbyImageData(placeholder: TRACED_SVG)
            }
          }
          title
          desc
          link
        }
      }
    }
  }
`

export const Head = ({pageContext}: Props) => {
  const {issn, description} = pageContext
  const title = "Home | Scholarly Editing"
  return (
  <SEO>
    <html lang="en" />
    <title>{title}</title>
    <meta name="og:title" content={title}/>
    <meta name="twitter:title" content={title}/>

    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
    <meta name="DC.type" content="Text" />
    <meta name="DC.format" content="text/html" />
    <meta name="DC.identifier" scheme="URI" content="https://scholarlyediting.org" />
    <meta name="DC.identifier" scheme="ISSN" content={issn} />
    <meta name="DC.title" content="Scholarly Editing Journal" />
    <meta name="DC.publisher" content="The Association for Documentary Editing" />
    <meta name="DC.language" scheme="RFC3066" content="en" />
    <meta name="keywords" content="Text Encoding Initiative, TEI, digital humanities, markup, XML, scholarly publishing" />
    <meta name="DC.subject" content="Text Encoding Initiative, TEI, digital humanities, markup, XML, scholarly publishing" />
    <meta name="description" content={description} />
    <meta name="DC.description" content={description} />
    <meta name="twitter:card" content="summary" />
    <meta property="og:type" content="article" />
    <meta property="og:url" content="https://scholarlyediting.org" />
    <meta property="og:title" content="Scholarly Editing Journal" />
    <meta property="og:description" content={description} />
    <meta property="og:image" content="https://www.openedition.org/docannexe/image/9092/jtei_160x75.png" />
  </SEO>
)}
import React from "react"
import months from "../utils/months"
import Typography from "@mui/material/Typography"

import Layout from "../components/layout"
import SEO from "../components/seo"
import Grid from "@mui/material/Grid"
import theme from "../theme"

import {Author} from '../utils/printAuthors'
import { Link } from "gatsby"
import Doi from "../components/doi"
import { GatsbyImage, IGatsbyImageData } from "gatsby-plugin-image"
import Player from "../components/webvtt/player"
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Stack, TextField, useMediaQuery } from "@mui/material"
import Share from "../components/share"


interface Props {
  location: any
  pageContext: {
    modifiedTime: string
    title: string
    subtitle: string
    authors: Author[]
    issue: string
    issueDate: string
    issuePath: string
    html: string
    doi: string
    orcidImg: IGatsbyImageData
    group: string,
    slug:string
    issn: string,
    keywords?: string[],
    abstract?: string,
    audio: {
      file: string
      transcript: string
      chapters: string
    }
  }
}

const styles = {
  info: {
    fontSize: "1rem",
    margin: "1.5rem 0",
    backgroundColor: theme.palette.secondary.main,
    padding: "1em",
    "& :last-child": {
      textAlign: "right"
    },
    "& a": {
      color: `${theme.palette.text.primary} !important`
    }
  },
  back: {
    "& a": {
      color: `$theme.palette.text.primary} !important`
    },
    display: 'inline-block',
    paddingBottom: "1em"
  },
  orcid: {
    marginLeft: ".5rem"
  },
  Content: {
    "& p": {
      textIndent: "2rem"
    },
    "& p:first-of-type, & h2 + p, & h3 + p, & h4 + p, & h5 + p, & h6 + p, & .noindent p": {
      textIndent: "0"
    },
    "& blockquote": {
      marginLeft: "5rem"
    },
    "& .footnote-backref": {
      marginLeft: ".75rem"
    }
  },
  Review: {
    "& > p:nth-child(1)": {
      border: `1px solid ${theme.palette.primary.main}`,
      padding: ".5em"
    },
    "& > p:nth-child(2)": {
      textIndent: "0"
    }
  },
  icon: {
    position: "relative",
    top: ".25rem"
  }
}

export default function PageTemplate({ location, pageContext }: Props) {
  const isScreenSmall = useMediaQuery(theme.breakpoints.down('sm'))
  
  const { modifiedTime, title, subtitle, authors, html, issue, issuePath, doi,
    orcidImg, group, slug, audio } = pageContext

  const modifiedDate = new Date(modifiedTime)
  const date = `${modifiedDate.getDate()} ${
    months[modifiedDate.getMonth()]
  } ${modifiedDate.getFullYear()}`

  const authorsFull = authors.map((a: Author) => (
    <React.Fragment key={a.last}>
      {a.first}{a.middle ? ` ${a.middle}` : ''}{a.last ? ` ${a.last}` : ''}, {a.affiliations.join('; ')}
      {a.orcid && 
        <a href={`https://orcid.org/${a.orcid}`} style={styles.orcid}>
          <GatsbyImage image={orcidImg} alt="ORCID logo"/>
        </a>
      }
      <br/>
    </React.Fragment>
  ))

  let contentClass = styles.Content
  let review = ''
  if (group === "Reviews") {
    contentClass = Object.assign({}, contentClass, styles.Review)
    review = "Reviewed by " 
  }

  const pdf = <span><PictureAsPdfIcon fontSize="small" sx={styles.icon}/>{' '}
  <a id="PDFLink" href={`../${slug}.pdf`} aria-labelledby="PDFLink contentTitle">PDF</a></span>
  const url = `https://scholarlyediting.org/issues/${issuePath}/${slug}/`;
  const info = !isScreenSmall ? (<><Doi value={doi}/>{' | '}{pdf}{' | '}<Share url={url}/></>)
  : (<Stack alignItems="flex-start" spacing={1}><Doi value={doi} numberOnly/>{pdf}<Share url={url}/></Stack>)

  return (
    <Layout location={location.pathname}>
      <Typography variant="button" sx={styles.back}>
        &lt; <Link to={`/issues/${issuePath}`}>{issue}</Link>
      </Typography>
      <Typography variant="h3" component="h2" gutterBottom={false} id="contentTitle"
        dangerouslySetInnerHTML={{__html: title}}/>
      <Typography variant="h4" component="h3" gutterBottom={false}
        dangerouslySetInnerHTML={{__html: subtitle}}/>
      <Typography variant="h5" component="h4" gutterBottom={false} style={{textAlign: "right"}}>
        {review}{authorsFull}
      </Typography>
      <Grid container sx={styles.info}>
        <Grid item xs={6}>
          {info}
        </Grid>
        <Grid item xs={6}>Scholarly Editing, {issue}</Grid>
      </Grid>
      <Typography
        sx={contentClass}
        variant="body1"
        gutterBottom
        component="div"
        dangerouslySetInnerHTML={{ __html: html }}
      />
      {audio && 
        <Player audio={audio.file} transcript={audio.transcript} chapters={audio.chapters} preload />
      }
      <div>Last updated: {date}</div>
    </Layout>
  )
}

export const Head = ({pageContext}: Props) => {
  const { title, issue, issuePath, slug, issn, doi, authors, issueDate } = pageContext
  const safeTitle = title.replace(/<[^>]+>/, '')
  const fullTitle = `${safeTitle} | ${issue} | Scholarly Editing`
  const issueDateObj = new Date(issueDate)
  const fullPath = `http://scholarlyediting.org/issues/${issuePath}/${slug}`
  const formatAuthor = (a: Author) => (`${a.last}, ${a.first}${a.middle ? ` ${a.middle}` : '' }`)
  return (
  <SEO>
    <html lang="en" />
    <title>{fullTitle}</title>
    <link rel="schema.DC" href="http://purl.org/dc/elements/1.1/" />
    <meta name="DC.type" content="Text" />
    <meta name="DC.format" content="text/html" />
    <meta name="DC.identifier" scheme="URI" content={fullPath} />
    <meta name="DC.identifier" scheme="ISSN" content={issn} />
    <meta name="DC.title" content={safeTitle} />
    <meta name="DC.publisher" content="The Association for Documentary Editing" />
    <meta name="DC.language" scheme="RFC3066" content="en" />
    <meta name="DC.type" content="journalArticle" />
    <meta name="DC.identifier" scheme="DOI" content={doi} />
    <meta name="citation_doi" content={doi} />
    {authors.map(a => <>
      <meta name="author" content={formatAuthor(a)} />
      <meta name="DC.creator" content={formatAuthor(a)} />
    </>)}
    {pageContext.keywords ? <>
      <meta name="keywords" content={pageContext.keywords.join(", ")}/>
      <meta name="DC.subject" lang="en" content={pageContext.keywords.join(", ")}/>
      <meta name="citation_keywords" content={pageContext.keywords.join(", ")}/>
    </> : ""}
    {pageContext.abstract ? <>
      <meta name="description" lang="en" content={pageContext.abstract} />
      <meta name="citation_abstract" lang="en" content={pageContext.abstract} />
      <meta property="og:description" content={pageContext.abstract} />
      <meta name="twitter:description" content={pageContext.abstract} />
    </> : ""}
    <meta name="DC.date" scheme="W3CDTF" content={issueDate} />
    <meta name="DC.rights" content="
                For this publication a Creative Commons Attribution 4.0 International license has been granted by the author(s) who retain full copyright.
          " />
    <meta name="DC.relation.isPartOf" content={`Scholarly Editing Journal, ${issue}, ${issueDateObj.getFullYear()}`} />
    <meta name="DC.source" content="https://scholarlyediting.org" />
    <meta name="citation_journal_title" content="Scholarly Editing Journal" />
    <meta name="citation_publisher" content="The Association for Documentary Editing" />
    <meta name="citation_authors" content={authors.map(a => formatAuthor(a)).join("; ")} />
    <meta name="citation_title" content={title} />
    <meta name="citation_publication_date" content={issueDate.replace(/-/g, "/")} />
    <meta name="citation_online_date" content={issueDate.replace(/-/g, "/")} />
    <meta name="citation_issn" content={issn} />
    <meta name="citation_issue" content={issue} />
    <meta name="citation_language" content="en" />
    <meta name="citation_abstract_html_url" content={fullPath} />
    <meta name="citation_fulltext_html_url" content={fullPath} />
    <meta name="citation_pdf_url" content={`${fullPath}.pdf`} />
    <link title="schema(PRISM)" rel="schema.prism" href="http://prismstandard.org/namespaces/basic/2.0/"/>
    <meta name="prism.url" content={title}/>
    <meta name="prism.publicationName" content="Scholarly Editing Journal"/>
    <meta name="prism.number" content={issue}/>
    <meta name="prism.issueName" content={issue}/>
    <meta name="prism.publicationDate" content={`${issueDate}T00:00:00+02:00`}/>
    <meta name="prism.eIssn" content={issn}/>
    <meta property="og:type" content="article" />
    <meta property="og:url" content={fullPath} />
    <meta property="og:title" content={title} />
    <meta name="twitter:title" content={title}/>
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:creator" content={authors.map(a => formatAuthor(a)).join("; ")} />
  </SEO>
)}
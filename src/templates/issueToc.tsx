import React from "react"
import Typography from "@mui/material/Typography"
import {graphql, Link} from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import theme from "../theme"
import Grid from "@mui/material/Grid"
import Box from "@mui/material/Box"

import months from '../utils/months'
import printAuthors, { Author } from '../utils/printAuthors'
import Doi from "../components/doi"
import { styled } from '@mui/material/styles'
import Share from "../components/share"

type Content = {
  title: string
  htmlTitle?: string
  subtitle?: string
  path: string
  authors: Author[]
}

interface Props {
  location: {
    pathname: string
  }
  pageContext: {
    issueName: string
    toc: {
      [key: string]: Content[]
    }
  }
  data: {
    allMicroEditionMeta?: {
      nodes: Content[]
    }
    allIssuesJson: {
      nodes: {
        sections: {
          title: string
          contents: string[]
        }[]
        doi: string
        date: string
        more?: {
          title: string
          content: string
        }[]
        issueDir: string
      }[]
    }
  }
}

const StyledLink = styled(Link)(() => ({
  textDecoration: "none",
  "&:visited": {
    textDecoration: "none"
  },
  "&:hover, &:active": {
    textDecoration: "underline"
  }
}))

const styles = {
  Group: {
    marginBottom: "1em"
  },
  Section: {
    paddingBottom: "0 !important",
    marginBottom: "1.25rem",
    textDecorationLine: "underline",
    textDecorationColor: theme.palette.primary.main,
    textDecorationThickness: "4px",
    textUnderlineOffset: "5px"
  },
  Authors: {
    paddingLeft: '3em'
  },
  Editors: {
    fontStyle: "italic"
  },
  Doi: {
    "& a": {
      color: theme.palette.text.primary
    },
    fontSize: "1rem",
    float: "right"
  },
  info: {
    margin: "1rem 0 1rem 0",
    backgroundColor: theme.palette.secondary.main,
    padding: "1em",
    "& :last-child": {
      textAlign: "right"
    },
    "& a": {
      color: theme.palette.text.primary
    }
  }
}

const IssuesPage = ({ location, pageContext, data }: Props) => {
  const {issueName, toc} = pageContext
  const {sections, doi, date, more, issueDir} = data.allIssuesJson.nodes[0]

  
  const makeContent = (contents: Content[]): JSX.Element[] =>  {
    if (!contents) return []
    return contents.map((content) => {
      const mainTitle = content.htmlTitle ? content.htmlTitle : content.title
      const title = content.subtitle ? `${content.title}: ${content.subtitle}` : mainTitle
      return (<React.Fragment key={content.title}>
        <Typography variant="h6" component="p" sx={{paddingBottom: "1rem"}}>
          <StyledLink to={content.path} dangerouslySetInnerHTML={{__html: title}} />
        </Typography>
        <Typography variant="body1" component="p" sx={styles.Authors} gutterBottom>
          {printAuthors(content.authors)}
        </Typography>
      </React.Fragment>)
    })
  }
  
  let microeditions: JSX.Element | null = null
  let topsection: JSX.Element | null = null

  if (data.allMicroEditionMeta) {
    microeditions = <Box style={styles.Group}>
      <Typography variant="h4" component="h2" sx={styles.Section}>
        Micro-Editions
      </Typography>
      {makeContent(data.allMicroEditionMeta.nodes)}
    </Box>
  }

  if (toc["__none__"]) {
    topsection = <Box style={styles.Group}>{makeContent(toc["__none__"])}</Box>
  }

  const DateObj = new Date(date)

  const extraSections = more ? more.map(m => (
    <React.Fragment key={m.title}>
      <Typography variant="h4" component="h2" sx={styles.Section}>
        {m.title}
      </Typography>
      <Typography variant="body1" component="div" gutterBottom dangerouslySetInnerHTML={{__html: m.content}} />
    </React.Fragment>
  )) : ''

  return (
    <Layout location={location.pathname}>
      <Typography variant="h3" component="h2">
        {issueName}
      </Typography>
      <Grid container sx={styles.info} spacing={0}>
        <Grid item xs={4}><Doi value={doi}/></Grid>
        <Grid item xs={4}><Share url={`https://scholarlyediting.org/issues/${issueDir}`}/></Grid>
        <Grid item xs={4}>{DateObj.getDate() + 1} {months[DateObj.getMonth()]} {DateObj.getFullYear()}</Grid>
      </Grid>
      {topsection}
      {microeditions}
      {sections.map((section, i) => {
        return <Box sx={styles.Group} key={`g${i}`}>
          <Typography variant="h4" component="h2" style={styles.Section}>
            {section.title}
          </Typography>
          {makeContent(toc[section.title])}
        </Box>
      })}
      {extraSections}
    </Layout>
  )
}

export const pageQuery = graphql`
  query($issueName: String!) {
    allMicroEditionMeta(filter: {issue: {eq: $issueName}} sort: {group_order: ASC}) {
      nodes {
        authors {
          first
          middle
          last
          affiliations
        }
        subtitle
        title
        htmlTitle
        path
      }
    }
    allIssuesJson(filter: {name: {eq: $issueName}}) {
      nodes {
        sections {
          contents
          title
        }
        doi
        date
        more {
          title
          content
        }
        issueDir
      }
    }
  }
`

export default IssuesPage

export const Head = ({pageContext}: Props) => {
  const title = `${pageContext.issueName} | Scholarly Editing`
  return (
  <SEO>
    <html lang="en" />
    <title>{title}</title>
    <meta name="og:title" content={title}/>
    <meta name="twitter:title" content={title}/>
  </SEO>
)}

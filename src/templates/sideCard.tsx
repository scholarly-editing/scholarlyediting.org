import React from "react"
import { navigate } from "gatsby"
import Card from "@mui/material/Card"
import CardActions from "@mui/material/CardActions"
import CardContent from "@mui/material/CardContent"
import CardMedia from "@mui/material/CardMedia"
import Button from "@mui/material/Button"
import Typography from "@mui/material/Typography"
import { GatsbyImage, IGatsbyImageData, getImage } from "gatsby-plugin-image"

interface Props {
  img: IGatsbyImageData
  title: string
  desc: string
  link: string
}

const styles = {
  media: {
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "top center",
  },
  featured: {
    marginBottom: "1.25rem",
    "& button": {
      marginBottom: "1.25rem",
    },
  },
}

const SideCard = ({ img, title, desc, link }: Props) => {
  const cardImage = getImage(img) as IGatsbyImageData

  let cardActions
  if (link && link !== "") {
    let onClick = () => {
      navigate(link)
    }
    if (link.endsWith("html")) {
      onClick = () => {
        window.location.href = link
      }
    }
    cardActions = (
      <CardActions>
        <Button size="small" color="primary" onClick={onClick}>
          Learn more
        </Button>
      </CardActions>
    )
  }

  return (
    <Card sx={styles.featured}>
      <CardMedia title="Example Cover" sx={styles.media}>
        <GatsbyImage image={cardImage} alt="Side image" />
      </CardMedia>
      <CardContent>
        <Typography gutterBottom={true} variant="h5" component="h3">
          {title}
        </Typography>
        <Typography
          variant="body2"
          component="p"
          dangerouslySetInnerHTML={{ __html: desc }}
        />
      </CardContent>
      {cardActions}
    </Card>
  )
}

export default SideCard

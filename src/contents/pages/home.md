---
path: "/"
title: "Scholarly Editing: The Annual of the Association for Documentary Editing"
subtitle: "Editor in Chief Noelle A. Baker"
side: 
  - title: "Volume 41"
    img: "../../images/cfp-vt.jpg"
    desc: "Volume 41 adds upgraded accessibility and audio content to enhance readers’ experiences, and once again reflects the journal’s commitment to offering public-facing materials with topical, geographical, and chronological breadth. This volume includes editions that expand access to documents that recover kinship and resistance practices among peoples of African and Indigenous descent in colonial Louisiana and that offer glimpses into Northern rural women’s marital, sexual, and reproductive lives during the U.S. Civil War; LGBTQ source materials for K-12 classrooms; decolonialist narratives of interactions between Spanish and Indigenous peoples in the U.S. Southwest; editorial interventions necessitated by wartime trauma; the affordances of digital tools and strategies to create innovative editions and engage with students; and the recovery of the  lives and writings of Black feminists, Muslim women, queer coteries, and prison inmates."
    link: "/issues/41"
  - title: "Rolling Call for Contributions"
    img: "../../images/archive-card.jpg"
    desc: "The editorial team invites ongoing contributions for peer review to <em>Scholarly Editing</em>. We welcome transcripts of conversations and interviews between recovery practitioners; essays on the theory, practice, and pedagogy of recovery; reviews of print and digital editions, digital humanities projects, and the digital tools that enhance recovery; and small-scale editions of the understudied authors, texts, and documents that reflect our diverse and multifaceted cultural heritages."
    link: "/call"
---

## Statement of Purpose
*Scholarly Editing* is an open-access, peer-reviewed journal committed to the development and advancement of all aspects of textual and recovery, including the preservation of texts and artifacts that represent and celebrate the lives of and contributions from and about Black, Latinx, and Indigenous peoples; Asian Americans and Pacific Islanders; women; LGBTQ+ individuals; and peoples of the Global South. In addition to projects that illustrate the traditional range of editorial methodologies and practices, we welcome those that feature rare or marginal texts, texts that dislodge the single-author model, oral histories and tales, community recovery, creative works of “rememory,” and the decolonizing of artistic works, archives, records, and editions for the discoverability of racialized and underrepresented stories and cultural artifacts.

The editors are committed to amplifying the work of diverse voices. We particularly encourage contributions from marginalized, silenced, or overlooked groups. We welcome work in all disciplines—from new and established projects, individuals at any stage of their career, and those who engage in public history or otherwise seek to advance knowledge beyond the academy.

The content published in the journal includes essays, micro-editions, reviews of print and digital editions and of digital projects and tools that enhance recovery work, teaching materials, and transcripts of interviews and conversations. The journal’s eclectic, multidisciplinary approach makes it an invaluable resource for anyone interested in the theory, practice, and pedagogy of recovery and editing, including educators, students, researchers, scholars, historians, archivists, editors, information professionals, digital humanists, local genealogists, and community members. 


## Statement on Diversity and Anti-Racism
The editors of *Scholarly Editing* recognize that Black, Latinx, and Indigenous peoples; Asian Americans and Pacific Islanders; women; LGBTQ+ individuals; and the peoples and cultures of the Global South are underrepresented in the field of scholarly editing. That paucity of representation is a telling indicator of systemic and institutional racism, sexism, homophobia, and other forms of discrimination. 

We recognize that our editorial and advisory boards are insufficiently diverse, and we will invite underrepresented voices as we add to these bodies. We commit to supporting historically underrepresented and racialized groups and peoples and the recovery of their primary materials, and we further commit to several anti-racist and anti-colonialist actions, moving forward. As our [Rolling Call for Contributions](https://scholarlyediting.org/call) details in greater depth, in all journal sections we seek decolonialist analyses and content from and about underrepresented groups. Likewise, we are inviting members of underrepresented groups to serve as peer reviewers for contributions to the journal. 

## Peer Review
All content published in *Scholarly Editing* is rigorously reviewed by at least two scholars with expertise relevant to the submission under review. Essays are subject to a standard double-blind review process. A different procedure is followed for micro-editions. The editors select promising editions from submitted proposals. For selected editions, the journal’s technical editor and micro-edition co-editors work with the edition's editor(s) to put the edition online at an unpublished URL. Thereafter, the edition undergoes a double-blind review. Reviewers evaluate the edition's content in addition to its editorial theory and methodology. Reviewers of essays and micro-editions recommend that the journal accept the submission as-is, accept it only after revision, or reject it. 

All reviews are shared with contributors after evaluation by the editor in chief to ensure their compliance with the journal's ethical standards for peer review. Submissions that are accepted as-is or after appropriate revision are edited by one or more members of the editorial board as well as the journal's copy editor before publication in *Scholarly Editing*.

## Open Access
Between 2012 and 2017, content was published under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License. Since 2022, content has been published under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. Contributors retain all rights to their original content. For every edition published in *Scholarly Editing*, readers may access the user interface developed for web publication and the XML file containing the core metadata for the edition.



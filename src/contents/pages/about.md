---
path: "/about/"
title: "About"
---

*Scholarly Editing: The Annual of the Association for Documentary Editing* is a peer-reviewed, open-access journal for the advancement and promotion of editorial and recovery theories, practices, and pedagogies. Revived in 2020, the journal was edited by Amanda Gailey and Andrew Jewell from 2012 to 2017. From 1979 to 2012, the Association for Documentary Editing published the journal's predecessor, *Documentary Editing*, a print publication that evolved from a newsletter format to a quarterly and then annual journal.

The editorial board of *Scholarly Editing* invite essays, reviews of print and digital editions, interviews, pedagogical pieces, and small-scale editions of understudied authors and texts that reflect our diverse and multifaceted cultural heritages.

The journal intends to represent contributions from all countries and cultures, across disciplines and from outside the academy, including but not restricted to educators, community groups, local genealogists, families seeking ancestors, researchers, scholars, historians, archivists, curators, editors, information professionals, students, and digital humanists. We particularly welcome contributions from and about Black, Latinx, and Indigenous peoples; Asian Americans and Pacific Islanders; women; LGBTQ+ individuals; and the people and cultures of the Global South. 

## Essays

In addition to projects that illustrate the traditional range of editorial methodologies and practices, we welcome those that feature rare or marginal texts, texts that dislodge the single-author model, oral histories and tales, community recovery, and creative works of “rememory.” Such contributions may also explore the digital tools and contexts that enhance this work.

## Uncovering and Sustaining the Cultural Record

Editing primary sources for publication has extensive origins in multiple disciplines. We invite scholars, digital humanists, librarians, students, archivists, educators, and community members from outside these groups to contribute brief essays (1,250-4,000 words) about their experiences of uncovering and sustaining the cultural record as a set of practices, as a field, or as an act of recovery of silenced voices.
Our goal is to explore capaciously the contexts of knowledge production as theorized by Roopika Risam in _New Digital Worlds: Postcolonial Digital Humanities in Theory, Praxis, and Pedagogy_ (2019). Central questions include “how projects are designed, how material in them is framed, how data in them is managed, and what forms of labor are being used to create them.”

## Review Essays

_Scholarly Editing_ reviews letterpress and digital editions, digital projects, and the digital tools that enhance recovery of, and expand access to, primary source materials. We also review materials that provide new ways of understanding, contextualizing, and reimagining editorial, archival, and publishing practices and their role in shaping the historical record and literary canon. In accordance with our Statement of Purpose, we review materials that amplify the work of diverse voices and celebrate the contributions of underrepresented and silenced communities. While we do not accept unsolicited reviews, we welcome proposals from readers who would like to serve as reviewers as well as recommendations of work that may be appropriate for review.

## College and University Classroom Essays

We invite all explorations of the intersections between recovery and pedagogy at the university level. Potential areas of inquiry may include theoretical approaches to teaching scholarly editing and other forms of digital recovery, the use of primary source materials in the classroom and in public outreach programs, teaching with print editions and/or born-digital projects, and training student members of editorial projects. Collaborative essays are welcome, including those that advance their argument through case studies and with materials such as assignments, course syllabi, and excerpts from students' work (with appropriate permission).

## Micro-Editions

_Scholarly Editing_ is a home to sustainable small-scale editions of interesting and understudied texts. Such editions may range from a single document to 130 short documents or to two variants of a single text. The editors are particularly interested in hybridic and “messy” texts; rare and marginal texts; texts by marginalized and racialized people, women, LGBTQ+ and other underrepresented groups—as well as texts that dislodge the model of single-author agency. Past micro-editions have included plays, letters, poetry, fiction, journals, treatises, theological writing, periodical extracts, experimental autobiographies, an interactive game manual, and interviews.

We encourage those who wish to propose a micro-edition to consult the micro-editions co-editors [Raffaele Viglianti](mailto:rviglian@umd.edu) and [Megan Bushnell](mailto:megan.bushnell@ling-phil.ox.ac.uk) in advance of forwarding their proposals.

## Voices and Perspectives: Interviews and Conversations

We publish transcripts of conversations and interviews with recovery practitioners. We invite those who wish to propose interviews to complete <a href="https://docs.google.com/forms/d/1XNFvntl4MLsNG__rnB1cL31gazwiFODr0jd_xdss198" target="_blank">this form</a>. For additional concerns or information, contact the Voices and Perspectives section editor [Julian C. Chambliss](mailto:chambl91@msu.edu).

## Editorial Board

[Noelle A. Baker](mailto:noelle.baker@me.com), Editor in Chief and Acting Editor, Uncovering and Sustaining the Cultural Record

[Megan Bushnell](mailto:megan.bushnell@ling-phil.ox.ac.uk), Micro-editions Co-Editor

[Julian C. Chambliss](mailto:chambl91@msu.edu), Voices and Perspectives Editor

[Eagan Dean](mailto:eagansdean@gmail.com), Essays Co-Editor

Lona Dearmont, Copy Editor

[Silvia P. Glick](mailto:silviaglick@gmail.com), Reviews Editor

[Jenifer Ishee](mailto:jishee@conncoll.edu), Assistant Managing Editor

[Serenity Sutherland](mailto:serenity.sutherland@oswego.edu), College and University Classrooms Editor

[Cheyenne Symonette](mailto:symonet1@msu.edu), Voices and Perspectives Associate Editor

[Robert Riter](mailto:rbriter@ua.edu), Managing Editor

[Raffaele Viglianti](mailto:rviglian@umd.edu), Technical Editor and Micro-editions Co-Editor

## Advisory Board

Suzanne R. Black, University of Edinburgh

Brigitte Fielder, University of Wisconsin-Madison

Clinton Fluker, Emory University

Theresa Strouth Gaul, Texas Christian University

Melissa Jerome, University of Florida

Andrew Way Leong, University of California, Berkeley

Connie Lester, University of Central Florida

Clayton McCarl, University of North Florida

Linda García Merchant, University of Houston

Elizabeth Polcha, Drexel University

Scott Trudell, University of Maryland

Seretha Williams, Agnes Scott College

Mariam Zia, Lahore School of Economics

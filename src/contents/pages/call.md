---
path: "/call/"
title: "Rolling Call for Contributions"
---

We have a rolling call for contributions and are happy to accept them at any time. However, if you would like your piece to be considered for publication in the next volume, which comes out in April, please submit your piece for peer review by September 30.

Several recent publications influence our rolling call. The essays published in _Indigenous Textual Cultures: Reading and Writing in the Age of Global Empire_ (2020) and in _The Digital Black Atlantic_ (2021) emphasize the impacts of decolonial scholarship worldwide, and we are grateful to editors Tony Ballantyne, Lachy Paterson, Angela Wanhalla, Roopika Risam, and Kelly Baker Josephs for these contexts. Similarly, “Libraries and Archives in the Anthropocene” (2020), a recent issue of the _Journal of Critical Library and Information Studies_ edited by Eira Tansey and Robert D. Montoya, interrogates the meanings of archival and library practices in our current cultural moment.

We particularly seek the contributions of researchers from Black, Latinx, and Indigenous communities; Asian Americans and Pacific Islanders; women; LGBTQ+ individuals; and people and cultures of the Global South as well as those who have expertise in the histories and literatures of those groups and peoples, including documentary editors, textual scholars, historians, educators, genealogists, family historians, students, librarians, archivists, and community members.

Among others, contributors may explore subjects such as the following:  
* the recovery efforts of small-scale projects and micro-editions;
* rare or marginal texts;
* texts that dislodge the single-author model;
* oral histories and tales;
* creative works of “rememory”;
* explorations of ways in which scholarly editions, archives, metadata, and pedagogical recovery projects can promote inclusion, rather than reproducing colonization/marginalization;
* discussions of the manner in which editors can offer nuance and context to historically famous and canonical figures so that attention is given to their accomplishments as well as the ways in which they violated human rights or ethical norms, or in other ways failed to live up to the representations of them in popular culture;
* the decolonizing of artistic works, archives, records, and editions for the discoverability of racialized and underrepresented stories and cultural artifacts as well as cautionary advice from communities who prefer to preserve their cultural heritages in their own ways; and
* the role that new technologies, social media environments, editorial institutes, and other educational initiatives play in advancing all of the endeavors set forth above.

We acknowledge the complex issues related to cultural sovereignty and the ownership and control of texts, stories, and documents of Indigenous peoples, and we recognize that our adoption of a Creative Commons license and open access publication raises questions of colonization and appropriation with regard to micro-editions. We welcome short meditations, provocations, and full-length essays that address these concerns.

We particularly encourage transcripts of conversations and interviews between recovery practitioners; essays on the theory, practice, and pedagogy of recovery; reviews of print and digital editions, digital humanities projects, and the digital tools that enhance recovery; and small-scale editions of the understudied authors, texts, and documents that reflect our diverse and multifaceted cultural heritages.

We are interested in contributions in all disciplines and from individuals at any stage of their career, as well as from those who engage in public history and the advancement of knowledge beyond the academy. We encourage the submission of contributions from all those who are custodians of knowledge.

## Essays

In addition to projects that illustrate the traditional range of editorial methodologies and practices, we welcome those that feature rare or marginal texts, texts that dislodge the single-author model, oral histories and tales, community recovery, and creative works of “rememory.” Such contributions may also explore the digital tools and contexts that enhance this work.

## Uncovering and Sustaining the Cultural Record
Editing primary sources for publication has extensive origins in multiple disciplines, as is evident from the membership of the Association for Documentary Editing, a multidisciplinary organization that includes scholars from history, philosophy, literature, and musicology in the United States and abroad. We invite scholars, digital humanists, librarians, students, archivists, educators, and community members from outside these groups to contribute brief essays (1,250-4,000 words) about their experiences of uncovering and sustaining the cultural record as a set of practices, as a field, or as an act of recovery of silenced voices.

In issuing this invitation, we look forward to publishing a set of short essays that will demonstrate diversities of practice, perspective, and emphasis. Our goal is to explore capaciously the contexts of knowledge production as theorized by Roopika Risam in _New Digital Worlds: Postcolonial Digital Humanities in Theory, Praxis, and Pedagogy_ (2019). Central questions include “how projects are designed, how material in them is framed, how data in them is managed, and what forms of labor are being used to create them.”

## Review Essays
_Scholarly Editing_ reviews letterpress and digital editions, digital projects, and the digital tools that enhance recovery of, and expand access to, primary source materials. We also review materials that provide new ways of understanding, contextualizing, and reimagining editorial, archival, and publishing practices and their role in shaping the historical record and literary canon. In accordance with our Statement of Purpose, we review materials that amplify the work of diverse voices and celebrate the contributions of underrepresented and silenced communities.

While we do not accept unsolicited reviews, we welcome proposals from readers who would like to serve as reviewers as well as recommendations of work that may be appropriate for review.

Email Reviews Editor [Silvia Glick](mailto:silviaglick@gmail.com) with any questions or to propose a review.

## College and University Classroom Essays
We invite all explorations of the intersections between recovery and pedagogy at the university level. Potential areas of inquiry may include theoretical approaches to teaching scholarly editing and other forms of digital recovery, the use of primary source materials in the classroom and in public outreach programs, teaching with print editions and/or born-digital projects, and training student members of editorial projects. Collaborative essays are welcome, including those that advance their argument through case studies and with materials such as assignments, course syllabi, and excerpts from students’ work (with appropriate permission).

## Voices and Perspectives: Interviews and Conversations 

We publish transcripts of conversations and interviews with recovery practitioners. Those who wish to nominate subjects for conversations and interviews should consult with the Voices and Perspective section editor [Julian C. Chambliss](mailto:chambl91@msu.edu) with their suggestions.

## Micro-Editions
_Scholarly Editing_ is a home to sustainable small-scale editions of understudied texts. Such editions may range from a single document to 130 short documents or to two variants of a single text. We encourage those who wish to propose a micro-edition to consult the micro-editions co-editors [Raffaele Viglianti](mailto:rviglian@umd.edu) and [Megan Bushnell](mailto:megan.bushnell@ling-phil.ox.ac.uk) in advance of forwarding their proposals.

For further information about technical specifications, content, and house style, see the [“Contributing” page](../contributing) on our website.

Direct all questions about submission and peer review to Managing Editor Robert Riter at [rbriter@ua.edu](mailto:rbriter@ua.edu).

---
path: "/2023editorscall/"
title: "2023 Call for Editors"
---

## _Scholarly Editing: The Annual of the Association for Documentary Editing_

Dear Colleagues,

As the Editors in Chief of _Scholarly Editing_, we write to issue a call for editors and other recovery practitioners. _Scholarly Editing_ seeks to develop and advance all aspects of textual and documentary editing, including the recovery of texts and artifacts that represent and celebrate the lives and contributions from and about Black, Latinx, and Indigenous peoples; Asian Americans and Pacific Islanders, women, LGBTQ+ individuals, and people of the Global South as well as others whose history has been erased, misrepresented, or disregarded. As we strive to diversify the journal’s staff and bring in new voices, we strongly encourage applications from these communities, as well as those who have expertise in the histories and literatures of those groups and peoples. This call reflects our commitment to ensure the journal’s sustainability by cultivating a robust editorial team that will succeed the senior editors over time. We reinforced this pledge in our recent call for contributions for Volume 40, published in 2022. Applications from outside the US are welcome.

_Scholarly Editing_ seeks to fill the following positions on our editorial team, as described on our About website page:
* Reviews Editor (Print and Digital) (1)
* Voices and Perspectives Editor (1)
* College and University Classroom Editor (1)
* Interviews Editor (1)

Editors serve for three-year terms. Because the journal is grounded in higher education’s tradition of service, the work of editors is voluntary and uncompensated.
Editors have two main tasks in the production of each annual volume of _Scholarly Editing_, and both tasks involve spending time on outreach. Each editor’s first task is to cultivate contributions that speak to the rolling call. The editors’ equally important second task is to cultivate peer reviewers who will adhere to the journal’s commitment to generous and developmental peer review. 

New editors can expect to spend roughly twenty hours per month on their work for the journal. Section editors meet monthly to discuss content development plans for their sections. Section editors work routinely with the managing editor to adapt workflows and templates for peer review. Members of the executive editorial team are available for consultation and collaboration as needed. 

To apply, please complete [the application form](https://docs.google.com/forms/d/1EUDAKc6UoasxdN53LXN_8uUj4azc7VVxityAI9gTs9I), which asks for a short statement of interest.

Please feel free to contact us with any questions about _Scholarly Editing_ or the positions that the journal is seeking to fill. 

Please circulate widely.

[Noelle Baker](mailto:noelle.baker@me.com)<br/>[Kathryn Tomasek](tomasek_kathryn@wheatoncollege.edu)

Editors in Chief, _Scholarly Editing_
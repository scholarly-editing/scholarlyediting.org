---
path: "/reviewslist/"
title: "Text Available for Review"
---

## THE CONTENT ON THIS PAGE IS NOT UP TO DATE. PLEASE SEE THE [ROLLING CALL FOR CONTRIBUTIONS](../call).

Scholarly Editing reviews letterpress and digital editions, digital projects, and the digital tools that enhance the recovery of, and expand access to, primary-source materials. In accordance with the journal’s Statement of Purpose, we review materials that amplify the work of diverse voices and celebrate the contributions of underrepresented and silenced communities. While we do not accept unsolicited reviews, we welcome proposals from readers who would like to serve as reviewers as well as recommendations of work that may be appropriate for review. 

## Digital Editions
* [Archival Gossip](https://www.archivalgossip.com)
* [The Digital Exeter Book and Book Hand](https://exeterbookhand.com/)
* [Emma B. Andrews Diary Project](http://www.emmabandrews.org/project/)
* [Freedom on the Move](https://freedomonthemove.org/#team)
* [Hacking the Historical Data: Register of Chinese Immigrants to Canada, 1886-1949](https://osf.io/9zr6f/)
* [Last Seen: Finding Family after Slavery](https://informationwanted.org)
* [Louisiana Slave Conspiracies](https://lsc.berkeley.edu)
* [Memorable Days: The Emilie Davis Diaries](https://davisdiaries.villanova.edu)
* [One More Voice](https://onemorevoice.org/index.html)
* [Suffrage Postcard Project](https://thesuffragepostcardproject.omeka.net/)
* [The Travels of Henrietta Liston](https://digital.nls.uk/travels-of-henrietta-liston/index.html) (1796–1820)
* [When Melodies Gather: Oral Art of the Mahra](https://www.sup.org/books/title/?id=27390)
* [The Winnifred Eaton Archive](https://winnifredeatonarchive.org)

## Letterpress Editions and Monographs
* _[The Digital Black Atlantic](http://upress.umn.edu/book-division/books/the-digital-black-atlantic) _(Roopika Risam and Kelly Baker Josephs (eds.))
* _[Holy Digital Grail: A Medieval Book on the Internet](https://www.sup.org/books/title/?id=30509)_ (Michelle R. Warren) 
* _[The Lady's Magazine (1770-1832) and the Making of Literary History](https://edinburghuniversitypress.com/book-the-lady-s-magazine-1770-1832-and-the-making-of-literary-history.html) _(Jennie Batchelor)
* _[Marginal Notes: Social Readings and the Literal Margins](https://link.springer.com/book/10.1007/978-3-030-56312-7) _(Patrick Spedding, Paul Tankard (eds.))
* _[The Work and the Reader in Literary Studies: Scholarly Editing and Book History](https://doi.org/10.1017/9781108641012)_ (Paul Eggert)


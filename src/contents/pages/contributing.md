---
path: "/contributing/"
title: "Guidelines for Contributors"
---
## Statement on Ethics

### General Statement

The activities of *Scholarly Editing* are guided by the [COPE](https://publicationethics.org/core-practices) (Committee on Publishing Ethics)
Core Practices. *Scholarly Editing* is committed to maintaining transparent editorial operations, the highest standards of academic integrity, robust peer-review procedures, and respectful and constructive engagement with contributors, peer reviewers, and readers.

*Scholarly Editing* is committed to serving as an inclusive intellectual space and is committed to amplifying the work of diverse voices within the fields of scholarly editing and recovery. We particularly encourage contributions from and about Black, Latinx, and Indigenous peoples; Asian Americans and Pacific Islanders; women; LGBTQ+ individuals; and peoples and cultures of the Global South.

We also encourage contributions that engage in decolonialist examinations as well as those that address the publication and dissemination of underrepresented and racialized voices, neglected documentary forms, and innovative digital projects.

### Reviewing Ethics

*Scholarly Editing* maintains robust peer-review procedures concerned with supporting the publication of high-quality contributions. We place emphasis on ensuring that contributors receive constructive and encouraging peer-review reports, with the objective of providing thoughtful feedback and assessment.
 
Peer-reviewers are provided with comprehensive guidelines and reviewing templates to aid in this process. Peer-review reports are carefully assessed by the managing editors and editor in chief to detect any deviations from this practice.

### Ethics for Authors: Authorship and Contributorship

Authorship confers credit and has important academic, social, and financial implications. Authorship also implies responsibility and accountability for published work. _Scholarly Editing_ seeks to ensure there are clear policies in place that allow for transparency around who contributed to the work and in what capacity. In that regard, we follow the [CORE](https://authorservices.taylorandfrancis.com/publishing-your-research/writing-your-paper/ethics-for-journal-authors/) recommendations to ensure that contributors who have made substantive intellectual contributions to a paper are given credit as authors, but also that contributors credited as authors understand their role in taking responsibility and being accountable for what is published. 

## Micro-Editions

If you are interested in editing a small-scale digital edition, we want to hear from you. Such editions may range from a single document to 130 short documents or to two variants of a single text. Micro-editions should be submitted as a proposal and need not be submitted in a finished form.

Technological Specifications: with only rare exceptions, all editions published by *Scholarly Editing* must be in XML (Extensible Markup Language) that complies with TEI (Text Encoding Initiative) Guidelines, which have been widely accepted as the de facto standard for digital textual editing. *Scholarly Editing*’s technical editor will provide support for displaying images and text in the journal’s house style; building simple interactive features around the TEI markup, including—but not limited to—highlights and alternative renderings of the text; and displaying variant readings. Editions that are very complex and require a high degree of customization can be developed either by the authors in coordination with the technical editor, or by the technical editor in consultation with the authors. The *Scholarly Editing* website is a static site and intentionally adopts a minimal technical stack. As a consequence, micro-editions ready for publication will have to work statically as well, meaning that they will have to be composed exclusively of HTML, CSS, JavaScript, and data assets (typically images and TEI XML). When working with the technical editor, authors will be guided to work with TEI and CETEIcean to style their editions with CSS. Authors who prepare their editions more autonomously can use any system as long as the submitted edition is static and conforms to the house style (through coordination with the technical editor).

## Review Procedures

All content published in Scholarly Editing is rigorously reviewed by at least two scholars with expertise relevant to the submission under review. Articles, review essays, and pedagogical materials are subject to a standard double-blind review process. A different procedure is followed for micro-editions. The executive editorial team select promising editions from submitted proposals. For selected editions, the journal's technical editor works with the edition's editor(s) to put the edition online at an unpublished URL. Thereafter, the edition undergoes a double-blind review. Reviewers evaluate the edition's content in addition to its editorial theory and methodology. Reviewers of articles, review essays, pedagogical materials, and micro-editions recommend that the journal accept the submission as-is, accept it only after revision, or reject it.

All reviews are shared with contributors after evaluation by the editors in chief to ensure their compliance with the journal's ethical standards for peer review. Submissions that are accepted as-is or after appropriate revision are edited by one or more members of the editorial board as well as the journal's copy editor before publication in Scholarly Editing.

In most cases, contributors will be notified of the status of their manuscript within 6 to 8 weeks.

## Submission Procedures

1. Contributions are accepted through the Scholarly Editing Open Journal Systems platform: https://submissions.scholarlyediting.org.

2. Contributors will first register with Scholarly Editing, which will permit them to submit their work and monitor the review process.

3. All contributors will receive written notification confirming receipt of their submission.

## General Submission Guidelines

Note that Micro-editions only require a proposal and need not be submitted in a finished form. For all other submissions, follow the steps below.

1. To ensure double-blind peer review please remove all identifying information from submission.

2. Please consult the current edition of The Chicago Manual of Style for formatting and style guidelines.

3. Citations should be provided as footnotes and adhere to The Chicago Manual of Style notes system.

	Examples:

    - Hans Walter Gabler, “Theorizing the Digital Scholarly Edition,” Literature Compass 7, no. 2 (2010): 43–56.

    - David Reynolds, “New Manuscript Images Available,” Roman de la Rose blog, accessed July 31, 2012, http\://romandelarose.blogspot.com/2012/03/new-manuscript-images-available.html.

    It is not necessary to use parentheses to enclose the bibliographic material to separate it from the discursive part of the note:

    - As Ed Folsom has written, Whitman “had an ongoing battle with genre,” asserting in titles and elsewhere that the units of text in Leaves of Grass were poems, but pushing the boundaries of that designation. “Database as Genre: The Epic Transformation of Archives,” PMLA 122, no. 5 (October 2007), 1572. Whitman also experimented in books like Two Rivulets with the juxtaposition of prose and verse and the provocation to the reader produced by the structural and semantic relationships between the two.

4. Essay submissions should include a 250-word abstract.

5. Contributors will provide a 500-word overview summarizing their work's content, use of technologies if applicable, context of its place within the field, description of audience, and significance. This statement is a required element of the OJS submission process.

6. Essay submissions should be between 4000-8000 words, including all text [abstract, notes, captions]. Longer essays are also acceptable.

## Formatting Guidelines

Formatting guidelines are organized by manuscript and stylistic components. For additional formatting questions refer to the latest edition of The Chicago Manual of Style.

Abbreviations and acronyms

- Spell out acronyms the first time they appear in the paper.
- Enclose the acronym in parentheses after the spelled-out term only if the acronym is used later in the paper.
- Spell out United States as a noun and use US (no internal periods) as an adjective.
- Use two-letter capitalized state abbreviations in notes: Lincoln, NE.
- Do not use periods in common abbreviations such as “BC.”
- Time of day: a.m., p.m. (lowercase with periods).
- Use “ll.” in parentheses; “lines” outside parentheses.
- “ca.” [circa] is preferred to “c.” (for greater clarity).

Capitalization

- In general, follow CMoS chapter 8  (“Names and Terms”).
- CMoS 7.80 recommends “the web”; “website”; “the Internet.”
- When citing works published in the digital journal Scholarly Editing, all titles are treated as articles (roman within quotation marks).

Captions and callouts to figures and tables

- Capitalize “Figure 1” and “Table 1,” etc., in text and in captions. Do not abbreviate as “Fig.”
- A colon follows “Figure 1” or “Table 1” in the caption.
- Number figures and tables even if there is only one in an article. (If there is only one appendix, however, refer to it as simply “the Appendix.”)
- Figure captions are capitalized sentence-style.
- Table captions are capitalized headline-style. The caption should not be punctuated with a period. 
- Generally, an appendix would be handled like a figure or a table, depending on its elements.
Example of the caption of an appendix that is like a table:
Appendix 2: Comparison of the Use of Journals, Editions, and Facsimiles from 2002 and 2011

Dates

- Use the month-day-year style: July 31, 2012

Headings

- Title of the article is headline-style: Medievalists and the Scholarly Digital Edition
- A-level heads are headline-style: Introduction: A Brief History of Digital Medieval Studies
- Book review headings:

Example of format:

Beverley, Robert. The History and Present State of Virginia. Edited by Susan Scott Parrish. Chapel Hill: University of North Carolina Press for the Omohundro Institute of Early American History and Culture, Williamsburg, VA, 2013. 384 pp. Notes, illustrations, index. ISBN 978-1-4696-0794-8. $45.

The Dividing Line Histories of William Byrd II of Westover. Edited by Kevin Joel Berland. Chapel Hill: University of North Carolina Press for the Omohundro Institute of Early American History and Culture, Williamsburg, VA, 2013. 528 pp. Notes, illustrations, map, chart, appendix, bibliography, index. ISBN 978-1-4649-0693-4. $59.95.

The Eleanor Roosevelt Papers, Vol. 2: The Human Rights Years, 1949–1952. Edited by Allida Black. Charlottesville: University of Virginia Press, 2012. lxv + 1135 pp. ISBN 978-0-8139-3141-8. $125.

Hyphenation

- Close up most words with prefixes, e.g., metadiscipline, interdiscipline (follow hyphenation guide at CMoS 7.84).
- An en-dash is used to express range (e.g, 29–35) or to replace the hyphen in compounds where one part of the compound is two words (e.g., UNC–Chapel Hill).
- antislavery
- anti-war
- pro-war
- re-present, re-presentations
- e-book

Numbers

- Spell out numbers from one to one hundred; use numerals for 101 and above. (This is an adaptation of CMoS 9.2 through 9.4)
- Use numerals with the word “percent” (not %): 42 percent
- Spell out “dollars” when the number is spelled out; use “$” with numerals.
- Abbreviate inclusive page numbers (CMoS 9.61): Example: 110–29 (not 110–129).
- Abbreviate a span of two years (1862–63) but otherwise repeat all digits (1824–1863).

Punctuation

- Use the serial comma.
- Ellipsis points (. . .) are normally not used before or after a quotation.
- It isn’t necessary to change straight quotation marks and straight apostrophes to curly, or vice versa. Instead, seek internal consistency within individual articles and editions. (However, sometimes the editors will change curly quotation marks and apostrophes to straight ones for technical reasons; if in doubt about a particular article or edition, ask the editors.)
- Use en dash to express range, as in page numbers: 110–15.

Quotations

- Set off quotations that are longer than 4 lines.

Spelling

- When words vary in the way they are spelled or hyphenated, follow Merriam-Webster Collegiate Dictionary (11th ed.).

Examples:
CD-ROM
long-standing
toward (towards is chiefly British)
e-mail, e-book
website
Wikipedia (CMoS considers this analogous to a printed work. CMoS 8.191)

Titles of websites

- General titles of websites should be set in roman, headline-style, without quotation marks: Canterbury Tales Project
- Titled parts of a website should be placed in quotation marks.
- Titles of the types of works analogous to printed works should be treated the same whether they are published in print or online: The Electronic Beowulf

## Third Party Copyright Permissions

1. All contributors are responsible for ensuring that they have acquired any necessary permissions to use material created by a third party.

## Peer Reviewer Guidelines

Scholarly Editing welcomes contribution through participation in the peer review process. Individuals interesting in serving as peer reviewers may register within the OJS platform: https://submissions.scholarlyediting.org.

Peer reviews are expected to adhere to the journal’s ethical standards. Review reports will include detailed and constructive assessments and recommendations. Reviewers are asked to avoid using unprofessional language, ad hominem arguments, and attacks of any kind.

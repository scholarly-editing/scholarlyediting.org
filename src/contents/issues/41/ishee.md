---
title: '<em>History UnErased</em>: An Interview with Deb Fowler and Kathleen Barker'
authors:
  - first: Jenifer
    last: Ishee
    affiliations:
      - Connecticut College
  - first: Robert
    last: Riter
    affiliations:
      - University of Alabama
issue: Volume 41
group: 'Voices and Perspectives: Interviews and Conversations'
group_order: 1
doi: 10.55520/HBE3K7AM
keywords:
  - LGBTQ
  - K-12
  - Curriculum
  - intersectional
  - history
description: >-
  This conversation by Scholarly Editing Co-Managing Editors, Robert Riter and
  Jenifer Ishee features a discussion with Debra Fowler and Kathleen Barker of
  History UnErased, the “nation's premier provider of K-12 LGBTQ-inclusive and
  intersectional US history curriculum.” In this conversation, Fowler and Barker
  detail the impetus behind the founding of the organization, some of the work
  they have already accomplished, and their goals for the future. They explain
  how they utilize primary source material culled from the archives to allow
  teachers to engage students in conversations aimed at including LGBTQ
  individuals in the historical narrative. They also hope to connect broader
  audiences with these primary source materials.
audio:
  file: historyunerased.mp3
  transcript: historyunerased.vtt
  chapters: historyunerased_chapters.vtt
---

This conversation by _Scholarly Editing_ Co-Managing Editors, Robert Riter and Jenifer Ishee features a discussion with Debra Fowler and Kathleen Barker of [History UnErased](https://unerased.org/), the “nation's premier provider of K-12 LGBTQ-inclusive and intersectional US history curriculum.”

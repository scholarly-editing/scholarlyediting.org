---
title: >-
  Reading Prison through the Penal Press: <em>American Prison Newspapers,
  1800s–Present: Voices from the Inside</em> and the <em>Prison Journalism
  Project</em>
authors:
  - first: Sally
    middle: F.
    last: Benson
    affiliations:
      - Penitentiary of New Mexico
issue: Volume 41
group: Reviews
group_order: 5
doi: 10.55520/W196AJ1Y
keywords:
  - prison journalism
  - incarceration
  - censorship
  - digitization
description: >-
  This piece reviews American Prison Newspapers, 1800s–Present: Voices from the
  Inside and the Prison Journalism Project.
---

_American Prison Newspapers, 1800s–Present: Voices from the Inside_, https://www.jstor.org/site/reveal-digital/american-prison-newspapers/; _Prison Journalism Project_, *https://prisonjournalismproject.org/

<div style="border: 1px solid #dc3522; padding: .5em">
<svg style="margin-right: .2em; top: .125em; position: relative; width: 1em; height: 1em;" focusable="false" aria-hidden="true" viewBox="0 0 24 24" aria-label="info icon"><path d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"></path></svg>
This review was updated on July 17, 2024 to correct the author's affiliation.
</div>

When people ask me what I do for a living, I tell them I teach in a men’s maximum-security prison. Their reactions range from “God bless you” to “How can you work with those criminals?” Despite these disparate viewpoints, the public holds a voyeuristic fascination with prison, as evidenced by popular television shows such as *Orange Is the New Black*, *Prison Break*, or *Wentworth*, to name a few. Outside viewers, hoping to glimpse prison’s inner world, too often see one-dimensional depictions. Without having lived or worked in prison, or knowing someone who has, many people have little knowledge of the realities of prison or, importantly, of who is in prison. A growing number of prison blogs, podcasts, literary journals, and other forms of prison writing have responded to a public hungry to know more of the experience of prison *as told* by those with first-hand experience. An often-overlooked genre of prison writing is prison journalism.

Prison journalism, historically known as the penal press, began in a New York debtors’ prison in 1800 with the newspaper *Forlorn Hope*. By the 1950s and into the 1970s, hundreds of prison newspapers circulated beyond their home institutions to other prisons across the country and to public readers, including state officials, penologists, sociologists, and private citizens. Prison administrators supported prison newspapers for their rehabilitative potential to provide incarcerated people literacy and vocational skills while informing readers of prison events and programs. By 1965, prison journalism’s overwhelming popularity prompted Southern Illinois University’s journalism department to organize an annual [American Penal Press Contest](https://journalism-history.org/2022/03/21/diversity-essay-the-american-penal-press-contest-and-the-cultivation-of-prison-journalism-1965-1991/), which continued until 1990, legitimizing the fourth estate of prison journalism.[^1]

In 2015 I left my job teaching at the Penitentiary of New Mexico (PNM) to go to graduate school at the University of Arizona. During my first year in Tucson, an archivist friend at the New Mexico State Archives discovered a box of old issues of a newspaper called the *Enchanted News*, published by individuals incarcerated at PNM. Although I had worked for three years at the prison, I had never heard of the newspaper. From an overwhelmingly oppressive environment, what “news” did the incarcerated journalists report? I went back to New Mexico to find the box.

The *Enchanted News* started as the *N.M.S.P. News in 1956*, two weeks before the prison in downtown Santa Fe relocated to a new facility. By 1957, the newspaper was renamed and formatted as a magazine. It circulated among prisons and public readers across the United States until 1971, winning best news magazine in the 1970 American Penal Press Contest.

As I read the *Enchanted News*, an interior world of New Mexico’s prison of the past came to life in sharp contrast to the prison I knew. News of plays, movies, sports, education, and work programs at the prison filled the pages, along with short stories, editorials, cartoons, and more. I was particularly interested in the historical educational programs and surprised to read that a satellite college campus once existed inside the prison. Over the next several summers, I traveled to New Mexico to locate and read the entire newspaper collection, to find documents relating to the prison’s *Penitentiary Community College of Santa Fe*, and to piece together a forgotten world of PNM.

<hr/>

By the 1980s, tough-on-crime attitudes, stricter sentencing laws, and overpopulated prisons coincided with the near extermination of the penal press. Many prison administrators heavily censored or shut down their publications. Some incarcerated journalists filed complaints in court to establish the right to keep publishing.

Despite the suppression and shrinking of the penal press, thanks to the [*American Prison Newspapers, 1800s–Present: Voices from the Inside*](https://www.jstor.org/site/reveal-digital/american-prison-newspapers/?searchkey=1710250288844&so=item_title_str_asc) (APN) archive, researchers, educators, historians, and others now have access to a partial window into prisons’ interior world. Reveal Digital and JSTOR, under the nonprofit organization [ITHAKA](https://www.ithaka.org/#what-we-do), have digitized hundreds of newspapers published from inside prisons across the United States.[^2] ITHAKA’s mission to make education and knowledge more accessible now includes providing free digital access to an archive of prison newspapers spanning two centuries. Had the digital archive been available at the time of my research, access to much of the *Enchanted News* would have been significantly easier!

*APN* began development in 2020 with contributions from libraries and access to individual and institutional archival collections. A $500,000 grant from the Andrew W. Mellon Foundation supported the project’s development and jump-started its transition to open access in 2021. This primary source collection continues to grow.

How is this ambitious archive organized? With over 13,000 digitized items representing hundreds of prison newspapers dating back to the 1800s, the *APN* site can seem overwhelming. Thanks to the archive’s search tools, users may search the entire collection within a specific date range or sort publications by oldest, newest, or title.

At the time of this writing, the archive’s oldest prison-published newspaper is the *Monthly Record*, published by individuals incarcerated at the Connecticut State Prison. Its inaugural issue, “still in the swaddling clothes of its existence,” first appeared in October 1897.[^3] Contributing writers are identified by number and initials (e.g., No. 13 E. S.). The archive’s newest serial publication, *First Amend This!,* is written and published by incarcerated writer Patrick Irving from inside an Idaho State Correctional Institution. Irving launched a website in 2019, [Book of Irving \#82431](https://bookofirving82431.com/), which links to his newsletters and other writing and aims to “provide others with a window into the experience of incarceration and to provide Patrick with a vehicle to venture beyond his prison cell.”[^4]

JSTOR Daily invites individuals impacted by incarceration and others to engage materials in the *APN* collection and send proposals for articles that bring attention to the archive and to prison journalism. Educators interested in teaching with these original sources have free access to hundreds of prison newspapers in one collection. Suggested lesson plans and links to primary and secondary sources within the JSTOR collection at large complement the *APN* archive. JSTOR’s “[Text Analyzer](https://daily.jstor.org/how-to-teach-with-jstor-text-analyzer/)” helps students search for possible sources.[^5] These resources are not directly linked to the *APN* archive page, however, which makes finding them challenging. The JSTOR, JSTOR Daily, Reveal Digital, and ITHAKA umbrella sites lead to articles, teaching and research tools, and the call for submissions relating to the *APN* archive.

<hr/>

In 1963, during the height of the penal press, writer James F. Fixx interviewed prison newspaper editors around the country for *Saturday Review*. Harold Stroup, editor of the *Pendleton Reflector* (Indiana Reformatory) stressed that “these men are trying to reach the public through the only avenue of communication open to them, the prison press.” Walter Faherty of the *Mentor* (Massachusetts Correctional Institution) echoed Stroup, saying, “\[W\]e always have the enlightenment of the public in mind.”[^6] Writing as members of a professional community, the penal press, incarcerated journalists disrupted stereotypes of incarcerated people through their publications. Millions of incarcerated individuals since have systematically been denied this right.

When California courts sided with *San Quentin News* journalists’ fight for freedom of speech in the 1980s, the California Department of Corrections and Rehabilitation responded by suspending the newspaper for more than twenty years.[^7] *San Quentin News* resurfaced in 2008 and is one of about two dozen currently published prison news publications listed in the [*Prison Journalism Project*](https://prisonjournalismproject.org/) (*PJP*) online prison newspaper directory.[^8]

Founded in 2020 by veteran journalists Yukari Kane and Shaheen Pasha, *PJP* is a nonprofit organization working to keep prison journalism alive. *PJP* “empowers incarcerated individuals, who are talked about but are rarely heard from, to be a part of this conversation.” The project aims to create a national network of prison journalists who, through their reporting, change the narrative of mass incarceration by bringing transparency to it.

Toward this goal, *PJP* offers incarcerated writers a training and publishing model. Individuals interested in submitting writing to *PJP* first receive journalism training tips, handouts, and copies of *PJP Inside*, the organization’s newspaper. Writers who develop strong enough skills are invited to be contributors. Once contributors demonstrate a strong body of work, they may be eligible for *PJP’s* J-school correspondence program. The program guides writers within a journalism cohort and provides opportunities to publish.

*PJP*’s website features a wide variety of writing. Under the “Stories” tab, subcategories include News, Prison Report, Opinion, Perspective, Collections, Poems, Art and Illustrations, and writing organized by topic. Each section describes the respective category of contributors. *PJP* also republishes articles directly from prison newspapers. In addition to news features, *PJP* invites individuals with direct experience of prison, including formerly incarcerated individuals, their family members, and even prison staff, to submit personal essays, opinion pieces, narrative poetry, artwork, and more. Their focus is on journalism, incarceration, and the criminal justice system.

*PJP* has received strong support and recognition through grants from the John S. and James L. Knight Foundation, FWD.us Education Fund, and the Just Trust. Partnering with the Society of Professional Journalists, *PJP* has created a national virtual chapter of incarcerated journalists. In 2022, *PJP*’s own publication, *PJP Inside*, received the Institute for Nonprofit News Community Champion Award and the Society of New Design Award of Excellence. *PJP* founders Kane and Pasha share a proven track record of supporting journalism that brings transparency to prison and the criminal justice system. Most journalism about prison is produced outside prisons. *PJP* strives to ensure a pathway for incarcerated individuals to participate in a public discourse about prison reform from the inside.

<hr/>

Thanks to the *Enchanted News*, the forgotten historical prison college program in New Mexico became a major focus of my doctoral research. Incarcerated residents at the prison once sat in college classrooms. Many earned multiple degrees, left the prison, and never returned. This historical knowledge was nearly lost, and what was once a reality at PNM no longer seems possible. As I traced the prison’s educational programs through the newspaper, another narrative surfaced about how US justice policy shaped the New Mexico Corrections Department and the prison climate leading up to a violent riot at PNM in February 1980. Without that knowledge, I would not understand the connection between public mistrust in rehabilitation, reduced legislative support for new programs, and growing unrest inside the prison.

Although publishing under surveillance, incarcerated writers joined a public discourse through the penal press, responding to issues related to incarceration and rehabilitation from an inside vantage point. Using the *APN* archive, I recently found an editorial in the May–June 1980 issue of the *Angolite* (Louisiana State Penitentiary). The incarcerated editors (award-winning prison journalists Wilbert Rideau and Billy Sinclair) claimed the Penitentiary of New Mexico’s 1980 riot would fuel public mistrust of people in prison: “\[A\]s prisoners, we all will ride the rap for its senseless violence.”[^9] They used news of the riot to issue a call to action for incarcerated people across the country to walk away from the historical convict code, or “criminal ethic” of violence. Reporting on prison violence was taboo for penal press journalists and could result in losing their publication. Not surprisingly, I was hard-pressed to find news of the violent riot in New Mexico in other newspapers from 1980. Through their ingenious approach, turning a tragedy into a call for change, the *Angolite* editors published their piece.

Prison journalism from the inside offers those on the outside a portal through which to better understand a private world that is otherwise nearly invisible and unreachable. While exploring the *PJP* website, I discovered an [essay](https://prisonjournalismproject.org/2023/12/07/new-mexico-lifers-rebuffed-pursuit-education/) written by a former student of mine.[^10] Although I had seen prior drafts of the piece, the *PJP* online version linked directly to the statutes and policies he referenced in his writing. He has presented a strong argument for making education more accessible to incarcerated individuals with life or long-term sentences in New Mexico. His essay demonstrates his credibility as someone directly impacted by these policies and the need for incarcerated journalists’ voices to be heard by public audiences. I look forward to seeing more of his writing.

The *APN* collection and *PJP* website, largely comprised of original sources written by and for incarcerated people, demonstrate prison journalism’s power to create community archives of narratives of incarceration. Many prisons of the past bustled with education, work, and vocational programs. Through the historical penal press, we witness the arc of incarceration shifting from rehabilitation to punishment. Current prison journalism informs us of pressing issues concerning those inside our prisons today. With this knowledge, shared by experts of incarceration, we can advocate for prison reform alongside the incarcerated journalists.

[^1]: Kate McQueen, “Diversity Essay: The American Penal Press Contest and the Cultivation of Prison Journalism 1965–1991,” *Journalism History*, March 21, 2022, <https://journalism-history.org/2022/03/21/diversity-essay-the-american-penal-press-contest-and-the-cultivation-of-prison-journalism-1965-1991/>.

[^2]: *Reveal Digital, American Prison Newspapers, 1800s–Present: Voices from the Inside*, JSTOR Daily, <https://about.jstor.org/revealdigital/american-prison-newspapers/>; JSTOR, “About JSTOR,” <https://about.jstor.org/>; and ITHAKA, “What We Do,” <https://www.ithaka.org/#what-we-do>. All accessed December 15, 2023.

[^3]: Editorial, “To Our Friends,” *Monthly Record* (Connecticut State Prison) 1, no. 1 (October 30, 1897): 2, *American Prison Newspapers*, <https://www.jstor.org/stable/community.31696924>.

[^4]: Patrick Irving, _Book of Irving #83431_, https://bookofirving82431.com/.

[^5]: Rachel Herrmann, “How to Teach with JSTOR Text Analyzer,” *JSTOR Daily*, April 25, 2019, <https://daily.jstor.org/how-to-teach-with-jstor-text-analyzer/>.

[^6]: Quoted in James F. Fixx, “Journalists Behind Bars,” *Saturday Review*, March 9, 1963, 63.

[^7]: Kevin Sawyer, “The Visionary Who Resurrected San Quentin News,” *San Quentin News* (San Quentin State Prison), December 11, 2019, <https://sanquentinnews.com/the-visionary-who-resurrected-san-quentin-news/>.

[^8]: *Prison Journalism Project*, “Prison Newspaper Directory,” accessed December 15, 2023, <https://prisonjournalismproject.org/prison-newspaper-directory/>.

[^9]: Wilbert Rideau and Billy Sinclair, *editorial,* “The Criminal Ethic,” *Angolite* (Louisiana State Penitentiary), May–June 1980, 16.

[^10]: Angelo Sedillo, “New Mexico Lifers’ Requests for Education Rebuffed—Year After Year,” *Prison Journalism Project*, accessed December 20, 2023, <https://prisonjournalismproject.org/2023/12/07/new-mexico-lifers-rebuffed-pursuit-education/>.

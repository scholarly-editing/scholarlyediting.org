---
title: >-
  <em>Incidents in the Life of a Slave Girl</em>. Harriet Jacobs. Edited by
  Koritha Mitchell
authors:
  - first: Amy
    last: Larrabee Cotz
    affiliations:
      - American University
issue: Volume 41
group: Reviews
group_order: 2
doi: 10.55520/5H6ETRXS
keywords:
  - autobiography
  - slavery
  - women's history
  - nineteenth century
  - review
description: >-
  This piece reviews Jacobs, Harriet *Incidents in the Life of a Slave Girl*,
  edited by Koritha Mitchell
---

Jacobs, Harriet. *Incidents in the Life of a Slave Girl*. Edited by Koritha Mitchell. Ontario, Canada: Broadview Press, 2023. 347 pp. Notes, illustrations, appendices. ISBN 978-1-5548-1502-9.  
$17.95.

The latest edition of Harriet Jacobs’s (1813–97) *Incidents in the Life of a Slave Girl,* edited by Koritha Mitchell, is not to be missed. Mitchell, a literary scholar whose research focuses on African American literature, notes that while there are “many English-language editions of *Incidents*,” hers presents the context necessary for readers to appreciate the “monumental achievement” it represents (52). Indeed, Mitchell brings fresh perspective to *Incidents*, the first book-length autobiography of a formerly enslaved African American woman.

For anyone unfamiliar with Jacobs’s narrative, it is nothing short of revelatory. It chronicles her life as a child born into slavery in the American South, her eventual attainment of freedom, and the beginning of her life in the North. Jacobs was forced to navigate an institution meant to deny not only her freedom but also her dignity; she describes her survival despite being sexually terrorized for years by her enslaver. Around the age of twenty-two, Jacobs successfully hid herself away in an attic crawl space that she inhabited for nearly *seven years* before she escaped to freedom. And once in New York, Jacobs faced the ongoing need to hide in plain sight, especially after the passage of the Fugitive Slave Act in 1850. As Mitchell notes, “when slavery was the law of the land, not every white man was an enslaver and not every African-descended woman was enslaved, but every life was touched by the forces that made slavery possible” (12). Mitchell’s edition boasts not only a rich introduction but also a chronology, an annotation of the original narrative, and a series of appendices providing historical context, a look into Jacobs’s publication journey, and her work as an activist.

Mitchell’s insightful and thought-provoking introduction begins: “*Can you get pregnant*? The answer to that question just might shape your life” (11). In this way she encourages readers to self-reflect: to pause for a moment and ruminate about the body they inhabit before approaching Jacobs’s story. Mitchell then delves into a discussion of the double burden of racism and sexism Black women were forced to navigate while living in a society whose economic fortune recognized their “sexual violation as a necessary non-event” (13). Indeed, in the years that African Americans “were legally considered chattels, those who could become pregnant were treated as breeding animals that just happened to resemble human beings” (11). Moving beyond the intersection of sex and race, Mitchell compares Jacobs with Black luminaries such as Sojourner Truth, Frederick Douglass, and Harriet Tubman, deftly orienting Jacobs’s experiences within the literary and activist framework of her time. She encourages her readers to recognize the uniqueness of Jacobs’s experience as a Black female author, noting, “surviving slavery in a child-bearing body was a feat . . . to write and publish about one’s survival was nearly unheard of, but Harriet Jacobs did precisely that” (15).

The introduction is followed by a broad yet concise chronology that situates the focal points of Jacobs’s life among a variety of events that impacted her experiences. Mitchell notes not only Jacobs’s time as an enslaved woman and her path to freedom but also her roles as an author, self-publicist, and activist, as well as her interactions with figures such as Harriet Beecher Stowe, Charlotte Forten Grimké, and William Lloyd Garrison. Onto this she overlays the social, political, and literary history that played out during Jacobs’s lifetime.

Mitchell’s annotation of Jacobs’s narrative allows for a deeper, more nuanced understanding of the text. Unfortunately, however, she does not describe her methodological approach; she simply notes that the edition reproduces the narrative with “extensive footnotes, clarifying anything that is unfamiliar” (52). She defines antiquated language, as when, for example, Jacobs used the word “plaguy” (I don’t mind admitting I would have had to look this up); the footnote reads “excessively” (92). She notes where Jacobs misremembered facts, such as the spelling of James Hamlet’s surname; he was the first man in New York who was, as Jacobs put it, “given up by the bloodhounds of the north to the bloodhounds of the south” after the passage of the Fugitive Slave Act (245). As Jacobs used pseudonyms for most individuals mentioned in her narrative, Mitchell notes when, as with Hamlet, Jacobs did *not* employ pseudonyms. And at times Mitchell’s annotation moves well beyond clarifying language and spelling. For example, Jacobs wrote about encountering “patrols” one night when away from the plantation, and Mitchell explains this then-common practice of white men looking for enslaved individuals who were out without permission. Going further, Mitchell notes that “activists today remind Americans that the police evolved out of this precise dynamic of racial surveillance built on white license to punish Black mobility” (151).

Following her annotation, Mitchell curates a series of six appendices featuring everything from contextualizing historical documents, so readers may better understand the complexity of the world Jacobs inhabited, to images of a few individuals who were significant in her life, including Dr. Norcom (Jacobs’s enslaver for some seventeen years). Mitchell chooses a wide array of materials, including text from the Laws of Virginia, Act XII (1662), the Fugitive Slave Act (1850), and the Emancipation Proclamation (1863). Perhaps most interesting to this reviewer, however, are the appendices comprising a variety of Jacobs’s letters, newspaper articles she authored, contemporary reviews of *Incidents*, and remembrances upon her death. The correspondence includes six letters between Jacobs and her friend Amy Post, an antislavery and women’s rights activist who encouraged Jacobs to pen her autobiography, and two from Lydia Maria Child, the white woman who edited *Incidents* and wrote the original introduction.

Mitchell’s edition of *Incidents* adds to the extensive existing scholarship on Jacobs and her narrative in a meaningful and relevant way. By bringing together the original narrative, correspondence relating to its conception and publication, and a variety of historical documents, Mitchell builds on the work of others, including renowned Jacobs scholar Jean Fagan Yellin.[^1] While Mitchell’s edition would benefit from an index and perhaps a brief biographical directory of the most prominent individuals in Jacobs’s life, these are certainly not necessary. Mitchell’s edition, in its entirety, is akin to having a course in African American literature, women’s studies, and slavery at your fingertips.

[^1]: Yellin, who died in the summer of 2023, reestablished Jacobs (not Lydia Maria Child, the white activist who served as Jacobs’s contemporary editor) as the author of *Incidents*. Yellin’s seminal 2004 book, *Harriet Jacobs: A Life*, details her research into the authorship of *Incidents*.

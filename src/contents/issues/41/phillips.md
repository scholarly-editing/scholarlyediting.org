---
title: '<em>Archival Gossip: A Scholarly Take on Nineteenth Century Tattletales</em>'
authors:
  - first: Chelsea
    last: Phillips
    affiliations:
      - Villanova University
issue: Volume 41
group: Reviews
group_order: 4
doi: 10.55520/TCXNXRWS
keywords:
  - gossip
  - nineteenth century
  - women
  - celebrity
  - journalism
description: >-
  This piece reviews Archival Gossip: A Scholarly Take on Nineteenth Century
  Tattletales, edited by Katrin Horn
---

*[Archival Gossip: A Scholarly Take on Nineteenth Century Tattletales](https://www.archivalgossip.com/)*, ed. Katrin Horn.

One afternoon in 1857, in the middle of a fight with lover Matilda Hays over the sculptor Emma Stebbins, famed actress Charlotte Cushman ate a letter to prevent Hays from reading it. This, naturally, only spurred on the fight between the two, who threw things at each other, ripped clothing, and generally behaved as two “drunken washerwomen” or “gladiators.”[^1] The fight—certainly not their first, according to the maid, Sallie—was the end of their relationship, and signaled Hays’s exit from the circle of friends and expatriate writers and artists around Charlotte Cushman then making their home in Rome. This evocative story was recorded decades after the event in a private diary by journalist Anne Hampton Brewster, who heard it from sculptor Harriet Hosmer, who had been present that day (and for whom Hays had briefly left Cushman in 1853).

If this saga sounds like it belongs on Bravo, you’re not wrong.

*Archival Gossip* stems from the DFG-funded research project “The Economy and Epistemology of Gossip in Late Nineteenth- and Early Twentieth-Century US-American Literature and Culture,” a literary and cultural studies project led by Dr. Katrin Horn (University of Bayreuth, 2019–2022).[^2] A monograph connected to the project, *Gossip in 19th-Century US-American Culture and Literature*, is under contract with Edinburgh University Press.

Focusing on late nineteenth- and early twentieth-century US-American culture, which Horn and her team identify as characterized by “crucial changes in the conception of privacy, publicity, speculation, and risk,” the investigators situate gossip as both a way of knowing and as “social speculation with financial stakes.”[^3] In the larger project, Horn asserts that “as women are unable (or at least limited in their ability) to participate in the marketplace, their private lives become themselves ‘economic’ and many women become speculators in the stock of their own marriageability and other forms of social capital. In this constellation, gossip—as the seed of rumor and scandal, and as a source of otherwise unavailable or unshareable knowledge—emerges as a prime commodity.” This move of gossip from a more (though far from exclusively) oral and localized phenomenon into an increasingly international commercial print market is, Horn asserts, both an important part of women’s ability to access financial success and independence, and a spur to the familiar gendered critiques of gossip and nineteenth-century attempts to shield men from its effects by leveraging and attempting to codify rights to privacy.

The digital outlet, *Archival Gossip*, includes a collection of digitized primary source material, secondary commentary, data visualization tools, and a blog documenting the researchers’ process. It illuminates both the micro and macro scales of gossip’s networks. Drawing on Joseph Roach’s articulation of “public intimacy” as a crucial mechanism of celebrity,[^4] *Archival Gossip* allows us to look at gossip in both public and private contexts, seeing its manipulation and contribution to celebrity and profit.

Actress Charlotte Cushman, the dominant focus of the digital collection and one of the main entry points into the wider study, is an apt subject for this inquiry as there is considerable access to her immediate social networks in both private writings and journalistic communiqués. The latter, written for a broad public, aimed to give the impression of intimate, private access to Cushman’s life, even as the information traveled across countries and continents. Cushman, in removing herself to Rome and joining a like-minded community, helped control what information about her private life and experiences made it into the papers, and how she was perceived—as a dashing, romantic figure well worth the fortune she made, but never so overtly queer or scandalous as to lose her (literally) valuable reputation.

The opportunity to look closely at identity and persona across the public-private divide is truly fascinating and helpful to those interested in celebrity culture, print media, journalism, women’s and queer history, and more. The nearly one thousand items in the digital archive span realist fiction, periodicals, life writings, and more, and can be searched broadly or by curated collections. Sources are drawn from a variety of archives, recreating and making visible networks and threads of connection otherwise siloed.

The creators have made their work and the decisions that undergird the archive fully transparent in an “[Annotations and User Guidelines](https://www.archivalgossip.com/sources/annotation-and-user-guidelines-archival-gossip-collection/)” document. This covers the software being used, standards and conventions applied to the material, and the various ways the data can be used, now or in the future. This page also clarifies how the project discusses gender, sexuality, and relationships with regard to both historical and contemporary understandings of the complexities involved. This includes the decision to use, for example, the “in a relationship with” tag in the plug-in [AvantRelationships](https://omeka.org/classic/plugins/AvantRelationships/) to keep the exact nature of, for example, Cushman’s myriad relationships with women as widely defined as possible, allowing users to investigate through the archival material for more details.

Beyond Cushman, there are several other collections that facilitate broadly targeted research. “[Gossip Columns and Columnists](https://www.archivalgossip.com/collection/collections/show/3)” allows users to explore how gossip came to occupy greater real estate in magazines and periodicals in the late nineteenth century. A related exhibit, “[Gossip and/as Foreign Correspondence](https://www.archivalgossip.com/collection/exhibits/show/gossip-in-print),” takes the work of Anne Hampton Brewster and Grace Greenwood as a case study, presenting both their private correspondence and the journalism they produced for public consumption.

Tagging facilitates another way of organizing the information in the archive. For example, the “Black Periodicals” tag offers forty-five items from a number of publications and two major figures (Lillian A Lewis and Gertrude Mossell), whose networks and lives can be further explored in and around the digital exhibit “[Gossip in Black Periodicals](https://www.archivalgossip.com/collection/exhibits/show/black-periodicals).” Gossip’s function to help equalize power imbalances, and to clue folks in on information that may impact their safety, meets these journalistic outlets in interesting ways—often serving, in the example of the Black periodicals tagged here, to provide information about achievements and discrimination that also illuminates the accessibility of locales, institutions, and societies.

The primary sources are usefully accompanied by transcriptions, archival locations, basic biographical information, and commentary on the figure or figures involved culled from secondary texts. In the case of periodical material, entire pages of the periodical are provided, allowing one to see the items of gossip in situ alongside the other information.

Each major figure whose life and/or work are highlighted is further explicated with tools for visualizing their social networks and relationships. Charlotte Cushman, for example, is connected to other figures and localities with tags such as “performs at,” “business partners with,” and “in a relationship with.” For me, the “in a relationship with” map immediately activated the iconic image of Alice Pieszecki’s [The Chart](https://the-l-word.fandom.com/wiki/The_Chart) from Showtime’s *The L Word*.

Timelines offer another way of organizing data, a macro view that helps situate items in their chronological relationships, while maps give a sense of the geographical reach of Cushman’s performances, celebrity, and personal networks. In this particular case, 725 items have been geolocated to show where she acted, where she sent and received correspondence, and where she lived. I appreciate the multiple ways the site offers of visualizing Cushman’s life, career, and impact. She certainly was very famous on both sides of the Atlantic, but these resources let us see what that actually looked like in terms of her physical movements: how and where audiences might encounter her in person, and where they did not.

These tools testify to the thoughtful organization of the archive, the paths one might take into and through the material—from item to person to relationship, from person/relationship to item—and how the presentation of data visually and spatially might serve various users.

As a theatre historian, I couldn’t resist spending most of my time with Cushman.

I click on “Charlotte Cushman” in a featured item box on the “[Items](https://www.archivalgossip.com/collection/items/browse)” page and am taken to a brief biography, commentary from secondary sources, images, and an interactive map of relationships with both people (siblings, lovers, and so forth) and places (“performed at”) that allows me to follow out various threads of interconnectivity represented by items in the archive. Landing on another person, I find more details that situate them beyond their specific relationship to Cushman; however, the Cushman relationship remains the focus since it is often what brings them into the archive’s orbit. Letters between Cushman and these various subjects, or that mention these subjects, are the bulk of what I find. But playbills, newspaper articles, images, and more are also plentiful.

I felt a tenuous thrill looking into correspondence that speaks openly of Cushman’s romantic relationships with women. The triumph and validation of being able to show, beyond all doubt, the presence of queer figures, queer love, queer gossip, queer *normalcy* in a historical setting is powerful and valuable. And setting these artifacts alongside secondary sources that have effaced or ignored Cushman’s queerness is particularly impactful. So, too, are the questions and responses it raised in me as I went, allowing me to reflect on questions of privacy, identity, and materiality in my own work.

The blog is an element I highly recommend to those interested in the process of research as well as its outcomes, as Horn and her team are excellent storytellers: We meet the gloriously dashing Hattie Hosmer, pounding through the Italian countryside on a fox hunt, staying out until the small hours at parties, and pricking the egos of male sculptors. To her defense come her fellow expatriates, publishing odes to her energy, vivaciousness, and muscularity. We read Grace Greenwood’s paeon to Cushman’s Romeo, who “grasps you, holds you, and conquers you . . . storms upon you with all the fire and flood of maddened love.”[^5] The bits of publicity were an intentional strategy of Cushman’s circle, whose members defended and uplifted the careers and reputation of their members. (This practice resonated with another recent project, Joanna Scutts’s *Hotbed: Bohemian Greenwich Village and the Secret Club that Sparked Modern Feminism*, 2022).

The entries do multiple work—presenting interesting tidbits from history, bringing together public and private information, and providing metacommentary on the work of the archivists: a story of a pet squirrel highlights the work of the transcriber, for example, while Cushman’s many love letters (mostly to Emma Crow, who eventually married Cushman’s nephew and by him had the children Cushman considered theirs) reveal the complex intimacies of her multiple partnerships, which the curators then attempt to honor in their selection and use of tags relating to relationships.

This project’s illustration of the epistemology of gossip activates both our intellectual understanding and the eager, bodily responses to shared private knowledge. The researchers’ reflections on their blog offer us access to process and evolution of understanding, as well as the personal interests and thrills that helped undergird the work. The still-available contact and feedback form facilitates the readers’ participation in the exchange of information and response. We can gossip back, if we wish.

Indeed, in the course of writing this review I found myself repeating, to everyone I encountered, the story of Cushman eating a letter to prevent Matilda Hays from reading it. This piece of gossip, originally shared in conversation between Anne Hampton Brewster and Harriet Hosmer some decades after the event, made its way into Brewster’s private diary, into this digital archive, and then became part of my own gossip, used to make and celebrate connections to the people in my own life. Cushman may not be as famous now as she was then, but her status as theatrical celebrity and queer icon still operated effectively to heighten interest in my community of scholars and friends. In short, the project successfully makes its case, both academically and through the more visceral, familiar, and intimate feelings it reflects and evokes.

[^1]: Anne H. Brewster, “*Diary Entry by Anne H. Brewster about the Breakup of Charlotte Cushman and Matilda Hays,”* June 5, 1876, Library Company of Philadelphia, <https://www.archivalgossip.com/collection/items/show/36>.

[^2]: DFG is the Deutsche Forschungsgemeinschaft, or German Research Foundation.

[^3]: This and the subsequent quotation in this paragraph are from the project description on the *Archival Gossip* home page, accessed July 28, 2023, <https://www.archivalgossip.com/>.

[^4]: Joseph Roach, “Public Intimacy: The Prior History of ‘It,’” in *Theatre and Celebrity in Britain, 1660–2000*, ed. Mary Luckhurst and Jane Moody, 15–30 (London: Palgrave Macmillan, 2005), https://doi.org/10.1057/9780230523845\_2.

[^5]: Sara Jane Lippincott \[pseud. Grace Greenwood\], *Daily American Telegraph*, April 12, 1852, 4, <https://archivalgossip.com/collection/items/show/832>.

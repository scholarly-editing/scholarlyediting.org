---
title: >-
  <em>Afro-Creole Poetry in French from Louisiana's Radical Civil War-Era
  Newspapers. A Bilingual Edition</em>. Translated and introduced by Clint
  Burce. Transcribed by J.R. Ramsey
authors:
  - first: Caterina
    last: Bernardini
    affiliations:
      - University of Nebraska-Lincoln
    orcid: 0000-0001-8019-5167
issue: Volume 39
group: Reviews
group_order: 1
doi: 10.55520/DSW5S6VH
---

*Afro-Creole Poetry in French from Louisiana’s Radical Civil War-Era Newspapers. A Bilingual Edition.* Translated and introduced by Clint Bruce. Transcribed by JR Ramsey. New Orleans: The Historic New Orleans Collection, 2020. 363 pp. Index. \$40. ISBN: 978-0-917860-79-9.

“Lost documents sometimes reveal themselves when we need them most,” Clint Bruce writes in the preface of his *Afro-Creole Poetry in French from Louisiana’s Radical Civil War-Era Newspapers* (XVii). Bruce’s sense that a thoughtful recovery of nineteenth-century materials is immensely relevant to today’s struggles for justice and equity—devoid of any racism and systemic oppression—is present in every page of this volume. The corpus, meticulously selected, translated, and presented, is composed of seventy-nine poems that were originally written in French and published in two bilingual Louisiana newspapers of the 1860s, founded by free people of color: *L’Union: memorial politique, littéraire et progressiste* (1862–64) and *La Tribune de la Nouvelle-Orléans* (1864–70).

Immediately apparent upon reviewing the volume is its editorial beauty and care— components that are central to Bruce’s enterprise. The large format of the book, with its elegant ivory-cloth cover and high-quality paper, encourages and facilitates readers’ close encounters with the poems, in both the original French and English—as do the numerous, clear, and carefully annotated reproductions of pages and illustrations from the original newspapers. The endpapers are filled with a reproduction of an issue of *La Tribune* from 1866 that Bruce located at the American Antiquarian Society. This issue illustrates the tragic aftermath of the Mechanics’ Institute Massacre of July 30, 1866, a violent encounter in which racist reactionaries and police officers murdered nearly fifty advocates of Black suffrage. Bruce’s archival excavations, which led to the retrieval and reintroduction of these newspapers’ crucial concerns (like the one noted above) demonstrate an obvious and remarkable achievement for several fields of historical, cultural, and literary studies. Bruce’s deft and impressive historical and literary contextualization renders this reintroduction even more remarkable by virtue of its accompanying introduction to the selected poems. Indeed, this introduction illuminates the significance of the existence of *L’Union* and *La Tribune* in the complex sociopolitical landscape of the Reconstruction years in Louisiana, an existence that prompted national and international recognition. One of the book’s major merits is surely that of effectively bringing to light not only a textual corpus, but also a wholistic cultural and historical climate.

With in-depth reflections about Afro-Creole identity (and insightful considerations on the use of the term “Creole”), Bruce explains how New Orleans functioned in these years as a place of cultural convergence, where the group of activist writers who belonged to the distinct class of Louisiana’s *gens de couleur* forged a platform for “mighty contributions to the civil rights struggles of the 1860s” (23). Bruce highlights the cosmopolitan nature of this community in its connections with Haiti and France. He notes, in particular, the ways in which the scope of the poetics articulated in these newspapers appropriates and reinvents French Romanticism and the French universalist vision. This lens yields important insights and is a powerful reminder of the transnational nature of literary exchanges. It must also be noted that Bruce takes care to identify vexed questions within this community, including the patriarchal system of masculinist values that remains firmly at play in the periodicals.

The edition presents the poetic corpus in five main thematic groups: “Poetry as Prophecy,” “The War and Reconstruction,” “Liberty, Racial Equality, and Fraternity,” “The World of Ideas,” and “Matters of the Heart.” Bruce clearly explains the guidelines he followed in editing and translating the material—including those related to matters of textual treatment such as spelling, vocabulary, punctuation, diacritics, and line arrangements—in the final part of the introduction and in the “Note on the Text” that follows. Likewise, he offers rich biographical notes on known authors. The “Note on the Translation” is a fascinating, if at times slightly contradictory, piece. Bruce commences this section by invoking Octavio Paz’s description of a translators’ task; for Paz, the translator creates, with a new text, “analogous effects” found in the original. Since the French original texts employ a versification in alexandrines, as is typical of nineteenth-century French poetry, perhaps in an attempt to put Paz’s theory into practice, as applied to rhythm and metrics, Bruce renders the English version in a fixed metrical structure: iambic pentameter (or, at times, hexameter). As much as possible, he also attempts to reproduce the rhyming schemes closely. Such an ambitious undertaking places a monumental weight on a translator’s shoulders, especially when working with such a large corpus, but Bruce has a valid reason for choosing to do so. As he argues, demonstrated mastery in poetic technique was vitally important to these writers, for “to be able to write like a French writer was to show one’s equal standing with the best thinkers and artists of the world–and, by implication, to negate ideas of white writers’ literary supremacy” (70). Indeed, although arguably the act of translating can be viewed as a militant embrace of imperfection, in following this method, Bruce at times achieves nearly perfect results. Consider, for example, Lélia D. \[Adolphe Duhart\]’s lines from the powerful “Poésie! Vox Dei!” (“Poetry! Vox Dei!”):

<blockquote lang="fr">
N’entends-tu pas sonner à l’horloge lointaine<br/>
L’heure de tant d’espoirs, l’heure sainte et certaine<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;De la pure Fraternité?<br/>
N’entends-tu pas vibrer ces voix mysteriéuses<br/>
Qui passent en courbant ces têtes sérieuses,<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ces voix fortes de Liberté? (96)
</blockquote>

Bruce alters the rhyming couplets brilliantly and adds only one extra image (that of “choices”), creating a stunning equivalence of meaning that relies heavily on the use of enjambments, just as in the French language:

> Do you not hear, distantly ringing, the chime<br/>
> Of the hour of hopes, the sure and sacred time<br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Of purest Fraternity?<br/>
> Do you not hear these enigmatic voices<br/>
> That speak to heads weighed down with serious choices,<br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The forceful voices of Liberty? (97)

In other instances, however, Bruce’s decision to recreate an equivalent meter and rhyming scheme places a heavy burden on the overall poetic effect. Word order, punctuation, and word choice—elements in poetry just as crucial as meter and rhyme—are perhaps excessively sacrificed to the altar of the latter elements. He often adds unnecessary adverbs and adjectives for the sake of achieving the correct meter, and occasionally, he repositions the line order of stanzas. Likewise, one sees numerous lengthened and considerably altered lines—both in lexical and syntactical terms. Archaisms are also used perhaps too extensively when compared to the French. In this sense, Bruce’s goal of creating “analogous effects” is not always achieved and occasionally transforms what is in the original French a quite modern and unembellished diction into an English that is much more ornate. In “Stanza,” also by Lélia D. \[Adolphe Duhart\], for example, the original French lines are minimal, unadorned:

<blockquote lang="fr">
Vous tressaillez . . . Porquoi? Relevez votre tête;<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Suivez, suivez votre destin;<br/>
L’Espérance apparaît, brillante en la tempête<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comme l’étoile du matin.(84)
</blockquote>

Because of Bruce’s attention to rhyme, however, the English translation adds words to the original, rendered below in italics:

>You quiver . . . Why? Lift up your head *from your breast*;<br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pursue your destiny, *however far*,<br/>
> *For* Hope appears, shining through the tempest<br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*Just* like *the glow* of the morning star. (85)

Such linguistic accretions rob the text of the immediacy and poetic simplicity of the original, whose literal translation would read: “You shudder . . . Why? Lift up your head / Follow, follow your destiny/ Hope appears, shining in the storm / Like the morning star.”

Notwithstanding such reservations, *Afro-Creole Poetry in French from Louisiana’s Radical Civil War-Era Newspapers* is to be praised for bringing to full light courageous voices who channeled and amplified the energy needed in the present for changing the future. The anonymous poem “The Triumph of the Oppressed,” for one, reminds past and present readers of *La Tribune*:

> Holy liberty, that right so dear<br/>
> To man, that right whose name rings bright and clear,<br/>
> Shall be the sacred prize secured by your deeds,<br/>
> The bequest that forever will meet your children’s needs.<br/>
> Should you be struck down on the road to glory,<br/>
> Should you sell your life to purchase the victory,<br/>
> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Their blessings will be the proceeds. (149)

Such lines seem both to anticipate, and resonate with, activist poetry of the twentieth century, such as Audre Lorde’s call for a *present* fighting in “A Litany for Survival”: “seeking a now that can breed / futures/ like bread in our children’s mouths / so their dreams will not reflect/ the death of ours.”[^1]

[^1]: Audre Lorde, “A Litany for Survival,” in *The Selected Works of Audre Lorde, ed. Roxane Gay* (New York: W.W. Norton & Company, 2020), 283.

---
title: The Julian Bond Papers Project
subtitle: >-
  An Interview with Deborah McDowell about the Project and Its Inclusion of
  Community
authors:
  - first: Kathryn
    last: Blizzard
    affiliations:
      - University of Virginia
issue: Volume 39
group: 'Voices and Perspectives: Interviews and Conversations'
group_order: 2
doi: 10.55520/230W4D7W
---

# Introductory Note

In 2017, the Carter G. Woodson Institute partnered with the Center for Digital Editing to create a scholarly edition of the papers of Julian H. Bond, noted civil rights leader and co-founder of the Student Nonviolent Coordinating Committee (SNCC) in 1960. To launch this editing project, the two organizations—both of which are located at the University of Virginia (UVA) in Charlottesville, Virginia—organized a community event that invited participants to contribute to the project by transcribing some of Bond’s speeches. The first transcribe-a-thon was held on August 15, 2018, in commemoration of the anniversary of Julian Bond’s death and in recognition of the tragic events that occurred in Charlottesville just a year previously, on August 11-12, 2017, when white supremacists had marched on the city and caused the death of counter-protester Heather Heyer. Additional community events have been held around the same time each year since then, with numerous local organizations participating in or contributing to the coordination of the event, including UVA’s Albert & Shirley Small Special Collections Library, UVA Athletics Department, Scholars’ Lab, Shenandoah Joe, the Virginia Center for the Book, and Virginia Humanities.

To speak to the inclusion of community in this editing project, **Deborah McDowell**—director of the Carter G. Woodson Institute and of the Julian Bond Papers Project, and UVA Alice Griffin Professor of English—has answered a few questions about the project and the transcribe-a-thon events. The interviewer, **Kathryn Blizzard**, is a research editor at the University of Virginia’s Center for Digital Editing, where she contributes to editing projects like the Julian Bond Papers Project, the Papers of Martin Van Buren, and the Washington Papers.

<div class="noindent">

## Interview

**Kathryn Blizzard:** What inspired you to work on an edition of Julian Bond’s papers? 

**Deborah McDowell:** Perhaps the most basic inspiration was to keep Julian Bond’s memory and legacy alive for students, scholars and the general public by making available selections from Julian Bond’s vast archive. Although he was an advocate for civil rights and social justice for roughly 50 years, the sheer range of Bond’s significant contributions is sometimes overlooked. We wanted to provide a wider range and deeper sounding, especially for the general public, of who Bond was, what he stood for, and the causes for which he fought over his long, distinguished career: economic justice, criminal justice, health care, affordable housing, the environment, LGBTQI rights. 

**KB:** What led you to involve the local community in this project, and how have you involved them? 

**DM:** Involving the community seemed a logical—indeed, a necessary—decision, given Julian Bond’s life and career, which were very much grounded in the community, in the local. He saw ordinary citizens, “the people,” if you will, as the center of any vision of democracy. Involving the public in this project sends a reminder to us all that social struggle, political struggle, that the advancement of civil rights, of human rights, is a community affair, is a community’s responsibility. We thus decided early on to involve the greater Charlottesville community—indeed, the nation—in transcribing the writings of Julian Bond. We wanted to give them a stake in that process. During the first event we hosted in 2018, citizens of Charlottesville flocked to sites all around the city to transcribe Bond’s speeches. They were active participants in the vitally necessary work of cultural memory and preservation.

**KB:** How has the community’s involvement advanced the work of this project?

**DM:** The community’s involvement has advanced this project significantly. Collectively, they have transcribed literally hundreds of documents over the past three years. These documents, which would be otherwise only available in hard copies, stored in boxes in UVA’s Special Collections, will now be available to anyone on the planet who has access to a computer. The work of transcription is a labor-intensive process, but we have created considerable community momentum around this project.

**KB:** What have been some of the community’s responses to this project and its transcribe-a-thon event?

**DM:** Here are a few randomly-selected responses from those who participated in the event:

One Rachel Ann Vigour described the transcribe-a-thon as a “community effort” that brought home the collective impact of Bond’s legacy. “It made me really feel like a citizen of this city,” she added.

A second participant had this to say about the experience: “To think about where he was at that point in his life, and what he was doing in the 1960s in the civil rights movement. Possibly sit-ins at cafes, SNCC, etc. It’s interesting to read his thoughts in his own handwriting.”

We heard many variations on the following theme: “The legacy of Julian Bond drew me here. With social justice and the need for the things that he was talking about back in the ‘60s, it’s kind of startling, and sometimes depressing, that there is still a need for some of those same conversations in 2018.” This participant was transcribing a decades-old speech about the need for proper health care in the Black community and the economic disparities between the black, working-class poor, and white communities.

**KB:** I understand you personally knew Julian Bond. How has your involvement in this project and the transcribe-a-thon event shaped your memory or understanding of him?

**DM:** Working with the transcription project for the past few years has not so much shaped but, rather, reinforced my memory of Julian Bond as a committed intellectual, an astute politician, and an uncompromising social activist, whose campaign for civil rights and social justice was, not only unwavering, but also constantly evolving. For example, when critics suggested that his activism on behalf of LGBTQI was incompatible with civil rights for racialized minorities, he answered them by saying, “Gay and lesbian rights are not ‘special rights’ in any way. It isn’t ‘special’ to be free from discrimination—it is an ordinary, universal entitlement of citizenship.” I consider it remarkable that among his last appearances at a protest, in February 2013, Bond was one of four dozen protesters arrested in front of the White House for demonstrating against the Keystone pipeline. There he noted, "The threat to our planet's climate is both grave and urgent." He declared his willingness “to go to jail to stop this wrong. The environmental crisis we face today demands nothing less."

Julian Bond’s was a trusted voice in American democracy throughout his lifetime, which spanned seven decades and significant historical moments in the U.S., the civil rights era, and beyond. Over the course of his long career—wherever and around whatever issue in which there was a need for moral clarity and insurgent activism—there you would find Julian Bond, who helped to set the template for social activism of the “long-distance” variety.

</div>

## Additional Resources

The Julian Bond Papers Project: <https://bondpapersproject.org>

---
title: The Long and Winding Road
subtitle: An Interview with Walter Earl Fluker
authors:
  - first: Silvia
    middle: P.
    last: Glick
    affiliations:
      - Harvard Law School
issue: Volume 39
group: 'Voices and Perspectives: Interviews and Conversations'
group_order: 1
doi: 10.55520/HD93DPST
---

## Introductory Note

Howard Washington Thurman (1899–1981) was a theologian, minister, writer, and teacher, and an advisor to many in the civil rights movement of the 1950s and 1960s, including Martin Luther King, Jr., Pauli Murray, Vernon Jordan, James Farmer, Whitney Young, and Bayard Rustin. Thurman served as dean of Rankin Chapel at Howard University from 1932 to 1944. In 1944, with A. J. Muste, he founded the Church for the Fellowship of All Peoples in San Francisco—the first major interracial, intercultural, and interdenominational church in the United States. From 1953 to 1965, Thurman was dean of Marsh Chapel and Professor of Spiritual Disciplines and Resources at Boston University, the first African American dean of chapel at a majority-white university.

Thurman is the author of over twenty books, including *With Head and Heart: The Autobiography of Howard Thurman* (New York: Harcourt Brace, 1979)*, The Search for Common Ground: An Inquiry into the Basis of Man’s Experience of Community* (New York: Harper & Row, 1971)*,* and *Jesus and the Disinherited* (Nashville, TN: Abingdon-Cokesbury Press, 1941). His writings on democracy, racism, spirituality, and intergroup and interfaith relations have much to teach us about contemporary issues and the ongoing struggle for freedom and equality for all.

Walter Earl Fluker is the Dean’s Professor of Spirituality, Ethics and Leadership at the Candler School of Theology at Emory University; the Martin Luther King, Jr. Professor Emeritus of Ethical Leadership at Boston University; and the editor and director of the Howard Thurman Papers Project. He is the editor of *The Papers of Howard Washington Thurman (vols. 1–5)* (Columbia: University of South Carolina Press, 2009–2019), editor (with Peter Eisenstadt) of *Moral Struggle and the Prophets* (Maryknoll, NY: Orbis Books, 2020), and author of *The Ground Has Shifted: The Future of the Black Church in Post-Racial America* (New York: New York University Press, 2016).

Silvia P. Glick is managing editor of *Negotiation Journal,* published by the Program on Negotiation at Harvard Law School; codirector of the Negotiation Data Repository at Harvard University; consulting editor for *Scholarly Editing*; and director of publications for the Association for Documentary Editing. She worked on the Howard Thurman Papers Project from 2012 to 2018 and served as its associate director and managing editor. She is currently working on a digital edition of *The Papers of Howard Washington Thurman* as a member of the Virginia Digital Publishing Cooperative, funded by the Andrew W. Mellon Foundation.

In this interview, Dr. Fluker describes how he came to know Howard Thurman, the influence of Thurman on his life and work, and his founding of the Howard Thurman Papers Project. He shares the challenges of directing a large documentary editing project that has been hosted at several institutions, and the importance of hard work, commitment, and trust to the Project’s success.

This interview has been edited for length and clarity.

<div class="noindent">

## Interview

**Silvia Glick**: When were you first drawn to the work of Howard Thurman?

**Walter Fluker**: It began in the early 70s when I was a chaplain’s assistant in the US Army. I was assigned to the post chapel and served under the direction of Chaplain Alfred Brough, and each Sunday he would use meditations from Howard Thurman, who I didn't know a thing about. I was a very young man, maybe 19, 20 years old, and I would type the meditations on the Sunday bulletin, and one day I actually turned over Thurman's book, *The Centering Moment*,[^1] I turned to the back cover and there was this incredibly funny-looking, wonderful face. It was both humorous and serious at the same time; I was struck by that. And that was my introduction to Thurman.

I was not to meet the face that I saw on the back of the book until 1977, when I entered Garrett Evangelical Theological Seminary, where I was enrolled in a class on Black preaching, and the professor played “The Third Component,”[^2] a sermon by Howard Thurman, a long-playing album which was very popular. I was just struck by the slow cadence, the rhythmic measures in his voice, this kind of baritone voice, taking us deeper and deeper into ourselves, but even then I really did not have the interest that would propel me. During this same period, when I entered Garrett Theological Seminary, there was a beautiful young woman next door, in the place where I lived, and of course, this young woman, later she and I became lifelong partners, we were married. But I didn’t know at the time that she was the goddaughter of Howard Thurman. It was almost magical; Thurman’s word for it might be “mystical.” After meeting Sharon Watson Fluker,[^3] we began a courtship and it was in the \[Watson\] family living room that I first met Howard Thurman and Sue Bailey Thurman[^4] in person. They had come for a visit because Thurman was lecturing in Atlanta during that time, and it was hilarious! He was very close to Melvin Watson,[^5] Sharon's father, and to Agnes—Butler, when she first met him. Agnes Butler was his secretary at Howard University, and Melvin, whom he called “Monk” Watson, was one of the individuals involved in the chapel with him during that period. He had met Melvin Watson earlier when he was teaching at Morehouse in the late 20s, and Melvin was his driver because Thurman was notorious for not driving, and he claimed that he was very uncomfortable on airplanes. But that was the meeting face-to-face, and later, maybe a few months later, I was invited, along with nine other people, ten of us in total, to the Howard Thurman Educational Trust[^6] in San Francisco. These were young people who had made commitments to religious vocation. And he wanted to spend time with several groups—there were three in total of the younger scholars—or emerging scholars. That was in October of 1979. And I can only tell you—those were six or seven days that—his words were watershed; they certainly were a watershed for me. Some days while he was working with us, I could only write poetry. It would stream out of me—I’d written elsewhere—like libations to the gods. It was just incredible to be in Thurman's presence. Of course, the fascination of youth—you finally meet your mentor. On the very first day he comes in moving very slowly. We were not aware that he was struggling with cancer at that time and would pass on in about two years. But we sat there, six men and four women, and every word that came out of his mouth—we were there like little puppies, almost, just mesmerized.

**SG**: You would hang on every word.

**WF**: Hang on every word. And he read from the Gospel of Mark, which is the oldest of the Gospels, and the Gospel—just for note—the only Gospel that does not include a Resurrection story. You know of course in later years, you begin to see the logic of Thurman's meeting with us and reading Mark. But all I could remember after his reading, and it took him the entire morning to read the Gospel of Mark—in fact he told us we could take a cigarette break if we liked and come back—he was just hilarious. All I could remember was that Jesus was a radically free man and that everything that Jesus touched was freed up. That was in my notes. That’s what I wrote down. I said, “Wow.” And of course, it cracked open for some of us, many of the kind of theological—even Christological—presuppositions we had about Jesus, you know—Jesus the Christ, no; Thurman's Jesus was authentically human. Very, very human and a subject—not an object of religious experience, but a subject. So that was my immersion into Thurman, and I would communicate with him over the next two years. And when I chose to go Boston University, I had no idea that maybe a year, really my first year at Boston, that Howard Thurman would pass on and that a few years later Sue Bailey Thurman would send all of his materials to Boston University. I should add that I had access to those materials before then, and really, I spent a lot of time going into these materials while they were still at the Howard Thurman Educational Trust. But there were so many, Silvia. We later learned that there were probably, at least to our estimates, about 75,000 cubic feet—estimating 58,000 to 60,000 documents that we did collect over time.

It was overwhelming to sit at the Trust, there in the Listening Room on the third floor, where Thurman met us for the first time. And I was beginning then to get a sense of who this very unique human being was. And it was not until his papers arrived at Boston University while I was engaged in my PhD work that I had an opportunity to think deeply about a dissertation on Howard Thurman. An added note is that I shared this with my thesis advisor, and he told me there isn’t enough in Howard Thurman to justify a dissertation. And of course, being a PhD student and knowing how political those things are, I simply let him speak. He determined that I should actually do a dissertation on Howard Thurman and Martin Luther King, Jr.'s idea of community. Boy, was I angry. But I didn’t say anything because he was my advisor. The long and short of it is I completed a dissertation on the concept of community in Howard Thurman and Martin Luther King, Jr., which in many ways has become my working paper—over the years for a lot of different things and projects. After graduating from Boston University in 1988—I had completed the dissertation in 1987—I was invited to serve as the dean of the chapel at Dillard University, at the Lawless Chapel—it wasn't a lawless place; the name was Lawless.

**SG**: I understand.

**WF**: Thurman had dedicated the chapel in 1955. And again, I felt the kind of nudge, being there. I was only at Dillard for one year. Sharon and I both were at Dillard. Then the offer came to go to Vanderbilt University as assistant professor of Christian ethics, and I was there for four years, teaching courses on Thurman and King and other things. The big moment for me was when I took a sabbatical from Vanderbilt to begin a second book, to kind of complete the book that was my dissertation. I had published a book entitled *They Looked for a City*,[^7] which is a comparative analysis of the ideal of community in Howard Thurman and King. At Harvard that year as a Harvard faculty fellow—a Mellon faculty fellow—I was going to do a sequel. And I was trying to ask a primary question: Where did these leaders come from? Folks like Howard Thurman and Martin Luther King, Jr. Were there things in the tradition that predisposed them to this kind of vision of community? To my surprise, during that year, and even into the following year, I met several people who were key in the development of the Howard Thurman Papers Project.

While I was at Harvard in 1990 through 91, I was invited to Colgate Rochester Divinity School (CRDS) as the Dean of Black Church Studies and as the Martin Luther King Jr. Professor of Theology. And it was through the very gracious auspices of James Melvin Washington,[^8] a great church historian at Union Theological Seminary, it was through his advice, his mentorship, that I began thinking about the Howard Thurman Papers Project. Because my idea was to maybe publish a few unknown, obscurely published documents, sermons by Thurman, but Jim thought it might be worth doing an entire project.

**SG**: Did you know anything about documentary editing?

**WF**: Not a thing . . . not a thing. Still don't know much about it. I’ve always relied on great human beings and editors and historians like you to make that work for me. I’m so glad I don't have to know everything to do something.

**SG**: Did Dr. Washington know about documentary editing?

**WF**: Yes, lord, he was incredible. He had done a documentary edition on King, in fact the very first—on Martin Luther King, Jr.— but had also done a lot of documentary work on Black Church traditions. James Melvin Washington is a very important name in the study of not just Black theology but in African American religion and religious history. I'll just say that.

The next person I met is fascinating—now you're helping me to think about it—in fact at the same conference where I met Jim, was my first meeting with Clay Carson.[^9] They were gathered at a conference at Harvard Divinity School on religion and politics. And lo and behold, Clay also felt very strongly that I should pursue a project on Thurman. And last but not least, the incredible, inimitable Vincent Harding,[^10] who served on the Howard Thurman board but was also closely tied to the Lilly Endowment—he also encouraged me. And the rest of it was like magic. Everything for me quite frankly has been that kind of path because Colgate Rochester Divinity School was also the alma mater of both Howard Thurman and Martin Luther King, Jr. Colgate Rochester Divinity School was the former Rochester Theological Seminary where Thurman had attended and it had merged with Crozier Divinity School, where King received his master of divinity.

I began to work at Colgate Rochester in the fall of 1991, and I began to identify historians who had some documentary experience to begin building up a team.

**SG**: At that point, when you met Clay Carson, was he already working on the King papers?

**WF**: He was already beginning his work on the King papers. And Vincent Harding had just published this incredible book, or was completing a manuscript on this incredible book, documenting Black history, the freedom struggle, entitled *There is a River.*[^11] We as editors of the Papers Project know that it was Thurman who encouraged him to complete this book and actually hand-carried the manuscript to Harper & Row Publishers, where Vincent published his book. This is the early years, early ’90s. I assembled a team, one of those being Peter Eisenstadt,[^12] who came from New York, who had been engaged in documentary history in New York and was very anxious to begin a new program. Quinton Dixie,[^13] who was a student of James Washington’s, also came on board and Catherine Tumber[^14]— who had just completed her PhD at the University of Rochester on Christopher Lasch. So this was a small team, but the Lilly Endowment had given me a huge grant to get started. Later other grants and awards came, and we were able to expand the staff. We had quite a team. For almost the first four years, we spent time not just combing through the documents that we had but really searching for the universe of texts, which allowed us to get a sense of what was in front of us. Many of the transcribed documents that we have now—some were done very well, some not so well. I always tell students when you first start something you don’t know anything about, you have your first pancake, your second pancake, and different iterations. Well, Colgate Rochester was my first pancake.

I began another project there that is not directly related to Thurman. I received a huge grant from the Kellogg Foundation for what was then called the National Resource Center for Ethical Leadership from Black Church Traditions. Thurman was a major, pivotal figure for me in that thinking because it continued the project that I wanted to do at Harvard—that is, trace moral traditions out of these Black Church figures and movements. Well, those projects were going on simultaneously. And in the midst of that I received an invitation to come to Morehouse College. The reasons are many and varied for accepting that position, but it wasn't just to come to Morehouse. My beloved Sharon—her family was getting older and she needed to be with them, and we also thought it would be a good move for our young sons, Clint and Hampton, to grow up in Atlanta and become more acquainted with Black culture. Rochester's on the other side of the world, at least the Rochester that we were in. So, in 1997 I accepted an invitation to consult with Morehouse College, and one year later I accepted a position there as professor of religion and philosophy and as the new director—the first director—of the Leadership Center, which has since been renamed as the Andrew Young Center for Global Leadership.

**SG**: It was the Coca-Cola Center, right?

**WF**: Well, three or four years later Coca-Cola endowed a chair for me that was called the Coca-Cola Chair for Leadership Studies. It's a strange mixture of things, a strange alchemy—I was professor of religion and philosophy, I was director of the Leadership Center, and I was also the Coca-Cola Professor. If you ever want to understand my scatteredness, just kind of imagine me having three different things I was doing altogether. Morehouse was beautiful. It gave a real home to the Howard Thurman Papers Project. Of course, I had to hire new people—Peter and Quinton were still engaged but not quite as engaged as they were in Rochester—and because of my busyness, I don't think we really got that far at Morehouse on the Papers Project. A lot of promises were made regarding scholars in the area who would work with me when I arrived; that didn't quite happen. The work that I did was very, very intense. Thurman’s personal library came to Morehouse, which of course was Thurman's alma mater *and* King's alma mater—their undergraduate alma mater.

**SG**: So all of Thurman's papers from San Francisco moved from Colgate Rochester to Morehouse.

**WF**: Well, his personal library. In 1984, Sue Bailey Thurman sent most of the papers that we work with as the Howard Thurman Papers to Boston University. Of course, we searched repositories around the nation and even in Europe and in South Africa—we searched for Thurman's papers. I think maybe fourteen different collections we researched while we were at Colgate Rochester Divinity School. At Morehouse, the personal library came, along with the Howard Thurman Educational Trust. I was appointed, of course, overseer of the Howard Thurman Educational Trust, and the personal library was placed in an archived room there in this beautiful new center that we had just built, and even the little cloth that Gandhi had given to Sue—and to Howard, mainly to Sue—it was on display in a case while we were there.

**SG**: How long were you at Morehouse? When you say that you then had the Trust, was the Trust still operating—the Howard Thurman Educational Trust? Were you running it?

**WF**: It was operating when I moved to Morehouse; in fact, I served on the Trust board with Vincent Harding and several other people. When Thurman’s personal library came to Morehouse, I was the logical custodian. However, that became very interesting, and the Trust for all practical purposes became moribund at Morehouse and still is. I won't say any more about that but that's what you should know.

**SG**: How long were you at Morehouse?

**WF**: I was at Morehouse as consultant for one year and then from 1998 until 2010.

**SG**: Did you start to worry about the Project at that point?

**WF**: I worried all the time. I had people working; they were working, we worked . . . we worked. But I was only able to produce one volume—the first volume—of the Howard Thurman Papers while we were there. I always place it on my shoulders, as director and editor, but you know the greatest challenge in documentary editing, believe it or not, Silvia, is not the intricacies and the technicalities of the editing itself. It has more to do with human beings and human personnel who do the work. And often the challenge is tied to a lack of competence in some ways, not a lack of a clearly defined mission and processes. I would say at Morehouse that was not the problem. That would have been more at CRDS, kind of those first iterations. There were also huge political issues, often around funding.

**SG**: Yes; I wanted to ask you about funding. How were you dealing with your funders at this point? You produced one volume in twelve years. I know that you still continued to get money after that. How did you work that out? I know that your personal charm obviously helped.

**WF**: Let me name the funders first; Lilly, of course, being the greatest supporter of the Project. The Pew Charitable Trust. Monies came also through some individuals—Virginia Scardigli,[^15] who was a member of the Fellowship Church . . . hers was a small gift. Also, the Henry Luce Foundation, the Louisville Institute, and the National Historical Publications and Records Commission.

When you're at a small college—liberal arts college—the scramble for funds and the appropriation of funds is always a very, very difficult task. You have development offices and they help you write the proposals—supposedly—and you cannot go beyond protocol. You have to stay within the structures. Often \[those complexities\] created major challenges. But we survived, and mostly, I think, because of funders' genuine interest in Howard Thurman—the subject matter—and with some modesty that I think I don't have much of—I think it had to do with their trust in me—because I really did want to get this done.

**SG:** Was it in 2010 that you went to BU?

**WF**: Yes, I went to BU in 2010.

**SG**: How did that come about?

**WF**: I really see all of this as a kind of circular movement. I began at BU with Thurman, and was able to complete the volumes at BU. The volumes would have never been completed had I not accepted the invitation from Mary Elizabeth Moore[^16] in the School of Theology to come to Boston University as the Martin Luther King Jr. Chair. The way that happened was . . . my wife and son, Hampton, pulled a great biblical story on me. You’re familiar with the story of Jacob and Esau, and Jacob steals his brother's birthright? And the mother had a lot to do with it. Who was the mother? I always forget who the mother was. She kind of helped Jacob. Well, Hampton and Sharon conspired against me while I was in South Africa doing some other work, and I wanted Hampton to go to Morehouse, and I would have stayed at Morehouse for that reason. But Hampton was his own person, still is, and he wanted to go to BU and to study at the conservatory there. And he did. So by the time I got home, it was my task to go to the orientation at BU with Hampton. It's a funny family story you should know.

**SG**: Was this before Dean Moore offered you the position?

**WF**: No. This would have been in 2009.

**SG**: So, Hampton wanted to study to be an actor, which you had a little bit of doubt about, but that worked out well for him.

**WF**: He did very well.[^17] He always knows best. So I went to BU for his orientation. And during that time I served as a member of the dean's advisory committee at the School of Theology. So I went in to greet the new dean. As we were talking, she said, “Are you aware that the Martin Luther King Jr. Chair is open? Would you be interested?” And again, I have these moments where I just know there's magic in it. I wish I had a better word for it, but I said, “Hmmm, Hampton is here, that's my old professor, I studied under the Martin Luther King Jr. Professor, and his work began there”; so I decided over time to leave Morehouse, which I loved. I loved the work I was doing, but I would have never completed the Papers Project. I was working with people like Oprah Winfrey and all kinds of folks, and I was enjoying it; my ego was just attached to whatever the mission was. But I left Morehouse in 2010; I assumed the Martin Luther King Jr. Chair. With that, came the agreement that the School of Theology would create space for the Howard Thurman Papers Project, and that they would be helpful in supporting the Project. This offer meant that they would put their weight behind funding opportunities.

**SG**: You completed the last four volumes at BU.

**WF**: Absolutely. It’s magical again, Silvia. One day the editors for the Papers Project, Quinton and Peter, who were on their own journey of working with *Visions of a Better World*,[^18] showed up at the Mugar Library.[^19] And there was this incredible human being seated in the back just watching us and later approached me, and I explored her interest in the Papers Project and after that, you know, it happened. Her name was Silvia Glick.

Having a managing editor in that place and taking care of protocols, structure, and engaged in the actual work of the project was really the key. So we were able, within the span of really only eight or nine years, to complete the remaining four volumes. And we were there with the \[Thurman\] papers, you know.

**SG**: What was it like to transfer the papers? If someone else had to do this, would you say don't transfer papers? Moving the papers seems like a big deal.

**WF**: The first thing you ought to know, if I had to do this all over again, I would not do it. And if I had had any idea of the work that was involved in getting this done, I would not....

**SG**: You would not have started the project at all.

**WF**: Right. I would not. Each transition was difficult, because you never were quite sure what you left behind.

**SG**: In terms of papers?

**WF**: What was lost in the move. Even though you packed things up in boxes—this is before digitization—you never were quite sure, and you weren't sure what you were bringing with you. So each move meant a new reorganization, and with different staff, it just made it very problematic.

**SG**: How did you actually physically move all the papers?

**WF**: I believe from Morehouse they were shipped by UPS as part of the arrangement. They arrived and I had a person, Jameson Collier, who worked in the Papers Project—not as an editor but basically as an administrative assistant—who was able to help with the arrangement of things when we arrived at BU. But that was very challenging. And going from Morehouse to BU: I think you might say it was the first time where we had a chance to very carefully begin to curate the files in a different way. We had all of this paper—some had been digitized to some extent. We had an attempt at data collection that never quite got off the ground, and it wasn't systematized until we really arrived at BU.

**SG**: You say that you wouldn't have undertaken it at all. If you had started at a place like BU, or another major research university, do you think the whole process would have been easier?

**WF**: It would have been very different. I think the level of personnel would have been different. I started at a seminary, a freestanding seminary, that had no reference for documentary editing. All of my editors attended Camp Edit.[^20] So, they had training, but how do you communicate that to administrators on a campus—a freestanding seminary at that, unlike the School of Theology—and at Morehouse College, which had its own challenges around archives? So, no, it would not have happened unless I had made the transition to Boston University.

**SG**: When you first were undertaking the Project, how long did you think it would take? What did you think you were getting into?

**WF**: I had this incredible, naive vision that I would complete it at Colgate Rochester, maybe within four or five years. And it could have probably been done with the kind of technology that we have now and with the right kind of team, but, boy, was I mistaken. I always tell students now, I say, studying or building anything around folks like Thurman is like seeing a beautiful mountain in view, and you don't realize how tedious the work is or the climb until you stand at the foot of the mountain and you begin to climb. You don't know what you're doing with this kind of work, at least I didn't, until I really began the process.

**SG**: I think that's true of most documentary editing projects. How long did it end up taking you, from the start of the Project to completing the fifth volume?

**WF**: Let's start with the year 1992 because things were kind of set up after that. The vision, the idea of the Project, began in '91. I think that we completed the last volume around 2018.

**SG**: Was there any point along the way—there must have been some times when you said, “I'm just going to throw in the towel”?

**WF**: There were a lot of times I felt that way. But I couldn't. You know, most people are unaware that I've never received any monetary compensation for the project. For whatever prestige being affiliated with the project has brought, I'm grateful. But I made a commitment early on to Sue Bailey Thurman that I would not receive any money. She didn't demand that, but I really did \[this work\] out of what I thought was my duty. That's not to talk about how humble I am or anything, but it's just the fact that I really thought this was such important work. And I knew in the early ’80s even, that people really had no idea who Thurman was. They loved his meditations, and they thought he was a great preacher. But they had no idea about the depth of his thinking and the kind of conversations he was a part of—not only in his own time but his larger intellectual conversation, his discourse around ideas and traditions. Most people had no idea. So I wanted to make that public.

**SG**: How did it come about? Did you approach Sue Bailey Thurman and ask her?

**WF**: It was shortly after Thurman's death, which would have been in 1981. Sharon and I were married in 1981. Thurman died in April; we were married in June. Sharon being the goddaughter, you know, we went there for a visit. It was during the visit—we had already begun communication—that I talked with both Sue Bailey Thurman and Anne Spencer Thurman,[^21] who was then working with the Howard Thurman Educational Trust. She was executive director. And it was Anne and I who first began to think \[about\] this idea that became the Leadership Center, and now it's my work in ethical leadership. I had this idea that we could create out of the trust, Howard Thurman fellows. That was the original idea. And that part of the work with fellows would be to immerse them in the teachings of Thurman, and send them out, similar to what he had done in bringing us to the trust during our student days. That emerged into the idea of a papers project after the meetings with James Washington and Clay Carson, and later Vincent Harding. Anne saw that as a lovely idea, and so did Sue. Anne was an editor in her own right; she had worked with her father on his autobiography, and Sue was no stranger to this kind of work. So, they were happy to do it. But even they didn't expect it to take off like it did.

**SG**: There was no question that you would get Sue Bailey Thurman's permission? That was an easy matter?

**WF**: No, that wasn't necessarily the case. I really had to work and be very patient with Sue and, most importantly, Anne Thurman. Because there was some pushback. You're going into folks' lives. . . delicate things. . . . Not just documentary editors—all researchers, but certainly documentary editors—know that when you move into folks' private lives, there's pushback there. There's also pushback with ownership. Who makes the rules around here? And how do the rules get made? How are we going to be in relationship? That might have been over half a year, just the deliberation around that. Because I brought this to them in the spring of 1991, and I was not able to get a proposal off the ground, I think, until later that year.

**SG**: How did you negotiate? Were there conditions?

**WF**: One was around money, to be honest. They knew that I was not trying to get rich. Anybody who does documentary editing should know this. But I wasn't trying to get rich off the Thurman name. I think I had to convince them of that. Even though I was known to them, it was still a very, very delicate situation. His death was still very fresh in their own minds, and the disposition and the ways in which Thurman's legacy would be communicated was at the forefront of Sue Bailey Thurman's mind.

**SG**: Were there any restrictions on particularly sensitive matters that you couldn't cover or report on? Did you have to run everything by her?

**WF**: Well, that was understood—that anything that was of a personal, private nature—basically, it was said to me that they would want to review that. And it was in the letter of permission. Anne was also a lawyer, so there was a very carefully written letter of permission that identified the texts that I could use and could not use. Even those were still open to some scrutiny early on.

**SG**: Meaning that you felt like you had to run everything by the family?

**WF**: I did. If it were a questionable letter or thought, that was so. Fortunately, we didn't bump into many of those. Thurman had taken pretty good care of private things by burning \[them\]. But there were some other things that I did not see, Silvia, until 2010. One was the letter that Thurman had written to his daughters in the early ’70s identifying Saul Ambrose as his kind of adopted father; he wasn't his biological father.[^22] I hadn't seen that letter.

**SG**: Did you think he was the biological father until that point?

**WF**: I had heard rumors but could never verify it.

**SG**: Do you think his daughters knew the truth?

**WF**: They knew the truth, and in fact, the letter was released to the filmmaker Arleigh Prelow;[^23] it was released by Anne Spencer Thurman. She gave her the letter. Arleigh, being from San Francisco, had a close relationship, I guess, with Anne. At least there was trust, and Anne released the letter to her. And when we went to Arleigh to ask to use the letter after I had seen it with Luther Smith[^24] in 2010, and also asked the granddaughter of the Thurman family, we were refused. But that came out of the relationship that Arleigh had had with Anne, and I guess Sue, but certainly with Anne Spencer Thurman.

**SG**: How did you finally get it?

**WF**: I didn't get the letter. Never received it to this date. Arleigh Prelow still has it.

**SG**: At some point, you felt that you could talk about the letter publicly, and how did that come to be?

**WF**: We actually have a note in Volume 5 that speaks to the fact that Saul—according to Howard—Saul Ambrose was not his biological father.

**SG**: At what point did you decide it's okay to release this sensitive information?

**WF**: I thought that after the death of Olive,[^25] the last surviving daughter, it was okay, but I still consulted with the grandchildren. Anton was on board for the most part, but the granddaughter, Suzanne, was not. She wanted Arleigh to have the letter and to use it in the film, which we're still kind of waiting for. I'll be glad when she’s able to complete that.

**SG**: How did that resolve, the disagreement between Anton and Suzanne?

**WF**: I don't know how that resolved. I'm not sure it was resolved. I know that I didn't get permission to publish the letter.

**SG**: So, you never really got permission to report on the letter, either, but you chose to.

**WF**: No, but I think we have a responsibility as scholars, and especially as historians, to share what we know, and it's not speculative because I actually witnessed it, and others have witnessed it, and Arleigh also acknowledges that she has the letter.

**SG**: I suppose there could’ve been an argument that you had the right to the letter because everything Thurman had written was covered by Sue Bailey Thurman's release.

**WF**: Right. I think Sue would've been open. I'm not sure Anne would've been as open, and that's for a host of reasons. Here's another better example, because that’s still a contentious kind of matter. Before Olive Wong passed away, in April of 2010, I believe it was, it may have been later, she was holding the Africa journal—we call it the Africa journal. These are Thurman's diary or \[personal\] notes that he wrote in 1964 through 1965 when he traveled to Nigeria and his other travels during that period. Olive had \[the journal\], which came to my attention later. And when that came to my attention, it took some real time, and conversations, and building trust and also finding a kind of institutional—I don't know the word for it—but an institutional proxy. That would have been the Gotlieb Archive Center;[^26] Vita Paladino[^27] was kind of the holder of this document because formally it was entrusted to Boston University. Olive allowed Vita to release \[the journal\] to me.

**SG**: Had Vita had it all those years?

**WF**: No, I think \[the journal\] came to Vita by way of Olive, who was holding it.

**SG**: Was there some agreement that the document wouldn't be published until after Olive's death?

**WF**: I don't know whether we had an agreement that it would not be published until she passed, but it was not published until she passed. You're really putting your finger on *the* issue, I think, that most documentary editors, not only know and experience, but have to find different creative ways around these matters without violating one, trust, which is very important, and two, making sure that we maintain integrity of protocol. And I really tried to do that.

**SG**: How did you keep the money coming? Because there had been around ten years without producing a volume. How did you get foundations and such to support you after that big hiatus?

**WF**: I think people really did have a genuine interest in Thurman, first. Two, I did my very best to build trust with sponsors. That is, I didn't wait until I needed money to be in touch with them. And I found different ways to include them in public announcements of the work or in public conversations around the work. I think this is very important—building relationships of trust, and sharing. In my language, it's building networks of mutuality and reciprocity. That's called social capital, by the way. How do you make sure that you include the people who brought you to the dance? People like to be included. Human beings love recognition perhaps more than they love bread. We just love recognition. And I think this is important. So, every conference, convocation, I would make sure that I shared my gratitude for their support and included them as best I could.

**SG**: What sort of advice would you give to someone who came to you and said they wanted to start a documentary editing project of someone, an African American figure who had a rich wealth of papers like Thurman but was relatively unknown? Would you say, just don't do it; it's too hard? What would you say?

**WF**: I would never say don't do it, but I would tell them how difficult it is. I would also, based on my experience, ask them to consider first things first. That is, prepare the market before you sell the dream. That means, if there are family members, please build a relationship of trust with them if possible. If not trust, then a transactional understanding, which is the norm. Quid pro quo, you get this, I get that. That's going to be part of the give-and-take period but I would tell them that's most important; with whoever has proprietary rights, normally that's family, or some designee. Build trust and make sure if it is transactional, that that's clear, that's formal, and that it is legally reviewed and sanctioned. I would also ask them to make sure that they assemble a team of people who bring competence, but I would add, maybe, two other important qualifications. Integrity. The last thing you want are people around you whom you can't trust. I've had my share of that, right? You're always monitoring, you're always moving in and out. So integrity is important. And I think energy, believe it or not. You can have competence but if people lack energy—that is, will—and are not able to stay in the game with you, it can be fatal to a project. Maybe a better word is commitment. But this commitment has to have some energy; that's important. I also think most of the issues, as I've mentioned, are around personnel. If you are in a small setting like a small liberal arts college, or as for me, beginning in a freestanding seminary—I would advise against that \[type of arrangement\], especially now, more than when I started. I just don't think these places have the capacity, the infrastructure, to really support a project, certainly not of the magnitude of Thurman and some others. You just can't do \[the work\] without infrastructure.

**SG**: Did you ever feel like there was any hurdle in terms of the fact that you were studying an African American theologian who wasn't well known—as opposed to other documentary editing projects who are—a lot of them are on the founding fathers, and other white people. How has this changed over the last 20 or 30 years?

**WF**: Absolutely. Especially when it came to sponsors, to foundations. Thurman is outstanding in that way because he kind of transcends, in his own way, some of the caricatures and assumptions associated with African American leaders, and I don't want to overstate that but I think in some ways he transcends that. But the difficulty is, I think the burden, in fact, beyond the area of study that you're looking at, is really with your ability—the director or the editor—to establish trust. It's a strange and scarce commodity nowadays to be able to meet with people face-to-face and not just become a salesperson, selling this idea, but to establish \[as\] best you can, factually establish, the need for this kind of research and the development of this research. Most people in the academy, beyond biases, like to see fresh areas of research that can contribute to a larger body of knowledge, and it doesn't make them look bad either, if they are part of the sponsorship of that.

**SG**: Do you think foundations are more eager to support projects focusing on people of color now than maybe 20 years ago?

**WF**: I think in many ways, the culture has caught up with itself. So much credit has to be given to people who have done the hard work of pushing against these old attitudes. The marching, the protesting, yes, but also the strategic placement of individuals in the hierarchy of power. It's no accident that in many of these foundations, you have some very able women and men from very diverse backgrounds who speak to power and who are part, or becoming more part, of the powerful. And I think this has been basically the work of people who have done the hard, difficult task with feet on the ground. That's true not just for African Americans and for women. I think it's true for LBGTQ+—whatever nomenclature we need—we have some folks who really deserve credit for this very difficult work that was done, that made some of these things possible.

**SG**: Related to that—the diversity of people in funding agencies—how do you think that the field of scholarly editing, and documentary editing in particular, could become more diverse? I'm the director of publications for the Association for Documentary Editing, where I've been involved for many years, and we struggle with how to let it be known that we are open to everyone. For organizations that are seen as sort of old school, what's your advice for diversifying advisory boards and the membership of organizations and editorial boards, the people who are actually putting out publications?

**WF**: Again, I hope this doesn't sound too sentimental. But I think building trust is huge, and part of building trust means that these organizations, these boards, must be proactive. Please do not wait until people come to you asking, or demanding, but be proactive. Even at the risk of being clumsy or making a mistake. Normally the big thing is, can we really do this, or can we trust this individual. But no, I think being proactive and finding ways as part of being proactive to communicate intentions with integrity. Sometimes that means saying publicly, Silvia—and I don't think we've done that well enough—that, boy, we've really blown it in the past, or we're part of a larger systemic structure and history that has set us up in ways that we have a very monolithic assumption about what the good is and who should be here. I think that's done by making sure that one, you build trust, you become proactive, and part of being proactive is to communicate with integrity. It has so much to do at the end of the day with this curious variable of power. Relinquishing power. And hearing other stories that are not just intellectually important. Sometimes we go to this place of discourse where we say, like an abstract conversation: Oh, this is very important. But actually, it's important because you're really dealing with other human beings, and their histories and their stories have to be told and retold. I'm really thinking in some ways now about Native American people perhaps even more than African Americans. It’s important to go to those who get the least attention and be very intentional about bringing them on board but also about providing them with the resources that they need to be successful.

**SG**: Making sure that you're offering them something that is of value to them.

**WF**: Yes, how do you get to places like integrity? You can tell that's a big word for me nowadays. But how do you get to that place where you can do that? Because you can't be empathetic with people until you have some sense of integrity in yourself. For yourself. I think those are major steps not just for documentary editing but certainly for the academy writ large.

## References

Eisenstadt, Peter. 2021. *Against the Hounds of Hell: A Life of Howard Thurman.* Charlottesville: University of Virginia Press.

Fluker, Walter Earl (ed.). 2009. *The Papers of Howard Washington Thurman (vol. 1): My People Need Me, June 1918–March 1936.* Columbia: University of South Carolina Press.

Fluker, Walter Earl (ed.). 2012. *The Papers of Howard Washington Thurman (vol. 2): Christian, Who Calls Me Christian?, April 1936–August 1943.* Columbia: University of South Carolina Press.

Fluker, Walter Earl (ed.). 2015. *The Papers of Howard Washington Thurman (vol. 3): The Bold Adventure, September 1943–May 1949.* Columbia: University of South Carolina Press.

Fluker, Walter Earl (ed.). 2017. *The Papers of Howard Washington Thurman (vol. 4): The Soundless Passion of a Single Mind, June 1949–December 1962*. Columbia: University of South Carolina Press.

Fluker, Walter Earl (ed.). 2019. *The Papers of Howard Washington Thurman (vol. 5): The Wider Ministry, January 1963–April 1981.* Columbia: University of South Carolina Press.

Howard Gotlieb Archival Research Center. The Howard Thurman & Sue Bailey Thurman collections, available at http://archives.bu.edu/web/howard-thurman/home.

The Howard Thurman Papers Project, available at https://www.bu.edu/htpp/.

Thurman, Howard. 1979. *With Head and Heart: The Autobiography of Howard Thurman.* New York: Harcourt Brace.

</div>

[^1]: Howard Thurman, *The Centering Moment* (Richmond, IN: Friends United Press, 1969).

[^2]: “The Third Component” was delivered by Thurman at Boston University’s Marsh Chapel on October 26, 1958. An audiotape of the sermon may be accessed at http://archives.bu.edu/web/howard-thurman/virtual-listening-room/detail?id=340585.

[^3]: Sharon Watson Fluker is director and founder of Daughters of the African Atlantic Fund. She previously served as strategic adviser and associate director of fellowships at the Center for Public Leadership (CPL) at Harvard Kennedy School and senior adviser to the Salzburg Global Seminar’s Mellon Fellows Community Initiative. She was a goddaughter of Howard Thurman.

[^4]: Sue Bailey Thurman (1903–1996), an author, lecturer, historian, and civil rights activist, married Howard Thurman on June 12, 1932. She founded the *Aframerican Woman’s Journal*; established the Museum of Afro-American History in Boston; and was a member of the Negro Delegation on the Pilgrimage of Friendship to India, Burma, and Ceylon that met with Mahatma Gandhi in 1935.

[^5]: Melvin Hampton Watson (1908–2006) lived in Atlanta, Georgia, where he was chair and professor of religion and philosophy at Morehouse College, professor of theology at the Interdenominational Theological Center, and pastor of Liberty Baptist Church.

[^6]: The Howard Thurman Educational Trust was chartered on July 1, 1961 and ended operations in the years following Thurman’s death (Eisenstadt, 2021). The Trust aided African American undergraduates and supported Thurman’s philanthropic interests.

[^7]: Walter E. Fluker, *They Looked for a City: A Comparative Analysis of the Ideal of Community in the Thought of Howard Thurman and Martin Luther King, Jr.* (Lanham, MD: University Press of America, 1989).

[^8]: James Melvin Washington (1948–1997) was Professor of Church History at Union Theological Seminary and Adjunct Professor of Religion at Columbia University.

[^9]: Clayborne Carson is the founding director of the Martin Luther King Jr. Research and Education Institute at Stanford University, where he directed the Martin Luther King Jr. Papers Project from 1985 to 2020. He is currently director of The World House Project at Stanford University.

[^10]: Vincent Harding (1931–2014) was a close associate of Martin Luther King Jr. He is the author of *Martin Luther King: The Inconvenient Hero* (Maryknoll, NY: Orbis, 1996) and *Hope and History: Why We Must Share the Story of the Movement* (Maryknoll, NY: Orbis, 1990).

[^11]: Vincent Harding, *There is a River: The Black Struggle for Freedom in America* (New York: Harcourt Brace & Co., 1981).

[^12]: Peter Eisenstadt was Associate Editor of the Howard Thurman Papers Project. His books include *Against the Hounds of Hell: A Life of Howard Thurman* (Charlottesville: University of Virginia Press, 2021), (with Quinton H. Dixie) *Visions of a Better World: Howard Thurman’s Pilgrimage to India and the Origins of African American Nonviolence* (Boston: Beacon Press, 2011), and *Rochdale Village: Robert Moses, 6,000 Families, and New York City’s Great Experiment in Integrated Housing* (Ithaca, NY: Cornell University Press, 2010) and co-editor (with Walter Earl Fluker) of *Moral Struggle and the Prophets* (Maryknoll, NY: Orbis Books, 2020). He is an affiliate member of the Clemson University history department.

[^13]: Quinton Dixie is Associate Research Professor of the History of Christianity in the United States and Black Church Studies at Duke Divinity School. He is the author (with Genna Rae McNeil, Houston Roberson, and Kevin McGruder) of *Witness: Two Hundred Years of African-American Faith and Practice at the Abyssinian Baptist Church of Harlem, New York* (Grand Rapids, MI: William B. Eerdmans Publishing, 2013) and (with Peter Eisenstadt) *Visions of a Better World: Howard Thurman’s Pilgrimage to India and the Origins of African American Nonviolence* (Boston: Beacon Press, 2011) and co-editor (with Cornel West) of *The Courage to Hope: From Black Suffering to Human Redemption* (Boston: Beacon Press, 1999).

[^14]: Catherine Tumber is a writer, editor, and urban policy analyst. She is the author of *Small, Gritty, and Green: The Promise of America’s Smaller Industrial Cities in a Low-Carbon World* (Cambridge, MA: MIT Press, 2012) and co-editor (with Walter Earl Fluker) of *A Strange Freedom: The Best of Howard Thurman on Religious Experience and Public Life* (Boston: Beacon Press, 2014).

[^15]: Virginia Scardigli was a friend of Thurman and served as secretary of the Fellowship Church.

[^16]: Mary Elizabeth Moore is Professor Emerita of Theology and Education and Dean Emerita of the School of Theology, Boston University. Her books include *Teaching as a Sacramental Act* (Cleveland, OH: The Pilgrim Press, 2004), *Ministering with the Earth* (St. Louis, MN: Chalice Press, 1998), and *Teaching from the Heart: Theology and Educational Method* (Minneapolis, MN: Fortress Press, 1991).

[^17]: Hampton Fluker’s film credits include *Patriots Day* and *The Blind Side;* TV credits include *Shades of Blue*, *Chicago Med*, and *Major Crimes.*

[^18]: Quinton H. Dixie and Peter Eisenstadt, *Visions of a Better World: Howard Thurman’s Pilgrimage to India and the Origins of African American Nonviolence* (Boston: Beacon Press, 2011).

[^19]: Mugar Memorial Library is Boston University’s main humanities and social sciences library.

[^20]: The Institute for the Editing of Historical Documents, affectionately called “Camp Edit,” was a five-day workshop for individuals new to historical documentary editing. Held from 1972 to 2019, it was funded by the National Historical Publications and Records Commission and for most of its existence was administered by the Association for Documentary Editing.

[^21]: Anne Spencer Thurman (1933–2001) was the daughter of Howard Thurman and Sue Bailey Thurman. A writer and journalist, she collaborated with Howard Thurman on his autobiography, *With Head and Heart: The Autobiography of Howard Thurman* (New York: Harcourt Brace, 1979) and was active in the work of the Howard Thurman Educational Trust.

[^22]: An editor’s note in vol. 1 of *The Papers of Howard Washington Thurman* states: “Saul Solomon Thurman was not Thurman’s biological father, and Thurman knew this as a young man. He wrote a letter about this to his daughters in the mid-1970s. Although several people close to the Thurman family, including the senior editor of the Howard Thurman Papers Project, have seen the letter, vouch for its existence, and have described its contents, the editors do not have a copy of the letter in our possession and are not authorized to either quote from it or publish it.” (Fluker, 2019: xliii)

[^23]: Arleigh Prelow is a filmmaker. She is currently working on a film about Howard Thurman titled *The Psalm of Howard Thurman.*

[^24]: Luther E. Smith, Jr. is Professor Emeritus of Church and Community at Candler School of Theology at Emory University. He is the author of *Intimacy and Mission: Intentional Community as Crucible for Radical Discipleship* (Eugene, OR: Wipf & Stock, 1994) and *Howard Thurman: The Mystic as Prophet* (Richmond, IN: Friends United Press, 1991) and editor of *Howard Thurman: Essential Writings* (Maryknoll, NY: Orbis Books, 2006).

[^25]: Olive Katherine Thurman Wong (1927–2012) was the daughter of Howard Thurman and Katie Kelley Thurman. She was a librarian, theater director, writer, and fashion and costume designer.

[^26]: The Howard Gotlieb Archival Research Center is a “repository for individuals in the fields of literature, criticism, journalism, drama, music, film, civil rights, diplomacy and national affairs. Although contemporary public figures is the specialty of the Center, there are substantial holdings of earlier historical documents and over 140,000 rare books” (http://archives.bu.edu/about).

[^27]: Vita Paladino was director of the Howard Gotlieb Archival Research Center from 2006 to 2019.
---
title: Writing Family and Literary History from Primary Texts
subtitle: An Intergenerational Interview with Neil Devereaux
authors:
  - first: Cristina
    last: Ramírez
    affiliations:
      - University of Arizona
    orcid: 0000-0002-4079-4007
issue: Volume 40
group: 'Voices and Perspectives: Interviews and Conversations'
group_order: 1
doi: 10.55520/FNCEF6XN
---

# Introductory Note

“Child, this is your book. Take it and read it, and you will be rich beyond your imagination.”

These are the luminous words of Doña Ramona González (1906–1995) in “Los Libros.” This one-page piece is written in a genre that her granddaughter and recovery scholar, Cristina D. Ramírez, coined *cuadro*, meaning “square” or “picture frame.” The verses within a *cuadro* focus on one theme and paint a picture of everyday life. Similarly eloquent framing is evident in Doña González’s extended writings, which have been recovered by her son-in-law, Neil Devereaux, and Ramírez. These writings reflect the United States/Mexico borderland’s barrio language and its characters, who assume universal consequence and speak to community life. Doña González’s writings also provide insight into a female autodidact’s self-actualization and stature within her community, as well as the art of translation.

Doña Ramona González was born in 1906 in the barrio of Chihuahita, located in south El Paso, Texas and on the banks of the Rio Grande. In the early twentieth century, her family journeyed from Parral, Chihuahua to El Paso. Doña González lived through some of the most significant events in American history: World War I, the Great Depression, World War II, the civil rights movement, women’s liberation, and the Chicano movement. Acquiring only a high school education in 1925, she was central to the life of her barrio. Doña Ramona González ran a grocery store into which neighborhood residents flowed, shared stories, and thrived. She nurtured her community with her stories and food, and even gave blessings to soldiers going off to war.

Remarkably, in her sixties, Doña Ramona embarked upon a different journey. Entirely self-taught, she wrote—nearly in secret but also within a women’s writing group—her own Mexican fables, translating them into English and crafting *cuadros*, *dichos* (sayings), creative fiction, poems, reflection pieces, riddles, oral histories, and short stories. She wrote about the barrios of El Paso and the people she remembered who lived there. Doña Ramona and her writings are representative of the understudied Chicano literary movement of the 1960s and 1970s. Even to her family, the scope of her publications is unknown. We do know, however, that she published Spanish short stories in *El Grito: A Journal of Contemporary Mexican-American Thought* (1967–1974), one of the most notable literary journals published at the beginning of the Chicano literary movement.

In 2015, thirty years after Doña Ramona’s passing at the age of 89, Cristina D. Ramírez’s aunt uncovered 750 pages of her manuscript-ready writings. Nestled in a vegetable box, the pages were forgotten and almost lost. A familial labor of love and recovery ensued. A bilingual anthology of Doña Ramona’s writings, A *Story of Stories from a West Texas Border Barrio: The Writings of Doña Ramona González*, is set to publish in late 2023. Devereaux translated Doña Ramona’s words and Ramírez recuperated them. Doña Ramona’s writings are now housed in the [Nettie Lee Benson Latin American Collection](https://www.lib.utexas.edu/about/locations/benson) at the University of Texas at Austin, and the manuscripts are digitized in [The Ramona González Papers Digital Exhibit](https://usldhrecovery.uh.edu/exhibits/show/the-ramona-gonzalez-papers) within the Hispanic Recovery Project[^1] at the University of Houston’s US Latino Digital Humanities Center. They represent *un grand plato de palabras*, a grand plate of words from a Mexican woman writer.

Cristina D. Ramírez is associate professor and program director of the Rhetoric, Composition, and the Teaching of English (RCTE) graduate program in the Department of English at the University of Arizona and is also interim department head of the English department. She has published two books focusing on the textual recovery and history of Mexican women journalists: *Occupying Our Space: The Mestiza Rhetorics of Mexican Women Journalists and Activists, 1875–1942* and *Mestiza Rhetorics: An Anthology of Mexicana Activism in the Spanish Language Press, 1875–1922.*

Ramírez’s father, Neil Devereaux, is professor emeritus of Spanish at Angelo State University in San Angelo, Texas, where he taught classes in Hispanic literature and linguistics for over 40 years. For more than 50 years, he has worked as a Spanish translator in the courts, private industry, and academia.

Kathryn Tomasek facilitated this interview and edited the AI transcription. She is co-editor in chief of *Scholarly Editing* and professor of history at Wheaton College in Massachusetts.

This interview has been edited for length and clarity.

## Interview

<div class="noindent">

**Cristina D. Ramírez:** Dad, how many years have you lived in Texas?

**Neil Devereaux:** About forty or fifty years. Once I get past my fingers and toes, there’s too many years.

**CR:** There are some things that I’d like the readers of *Scholarly Editing* to know about my father because he has been a fifty-year plus scholar of the Spanish language. Dr. Devereaux, can you talk a little bit about how you acquired your passion for the Spanish language?

**ND:** I’m not a native speaker. I went through the normal kinds of high-school Spanish classes in which you learn practically nothing unless it was a few conjugations and a couple of regular verbs with *ser* and *estar* and those kinds of things. But I was called on a mission by my church, that is the Mormon church, and I was called to go to Mexico, and that was in 1961.

That was way back before the invention of the wheel, but we managed to get through it. And so, I spent two and a half years in Mexico, in the southern part of Mexico—in the Yucatán—and I got a good feeling as I learned the language, which really did not take me very long at all. I found that rather than pursue a career in something like accounting, which was my first choice before I left for Mexico—my father was an accountant—I didn't really have any idea of what I wanted to do. But on my mission, I found out very definitely what I didn't want to do, and that was to be an accountant! And so, I found that I acquired the Spanish language very quickly, and within four or five or six months I was conversing fluently in it.

When I was down there, we always worked two by two; there’s what’s called the “senior companion” and the “junior companion.” And naturally, a senior companion has experience, but by the time I got through about four or five months, I was finding that I knew more about the language than my senior companion, so I was the one that was giving lessons in the language. So, I set goals for myself, that at the end of the time that I would spend in Mexico, I would be completely fluent—and not only in the kind of language that we dealt with on an everyday basis, which was basically a religious type of language. I read a lot of different kinds of things, everything from texts and grammar books to comic books. And I went to movies in Spanish, and read the subtitles, making sure to see what the translation was all about. When I left, I decided to go ahead and continue with acquiring the language at a deeper level.

But when I left Mexico, I spent a year with my parents, and we lived in Hawaii. I did some advanced studies—that is upper-level studies—with a major in Spanish. And then I went from there to Brigham Young University, and I graduated with a BA. Then I got a Fulbright scholarship to UCLA and majored in Hispanic languages and literature and got my MA. And then I applied to UT Austin \[the University of Texas at Austin\], and they accepted me into their language department as a graduate assistant, and I taught there for two years.

I got my first job at the first place that I put in an application for, Angelo State University. When I got to Angelo State University on a tenure-track position, I was receiving different requests for translating documents, which was really something that I hadn't done before, even though I had frequently orally translated different texts, such as conversations and meeting material. I became fairly fluent in that skill and was able to keep up with the different speakers that I was translating for. And so, I accepted these requests for translations.

It feels like we’re going back in time, going back to the sixteenth or seventeenth century because we didn't have computers yet, and I didn't type very well. And so, my first experience with translating was actually to take out a pen and pencil, or a pencil and paper, and that was the translation. And there hadn't been much evolution in translation back in those centuries that I am talking about; just get out your dictionaries and so on and so forth and do that type of translation.

It wasn’t until the mid or late seventies that somebody came walking up and down the halls in our offices, and they said, “Do you want a computer? How would you like a computer?” And I said, “Well, I don't know if I’ll have any use for that really. The things that I do in literature, I'm not sure about, but that's okay, as long as you're passing them out, go right ahead.” And so, we got the latest equipment, but they didn't give us any training or that type of thing. They expected us to learn sort of on the run, and that’s exactly what it turned out to be. But that turned out to be a great step forward.

If only I had been able to use that, for example, when I was writing my dissertation at UT Austin. I wrote it, and my wife, who typed very well, received the very unpleasant task of typing out a 250-page dissertation all in Spanish. My wife was bilingual also, and she taught in the public schools in both history and Spanish. And so, she actually wrote my dissertation about four or five times completely through, because if you made a mistake it wasn't something you could go ahead with and you had to begin from a certain page or the beginning of the chapter and write it again.

I then accepted positions with the federal court system, and I translated for thirty or thirty-five years with the court system, mostly translations that were done with Mexican immigrants who had been arrested by the Border Patrol, and all the different court sessions that the Border Patrol had with them. I then translated some court cases and translated in different types of settings.

The Border Patrol would send out agents and put them in “an undercover role.” They went on these surprise situations where agents would take immigrants from the border and transport them to North Carolina to work in the chicken processing industry. The undercover agents would audio record all that information, and they would send these tapes to their main office, which was in New York. The Border Patrol would get the tapes back, and they read through the transcriptions and the translations. The agents’ reviews were confusing. The notes were linguistically unrecognizable; they couldn't understand the language being used by the immigrants. It turns out that the people translating or doing the transcriptions couldn't understand the northern Mexican dialect. They realized that they missed out on large swatches of the material that they were supposed to be translating. And so, the director of the Border Patrol came up to me one day, and he said, “Neil, do you think that you can do this type of transcription and translation?” And I said, “Sure, I'd be glad to do that. That's how I learned my Spanish.” And so, I translated for them for about five years. I was also translating medical transcriptions and for businesses.

Later in my career, I translated all the documents for the City of El Paso. I was the official translator for the City of El Paso—the only one they ever had. And then they decided that they couldn’t pay me, the budget, you know. There was another mayor that came in, and she decided that I was expendable. They let me go. And so, I said, “Well, that’s all right.”

But I had done a lot of the legal translations and the medical translations and so on and so forth, and at the very end, I had translated literally thousands of pages for the city. And so, the city still called on me because they handed over the work that I did to a secretary, a bilingual secretary, saying, “Hey, you speak Spanish?” “Oh yeah, sure, sure.” She was not a trained translator, and so they received a much lower quality textual translation.

Speaking a language, even fluently, and translating is not the same thing. Even though you consider yourself bilingual as far as speaking is concerned, translation is much more than just having a knowledge of two languages. And that became very apparent to them. I had to work for the city of El Paso on an unofficial basis, and I did most of the documents that I had done before, when I was officially working for them. I always told the people that I translated for, “This translation will be in standardized Spanish, and I will guarantee my translation. And if you have anybody that has problems with it, please have them call me or have them correct it. But if you correct it, you're going to have to be responsible for it.” And so, they didn't. They had no one that ever challenged the translations that I did.

And then, Cristina and I got together and were translating for the first book that she and another professor \[Jessica Enoch\] were doing. This project then took me into translating different journalists from the nineteenth and early twentieth centuries. They were working on an anthology, anthologizing the words of Mexican women journalists, and their anthology then became a bilingual anthology. And I worked on editing and doing many of those translations. Sometimes you'd have to look into the old newspaper articles where they had taken quotes out of another newspaper. These translations were textually complex because the writers used a Spanish from the turn of the nineteenth century. And so really, I’ve done translation with and for my daughter’s projects ever since, and I’m still doing it. I don’t receive as many requests as I used to. The younger generation will probably look at me and say I’m obviously beyond the years that I am useful in those types of things. \[Laughs.\]

**CR:** I would disagree, Dad.

**ND:** Okay. But that’s basically what I’ve done, and I think that Cristina and I probably should share the fact that we have done a lot of work together, and we hope that it’s just the beginning.

**CR:** Thanks, Dad, for your background. There’s some history in there that I didn’t know. I didn’t know that you had received a Fulbright.

So, turning to my background now, I’ll talk about my education.

I do want to emphasize first the education that I received at home. I received a bilingual education from both my mother and my father, but I believe it was most influenced by my mom staying at home and teaching all of us Spanish. *Español fue la primera idioma que hablamos en casa*—Spanish was the first language that we spoke at home. My mom and dad really nurtured that rich Mexican culture, in the music, in the stories we read, even the novellas that we listened to. All of our interactions were culturally rich, so I owe a lot to them for that upbringing.

I went to the University of Texas at El Paso and received my minor in Spanish Education and a major in literature, English literature. And I went on to teach for about eleven, twelve years in the El Paso Independent School District. And as I was working on my Master of Arts in teaching English in the early 2000s, I realized that I wanted to continue my education even further. When I graduated with my master's, I think in 2005, they had just opened a PhD program in rhetoric and writing studies in the English department. And the first time I applied I didn't get in. But then I applied again, and I got in. And that's where this whole trajectory of my research just changed. I didn't want to just research pedagogy.

I read a book by Shirlene Soto, *The Emergence of the Modern Mexican Woman: Her Participation in Revolution and Her Struggle for Equality*.[^2] I read that book in one night. In her history, I read about these Mexican women journalists, like Juana Belén Gutiérrez de Mendoza, Laureanna Wright de Kleinhans, Hermila Galindo. And it was like they just grabbed me and said, “You know, you're going to study us.” It was so profound; it was like a calling. I started finding their primary texts, but there was nothing in English. I thought well goodness, I have a father who just happens to be one of the best translators in Texas! And I remember calling my dad, and I said, “Hey Dad, can I send you some texts?”

And he told me, “Well, you read the texts, and you send me the snippets that you want translated.” For me, it was a long, long process, because my dissertation was centered on these Spanish-language texts, and so I wasn't writing a dissertation just in English. I had my mind in two cultures, two languages. I had to contextualize those texts for my English-speaking readers but contextualize them within Mexican history and then intersect them also with rhetorical history and rhetorical theory. So, it was quite a challenge.

I remember sending my father the snippets—two or three paragraphs long or a paragraph long—and I would immediately get them back from him. And I knew, just like my father said, that he guarantees his translations, and I knew that what I received from my father I could include in my dissertation and feel good about the translations.

Since then, we've worked on two books. My dissertation became my first book, *Occupying Our Space: The Mestiza Rhetorics* *of Mexican Women Journalists and Activists, 1875–1942*,[^3] and he helped edit the Spanish and the texts. And you know, he was really good at capturing the essence of that late-nineteenth-century Spanish, which was so difficult. I remember him sending me the last translation for Laureanna Wright de Kleinhans, and he said, “Wow!” She was writing in that high, elite, eloquent, almost even Baroque kind of Spanish, but that's what was the norm then. Then I moved on to other Mexican and Mexican American women journalists.

I want to turn now to the project that we’re currently working on. About eight years ago, my aunt, Tía Norma—Dr. Norma G. Hernandez, Dean of the University of Texas Education School for many years—came to El Paso with a vegetable box. In this box were over 750 pages of my grandmother's writings.

Let me back up a little bit.

I lived with my grandmother, Doña Ramona González, for about five years, while I was going to the University of Texas at El Paso in my twenties. And you know when you're nineteen, you're kind of ignorant of certain things, right? But I knew that she had been a writer, and one day she pulled out this journal, *El Grito:* *Chicanas en la literatura y el arte* (1973). I didn't know the significance of this text until much later. She published five of her short stories that she had written in Spanish in this journal, which was one of the most significant literary journals at the beginning of the Chicano literary movement.

It was toward the end of writing my second book, the bilingual anthology, *Mestiza Rhetorics: An Anthology of Mexicana Activism in the Spanish Language Press, 1887–1922,*[^4] that my aunt produced this box of my grandmother’s writings. When we read through the documents, we discovered that they were “manuscript-ready” writings. In other words, they were edited and ready to send to publishers. My grandmother wrote mostly *en español.* She wrote short stories, poems, fables, and *dichos* (or sayings). Our family realized, “Oh, my gosh, we didn't know that she had been so prolific in her life.” Because at the time when she published these pieces in *El Grito*, she was in her sixties. Correct, Dad?

**ND:** In the beginning, yes. She went on and continued to work on her writing until she was in her eighties, I believe.

**CR:** Yes, she did, and I cover some of that in the book that I’m working on now, called *A Story of Stories from a Texas Border Barrio: The Writings of Doña Ramona González*.[^5]

These texts sat in the box for over three decades. Part of the research process included scanning all her writings, and from there, he would send them to me. I realized that these documents did not have any sort of organization. To help the research process, I created a digital finding aid with links to each document, along with transcriptions and translations, stored in the cloud. The digital finding aid contains links to the entire archived collection of my grandmother’s writings.

And one of the first stories—or actually one of the first poems—that I read from the collection falls under a genre that I call *un cuadro* (a square or picture frame). In essence, *un cuadro* is one page of writing that focuses on one topic. This genre is reminiscent of eighteenth-century writers and painters from Spain writing and creating pieces in the tradition of *costumbrismo*, which focuses on aspects of everyday life. The text didn't rhyme, and so I named the genre “*un* *cuadro.”* She titled this one *“Los Libros.”* This piece is my grandmother speaking to a child, giving the child a book, and she says, “Child, this is your book. Take it and read it, and you will be rich beyond your imagination.” “Los Libros” was the first of her writings that I read. I knew then that she was speaking to me. The significance is profound.

While sitting in my office at the University of Arizona, as an English professor, my grandmother spoke to me through these texts, saying, “Here is this book.” Her act of giving connotes a passing down of literacy and emphasizes the importance of literacy, not only in one language, but in two languages—first in Spanish, in her mother tongue. Thanks to my parents having passed along that Spanish to me I’ve been able to access and read all her writings.

But it was my father, through the process of translation, who brought her texts to life in another language, in English. And no one better to do this than my father because he remembers when she was writing in the 1970s and passing the texts on to her son-in-law, on to my father, and saying, “What do you think?”

Can we transition a little bit? Dad, what do you remember about Ramona González handing you her writings on our visits during the summer to El Paso?

**ND:** We would visit your grandmother three or four times a year. During our visits, she would sit—at her dining room or kitchen table—with a little portable typewriter that she would use. During some of that time, I was a student in graduate studies at the University of Texas at Austin; later when she handed me her texts, I had already graduated and was a professor. I was seeing what she was writing, but I didn’t read much of it because she didn’t share them all with us.

But what happened is that she felt a certain amount of linguistic inadequacy knowing the types of languages, such as street Spanish, that she had been exposed to as a young girl. We never know what we're speaking, or on what level we are speaking, when we're kids. We just open our mouths, and words come out. But later in life, she had no real literary training at all, and the only texts that she would read were mostly newspapers, and then she would read a little bit more in both English and in Spanish.

But she would write these stories, and what she realized was that the product that she had been producing, as far as her literary works were concerned, didn't completely square with what she was reading from Spanish-language texts from different sources. And so, this feeling of real inadequacy emerged, knowing that she was writing in a dialect form that’s spoken on the border. She wrote as she spoke, and so her written language was very different. It definitely was that type of dialect language, and you can see it reflected in the text. She felt that this form of communication was really not something that should be used as a published and finished text.

In many cases, people actually downgrade or denigrate the language that is spoken on the border. And society came up with names for all of this mixing, such as “Spanglish” or “Pocho.” It's just this mix of Spanish and English. When you actually get down to it, one language or one form of language is not any better than any other. As long as communication is made from the speaker or the writer to the listener or the reader, you have indeed succeeded in your goal of either writing or speaking.

And she would come to me, and she would tell me to look at and review the text. And she said, “Neil, what I would like you to do if you really could at all, I would like you to take my text and read it through and correct it and put all the accents in and do the spelling and things like that.” And I would tell her, “Ramona, I feel that if I did that, then if you ever had it published, it would not be your text anymore, and you would have to write something with the text, like this is ‘edited by.’” And I didn't want that at all. This should be her text, and it should be her thought, and it should be her language.

But that was a time when writing in that type of language was very definitely not something that was well accepted. There are a lot of people that write dialectically and the characters that actually utilize this type of dialect, well, it would be the sort of the lower class. People that live on the border use that type of language, but then the narrator comes in. And the narrator contrasts their language so that a standardized language would be in direct contrast with what most of the characters would speak in their dialogue.

That reflected her feelings, but it wasn't my feelings at all. And so, I really refused to do what she asked me to do. She passed away without my going through that process. And I didn't want to do it.

We had to have a discussion about this because Cristina lived with her, and she went through the same things, and Ramona would say to Cristina, “Oh, why can't your dad do this? You know I really have to have it done. And I know that it isn't what it should be, so on and so forth.” And so, Cristina realized that editing the writing was Ramona’s ideal.

But I said that if we did what she wanted us to do, we would be betraying the text. And those types of texts should not be transcribed or transliterated into something that they were not.

And her style represents sort of a confluence between the people that spoke in her texts; the characters and the narrator would utilize the same language and not be some sort of observer from on high and removed from the characters. But as the narrator she would be merged into the text as coequal with the characters in the text, and so that was the agreement that we came to that we work with.

**CR:** This issue of how to represent the texts became a real point of contention between my father and myself because I wanted her to be represented textually, with all the correct accents and the correct spellings and everything. And we got into some real disagreements and I said, “Well, I'm going to look for some scholarly help on this because there's really not much in MLA or Chicago that talks about this.” And this is when I found the Association for Documentary Editing and their manual, and there's a section in there on transcription from primary text to publishable form.[^6] I read that section, and it said that the text should not be altered. And I thought, “Okay, my dad is correct on this.”

The more that I started thinking about how I'm going to represent my grandmother's writings in this book that I'm about to publish, the more it makes sense to me to represent her writings as she wrote them. Because she wrote about the people of the barrio—her words, her life, and her experiences intersect with everything about not just the barrio, but the border barrio.[^7]

Barrio Chihuahuita, where she was born and lived until she was about 14 years old, is located right on the banks of the Rio Grande. And my grandmother writes about the floods of the Rio Grande coming in from one end of the adobe hut and going out the other. So her whole experience of the barrio as it was is reflected in her writings. If we take that out—my dad is so right—if we smooth that over, if we correct it, the writing could lose its authenticity of who she was. And we could lose the authenticity of the border barrio because there hasn't been a writer—a literary writer—that has come out of barrio Chihuahuita. There is a self-published historian, who is still alive—Fred Morales, who grew up in the barrio—but he wasn't born in barrio Chihuahuita. He writes history about the barrio, but he doesn't write literary texts.

One of the pieces that we're publishing through Trinity University Press is called “*Por vida de estas santas cruces, Yo viví en estos barrios”* (“By the life of these holy crosses, I lived in these barrios”). It's her saying, “I swear I lived here.” In writing this piece, she gives an account of the people that were living in the barrio, for example, the Doña Martina, the woman who delivered children.

**ND:** *La partera*. The midwife.

**CR:** Midwifes, midwives, thank you, I lost my thinking there. The midwife, Doña Martina, who was also the barrio bootlegger. I mean, it's so Shakespearian, but from the El Paso border barrio.

She wrote also about the milk man, Don Leandro. At one time, there had been milk farms there in Chihuahuita, and what she writes about checks out with some of the history that I have researched through the newspapers from 1914 and 1915 there in El Paso.

One really significant historical point that she writes about is the murder of this street candy salesman vendor, Don Teodoro, which did factually occur.[^8] In short, “*Por vida*” reflects a simple yet radical bearing witness to Chihuahuita’s people and community brought to life instead of being portrayed stereotypically in a newspaper report.

I said, “You know what, I'm going to plug his name into the archive search of the newspapers.” And, lo and behold, his name came up and he had been murdered during the time of the Mexican Revolution in 1914, when they were killing hundreds of people on the border. And that was one of the reasons that they left barrio Chihuahuita, because it really became violent.

But this story, I think, is one of her greatest, one of the best representations of her writing. That, and another one that we're publishing called "*La Tiendita*" (“The Little Store”).

Dad, now that you have almost, I don't know, forty-five, fifty years of distance, what would be some of the things that you say are really significant about Doña Ramona’s writings?

**ND:** Well in the first place, when I first took her writings seriously and opened up the vegetable box or whatever—it was the Jolly Green Giant—and saw the texts and actually sat down to read them, I had a really good idea of what I was reading. Because of the fact that I had been exposed to great literature and how literature becomes either localized or universalized and the ways in which local materials can have a universal influence and a universal quality about them, I reflected on the overall quality of her writings.

When I was reading her texts, I would say, “Gosh, this doesn't only apply to things that have happened in a small barrio on the border of the United States and Mexico; it has universal significance.” And what I saw is that in spite of—I don't know if I’d say that—in spite of the language that it was written in—you would not associate this writing with that of a university professor or someone who had writing skills that corresponded to classical writing—her writing was writing as it was.

But her writing went way beyond as far as the meaning and the significance of the characters because the characters themselves became like icons of this type of person that she was writing about and no longer represented just the person that sold vegetables out in the back of his truck. She really expressed and went into their soul and although she placed them within a context that was localized, the feeling and the way that she presented them was definitely way beyond any kind of purely localized importance. And so when I read her writings and I read the long story that she wrote about working in the small grocery store that her family had and bought over a period of around forty years, which became sort of the meeting place where people would come in and talk to each other and sit around a stove and share stories and gossip, it became also a history of the barrio itself, which now becomes something that is much more than just a barrio.

One of the things that I really remember about the piece is the fact that before World War II, the kids from the barrio would be drafted into military service and she had acquaintances with these boys, who were boys when she first met them, but now were going into the army. She told them, “Now if you’re being drafted, you're going to fight in a war, but if you come to the store, and you receive a blessing that I give you, I guarantee that every one of you will come back alive.” And well, that's a big guarantee, you know that's like Pat Riley saying we're going to win the NBA championships for three years, and we're going to three-peat, and you better make sure you know what you're talking about if you ever make that type of guarantee. And sure enough, every one of those young men that was in the barrio that actually visited her, she gave them each a blessing that they would go and participate in the war but come back to their families. After reading that section in “_La Tiendita_,” I thought that that was something completely so far out of believability. But this actually happened, and that's the kind of influence she had in the barrio.

**CR:** I'd like to give a textual example of what my father was describing. She would write about these barrio characters, and while they're local characters, they have universal appeal.

One of the stories that she published in *El Grito* is titled “*El Camoterro o El Vendedor Ambulante*” or “The Sweet Potato Salesman; The Roaming Salesman.” It's short—almost a one-page story—capturing this sweet potato salesman and the significance of his simple work.

Some of the themes that run through her writing reflect the loss of societal traditions. She was born in 1906 and lived through the Great Depression, World War II, the civil rights movement, women's liberation, and the Chicano movement, so she saw the so-called march of progress of the expanding city of El Paso and Juárez. I’ve been reading more closely into her writing some of what she captured; she laments the ways that progress robs us of some of these very personal kinds of experiences that we have either in the barrio or our own communities and neighborhoods.

And with the *camoterro*, she just brings the sweet potato salesman to life, selling an extra squirt of sugar or butter for an extra penny. And the image of the kids coming and running around him, you can smell the sweet potato and the sugar.

To give a literary comparison of what she was doing with “*Por Vida de estas Santas Cruces”* and also “*La Tiendita*,” it was very much like Thornton Wilder’s play, *Our Town*. And *Our Town* has this universal appeal, right? And even after all these years—it’s now sixty years that he wrote and published this play—it's still produced and it has this universal appeal. In a similar way, my grandmother’s grocery store was the center of the barrio, with all these people streaming in and out from the very beginning of the grocery store to the time it closed. In the end, my family had to sell the grocery store. It was taken by eminent domain by the State of Texas.

**Kathryn** **Tomasek:** That’s just awful.

**CR:** It *was* awful. This destruction centers her whole premise of progress, questioning this kind of progress. She may have thought, “The store that I loved, the community that I love, the gardens, and all the people are now under cement.” This realistic, everyday writing is what she left us. And sadly, she was not able to publish her writings. She tried. She tried. We have proof that she tried. I have a 1974 letter from the editor of *Quinto Sol,* the publisher of *El Grito*, and honestly, I don't know how I came to this letter. I was rummaging through my stuff and I found this letter. I'm going to share my screen with you so you can see it. Can you see the letter?

![Figure 1.](../../../images/issues/40/devereaux/figure1.jpg)

**ND:** Yeah, yeah.

**CR:** This is from the editor Hermenio Ríos, the main editor of *El Grito*.

**ND:** It reads, “I acknowledge having received your manuscript … or your various manuscripts … and I will communicate to you the decision that we will take about it. Again, I appreciate your interest in *Quinto Sol*, and your stories have been received very well, without anything I can really sincerely order,” and so on and so forth.

**CD:** So I found this letter, and this for me—you know as a historian right, Kathryn? You find a piece like this, and you're like, oh my God. Because I don't have to just speculate anymore. We know without a doubt whether she was continuously trying to publish her writings. She absolutely was.

We know that *Quinto Sol* closed its doors shortly after my grandmother received this letter.

And so with this letter, I know that she did try to publish her writings again after the 1973 publication. She must have just been heartbroken that she wasn't able to publish her writings. I discuss the fact that she kept writing in the forthcoming book. And no, we don't know which manuscripts she sent.

We don't know if there are some that we don't have. I have a feeling that there are probably some that we do not have. Dad, what do you think? Do you think there are stories that we don't have of hers?

**ND:** Very possible. Because of the fact that they were as we found them and in their original form. There was no order placed on them, and so no order was established. So, it's very possible that there were other writings. Also, because some of the pages are missing, especially from her *dichos*, her sayings. So, we know that there are parts that we don't have, and we don’t know whether or not we'll ever be able to recover that stuff.

**CR:** We don't have a provenance of the order, which tells us the original order the writings were arranged; we don't know dates, when she started writing. Because she really did write in, not secret, but maybe semi-secrecy because women, women writing, right Kathryn? Especially Mexican women writing, it was not something that was done.

We do know, and this is confirmed by my mother, that she would meet with Estella Portillo-Trambley, who won the *Quinto Sol* award in 1974. They were friends. Estella Portillo, one of the editors of *Quinto Sol*, was from El Paso, and she would come and talk to my grandmother and get ideas. I’m sure Estella Portillo encouraged my grandmother to write. And so, we have proof that she was in a writing group.

Ramona González, my maternal grandmother, is a Chicana literary writer from the Chicano movement of the 1960s and 1970s who has not been written about, whose work has not been recovered. But here we are doing this extremely important historical and literary work for seven or eight years, having received several grants for this project.

I received a Research Fellow grant from the University of Arizona to take time to write. I used that time to make a digital finding aid of the texts. I can show that screen to you. Creating this finding aid was an important and pivotal part of the writing process.

This is the finding aid. It shows the title of the piece, genre, transcription, translation, and number of pages. Look how prolific she was! This goes on for seventeen pages.

I created this finding aid because when I started to write about her writings, I would have to stop and scroll through hundreds of textual thumbnails. I realized that I had to put these in some order or the writing process would have been impossible. The creation of the finding aid took me maybe about two years.

![Figure 2.](../../../images/issues/40/devereaux/figure2.jpg)

For example, you click on a labeled link, and it'll take you to a Dropbox holding that brings up either the original text, the transcription, or the translation. The finding aid has saved me hundreds of hours by facilitating the search of the collection.

I was able to create this digital finding aid before we sent the texts to the Latin American Benson Collection, where they are now housed, which I think was the most appropriate place for them. When I wrote to the Benson Collection at the University of Texas at Austin, I gave them the background of who my grandmother was in terms of being a Chicana writer in the 1970s. They knew immediately where she fit in the literary historical timeline. They quickly wrote back, and we offered to donate the collection. We didn't receive (nor ask for) any funds for the collection because we don't think our grandmother would have necessarily wanted a monetary transaction related to her writings. She wanted her texts to be experienced by the community.

In addition to short stories, my grandmother wrote fables and poetry. The poetry reads as from a child's perspective, and she also wrote poetry for children, on themes such as growing up and learning to read. All these texts were written in Spanish. She completed the translation for some of these documents, such as the fable “*El gato y el Ratoncito*” (“The Cat and the Little Mouse”).

![Figure 3.](../../../images/issues/40/devereaux/figure3.jpg)

![Figure 4.](../../../images/issues/40/devereaux/figure4.jpg)

After organizing the texts, I was able to show this digital work. The organization of these texts led to a grant from the US Hispanic Recovery Project, which is now a major force in the recovery of Hispanic literature in the Americas. I participated in their first group of digital humanities scholars under a grant that they had received from the Andrew W. Mellon Foundation. Over the course of a year’s work, the project became [The Ramona González Papers](https://usldhrecovery.uh.edu/exhibits/show/the-ramona-gonzalez-papers) digital exhibit under the Hispanic Recovery Project.

**KT:** That's such significant work, and to make it available digitally really increases access.

**CR:** Here’s the link to the original of [“*Mi Tiendita*.”](https://usldhrecovery.uh.edu/items/show/3126)

**ND:** Forty-three pages long, and I scanned all of them.

**KT:** Thank you!

**CR:** The archivist at the Latin American Benson Collection said he was impressed with how clean and well preserved these texts appeared when they arrived at the center. Well-preserved documents are important to the process of digitization.

And so, my grandmother Doña Ramona González has left us—and when I say us, not just my family, but the community and the world—a treasure, a gift of this history that really would have been lost.

**ND:** It would be still sitting in a vegetable box in somebody's closet someplace.

**KT:** It's true, and I’m so glad that your aunt brought this to you. And I love the idea also that it could have just still been sitting in a vegetable box. I love that it's a vegetable box. That's a strong connection to the grocery store.

**CR:** Absolutely. I’m so glad you got that. Yes!

**KT:** Oh, this is lovely. Thank you both so much.

**CR:** Thank you so much for interviewing us, because this work goes beyond genealogy. Genealogy is extremely important for our family with a strong Mormon background. We know that Mormons work at the apex of genealogists and archivists around the world. So, preservation of this history goes beyond genealogy. It reaches into the construction of knowledge of not just our family, but of how our family intersects with societal history, local history, the history of the Chicano literary movement, the civil rights movement, the Great Depression, World War I, and World II. My grandmother was writing from all of the places in time.

**KT:** Yes, absolutely. And one of the things that I appreciate so much as a historian is micro history, and this gives us a kind of micro history that, as you say, Dr. Devereaux, opens up. And the idea behind micro history is that daily life is not just local; it is universal. This is a beautiful project, and your grandmother was a remarkable woman. Thank you both so very much.

**CR:** Thank you, and I also want to acknowledge my mother, who is not in this interview but has contributed so much. There are so many afternoons that I call my parents and ask about details about the family, about them coming from Parral, Chihuahua to El Paso, from Zacatecas to El Paso; she has this history in her memory. How blessed and lucky am I that I'm capturing all of this now, and this isn't just an interview with me saying, “I wish I had.” Because how many times do we hear people say, “I wish I had done that interview; I wish I had asked my grandmother more questions.” And still, after all this work, I wish I had asked my grandmother more questions. But she left what I call *un grand plato de palabras*, a grand plate of words.

**KT:** And it is so very beautiful. Thank you both. Thank your mother as well, please. This is wonderful work, Cristina, and I’m so excited for you that you get to do it as your scholarship and that you are rewarded for it.

**CR:** How lucky am I for that?

**KT:** No joke.

**CR:** Right? And my grandmother keeps giving. I've been able to pay my father from these grants to do the translation work because it's difficult work. The scholars who will be reading this work know the minute detail and scrutiny it takes to transcribe and translate texts, and then to historicize them. It’s so much work, and then in another language, that just adds a whole other layer.

**KT:** What your father says about translation is such a brilliant insight. Dr. Devereaux, many people don’t realize that just knowing two languages is not the same as being able to produce an accurate translation. That's so, so important.

**CR:** Well, again I am thankful beyond words for this opportunity, because I had started something like this, something like an interview of my father. But I'm working on the book, and you can only do so much, so thank you so much to *Scholarly Editing* for putting this project and our work on your radar and hopefully this will influence others to think to say, “What I have, writings, recordings, and any ephemera, from my grandmother, my grandfather, can be preserved and made socially significant.”

</div>

[^1]: Recovering the US Hispanic Literary Heritage Digital [Collections](https://usldhrecovery.uh.edu/).

[^2]: Shirlene Soto, *The Emergence of the Modern Mexican Woman: Her Participation in Revolution and Her Struggle for Equality* (Tucson: University of Arizona, 1990).

[^3]: Cristina Devereaux Ramírez, *Occupying Our Space: The Mestiza Rhetorics* *of Mexican Women Journalists and Activists, 1875–1942* (Tucson: University of Arizona, 2015).

[^4]: Jessica Enoch and Cristina Devereaux Ramírez, *Mestiza Rhetorics: An Anthology of Mexicana Activism in the Spanish-Language Press, 1887–1922* (Carbondale: Southern Illinois University Press, 2019).

[^5]: Cristina Devereaux Ramírez, *A* *Story of Stories from a Texas Border Barrio: The Writings of Doña Ramona González* (Trinity University Press, forthcoming 2023).

[^6]: Mary-Jo Kline and Susan Holbrook Perdue, *A Guide to Documentary Editing*, 3rd edition (Charlottesville: University of Virginia Press, 2008), available from https://gde.upress.virginia.edu/.

[^7]: The editors at Trinity University Press and Ramirez ultimately made the decision to edit Dona Ramona’s primary Spanish writings. The editors wanted to make them accessible to the Spanish-language reader. The original texts can be accessed at The Ramona González Papers, part of the Recovering the US Hispanic Literary Heritage Digital Collections.

[^8]: H. D. Slater, “Mexicans found guilty of murder,” *El Paso Herald*, May 27, 1914: 6. Available from https://www.newspapers.com/image/81014995.

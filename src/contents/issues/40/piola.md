---
title: Visual Literacy, Close Looking, and a Pivot to the Virtual
subtitle: The Library Company of Philadelphia’s Urban In-sights Workshop
authors:
  - first: Erika
    last: Piola
    affiliations:
      - The Library Company of Philadelphia
    orcid: 0009-0009-1493-8239
  - first: Sarah
    last: Weatherwax
    affiliations:
      - The Library Company of Philadelphia
    orcid: 0009-0003-1209-6118
  - first: Anne
    last: Verplanck
    affiliations:
      - Penn State University
    orcid: 0000-0002-1415-7864
issue: Volume 40
group: The College and University Classroom
group_order: 2
doi: 10.55520/6F10RCZG
---

## Visualizing a Visual Literacy Workshop

Soon after its inception in 1731, administrators of the Library Company of Philadelphia, a subscription library–turned–independent research library specializing in American history and culture, collected graphic material.[^1] In more recent decades, the Library’s administration hired a Curator of Prints to steward the collection, formed a Graphic Arts Department, and implemented a Visual Culture Program (VCP), including an associated fellowship.[^2] These benchmarks facilitated the project *Imperfect History: Curating the Graphic Arts Collection at Benjamin Franklin’s Public Library*, which commemorated the fiftieth anniversary of the Department in 2021. *Imperfect History*, funded by the Henry Luce Foundation and comprising an exhibition, publication, digital catalog, and complementary programming, centered on the themes of historical bias, close looking, visual literacy, and the centrality of visual material in understanding the complicated history and culture of the United States.[^3] The corresponding seminar, *Urban In-sights: A Workshop in American Visual Culture and Literacy from the Eighteenth through the Early Twentieth Century*, the subject of this essay, was also conceived and planned within this framework.

As cogently stated by Harvard University art history professor Sarah Lewis, “Being an engaged citizen requires visual literacy.”[^4] Increasingly, scholars like Lewis in the art history, visual culture, and curatorial fields as well as Jennifer Roberts, Shawn Michelle Smith, Makeda Best, and La Tanya Autry advocate for their peers to work as agents in the facilitation of individuals learning to read a visual work to the same extent as they learn to read a printed text.[^5] As articulated by Roberts, “just because you have *looked* at something doesn’t mean that you have *seen* it. Just because something is available instantly to vision does not mean that it is available instantly to consciousness . . . [A]ccess is not synonymous with learning. What turns access into learning is time and strategic patience.”[^6] One needs to learn to look in order to read a word or a pictorial detail.[^7] *Reading* a graphic work necessitates that an individual “see” what is within and what is not within the literal and figurative margins and borders of an image.

Within this ethos in the art history, visual culture, and curatorial fields, Graphic Arts Department curators Erika Piola and Sarah Weatherwax began the planning of *Imperfect History*, including *Urban In-sights*. In 2017, as the fiftieth anniversary of the Department approached, the idea for a visual literacy seminar that they had contemplated in the past fomented during the conception of the multi-part, grant-funded commemorative project. Department holdings, comprised of over 150,000 original works of art, prints, photographs, and ephemera which mirror the society that produced, collected, and viewed them over nearly three hundred years, would provide the foundation for the project and seminar.

Collaborative instruction and external funding would be essential. With this point in mind, Piola and Weatherwax reached out to colleague Anne Verplanck, an associate professor of American studies at Penn State, Harrisburg. Verplanck had been a longstanding researcher in the graphic arts collections, a former VCP Fellow, and an advocate for teaching with graphic materials as primary sources.[^8] Having secured Verplanck as a collaborator in developing the seminar, the construction of a program outline to facilitate institutional administrative support and grant writing began in 2017. The outline contained an overview of the program goals and expected outcomes; intended applicant base and number of attendees; internal and external logistics, such as tentative staffing, including programming support and guest presenters; a syllabus and day-by-day schedule; tentative pre-reading list; possible funders; and a budget incorporating stipends for all attendees. Between 2017 and 2019, this team garnered institutional support and solicited a funder, the Henry Luce Foundation.

The organizers of the seminar, initially conceived with a $20,000 budget (Table 1) and to be held over five days in summer 2020, aimed to enhance the skills of historians, art historians, archivists, curators, and university, library, and museum professionals, as well as graduate students who use images to interpret American art, history, and culture. Primary instructors Piola, Weatherwax, and Verplanck would work with twelve participants and augment and strengthen their ability to identify, read, and analyze graphic material at the Library Company of Philadelphia; urban topics would be a focal point. They would also engage guest speakers as lecturers to investigate select topics such as city panoramas as primary sources. Additionally, they would plan field trips to view related collections and to observe process demonstrations at local art institutions and organizations such as the Pennsylvania Academy of the Fine Arts.

<table><tbody><tr><td><strong>Original Budget (2019)</strong></td><td></td><td><strong>Modified Budget (2020)</strong></td><td></td></tr><tr><td>Student housing and meals</td><td>$6,000</td><td>Student stipends (housing, transportation, and meals / 12 students at $500 per student)</td><td>$6,000</td></tr><tr><td>Student transportation stipends ($450 @ 12 students)</td><td>$5,400</td><td>Professor’s honorarium</td><td>$2,000</td></tr><tr><td>Professor’s honorarium</td><td>$4,000</td><td>Guest instructors (two instructors at $450 per instructor)</td><td>$900</td></tr><tr><td>Guest instructors (six instructors at $450 per instructors)</td><td>$2,700</td><td>Meals, catering, and supplies</td><td>$1,100</td></tr><tr><td>Printing and postage</td><td>$1,000</td><td></td><td></td></tr><tr><td>Meals and catering</td><td>$900</td><td></td><td></td></tr><tr><td></td><td></td><td></td><td></td></tr><tr><td>Total</td><td>$20,000</td><td>Total</td><td>$10,000</td></tr></tbody></table>
<figcaption>Table 1: <em>Urban In-sights: A Workshop in American Visual Culture and Literacy</em>—Original and Modified Budgets, 2019–2020.</figcaption>

The participants would develop their abilities to (1) place visual materials in specific social, political, economic, racial, and religious contexts (among others) to understand more fully how and why graphics shaped and were shaped by these contexts; (2) identify key graphic processes and recognize when they were produced, used, and circulated; (3) conduct primary and secondary research using prints, photographs, and other graphic materials housed at the Library Company; (4) interpret the textual clues in the visual materials to aid in the identification of graphic processes and more fully construe relevant cultural contexts; and (5) understand and identify basic conditions or conservation issues that affect the way an image appears.

During the first two days of *Urban In-sights*, participants would learn about different graphic processes, including intaglio, relief, lithography, and daguerreotype. Prior to the program, participants would receive assigned historical and contemporary readings to prepare them for the topics to be examined. Planned readings included excerpts from William Dunlap’s *History* *of* *the Rise* *and* *Progress of the Arts* *of Design in the United States* (1834); Marcus Aurelius Root’s *The Camera and the Pencil* (1864); and Richard Benson’s *The Printed Picture* (2008). Piola, Weatherwax, and Verplanck would provide about thirty source materials as PDFs and online links. By the final stages of planning, they culled and amended the pre-readings to twenty sources (Appendix 1). The first days would also include the viewing of introductory videos about historical printmaking and photographic processes produced by individuals working at art institutions, including the Minneapolis Museum of Art, George Eastman Museum, and the Getty. Furthermore, the instructors would supply participants with examples of different graphic mediums from the Department’s teaching collection for close and hands-on physical review.

For sessions later in the week, the instructors intended for the guest speakers to highlight urban subject matter and the ways that graphics could be incorporated into the teaching and interpretation of those topics. The presentations would explore relevant examples, such as business panoramas and streetscape photographs, ephemera as primary resources for research, and social and cultural histories of popular art genres—including maps, political cartoons, advertisements, and parlor prints. With the remaining time, participants would conduct their own research on a specific visual material within the Graphic Arts Department’s collections. The seminar would conclude with participants presenting their research to one another.

Piola and Weatherwax devised the seminar as part of the early planning of the *Imperfect History* project and included it in initial budgets presented to the Luce Foundation in 2018. The Luce Foundation prioritized other elements of the project for funding. Consequently, Piola, Weatherwax, and Verplanck pursued other sources, and in the fall of 2019 the Philadelphia Museum of Art’s Center for American Art granted $5,000 to support the implementation of the program. At this juncture, the seminar organizers decided to re-envision the seminar as a three-day workshop with a $10,000 budget (Table 1). They reduced the number of guest speakers, field trips evolved into one offsite process demonstration, and a hands-on research experience with a graphic artwork came further to the forefront in the workshop schedule. Attendees would choose the work they would interpret and present from the Department’s collections at the start of the program, as opposed to the middle. However, the team agreed that hands-on, close-looking engagement with graphic materials remain a consistent thread. By the summer of 2020, the workshop organizers received the remaining $5,000 through the Terra Foundation for American Art.[^9]

In early 2020 Piola and Weatherwax collaborated with library systems staff to create a website incorporating the program outline, overview text created for the grant applications, instructors’ biographies and headshots, application instructions, travel and accommodations information, and the pre-reading list (Appendix 2). On March 13, 2020, about two weeks before the deadline to submit applications to attend the program, the COVID-19 pandemic forever altered the daily operations of the Library. The Library Company collection staff’s roles in facilitating virtual access and knowledge about the holdings entered a new phase. Little did the organizers realize that the new phase and level of virtual access to collections would continue and further evolve in the year to come.

## Planning during a Pandemic

On March 13, 2020, the Library Company closed to the public, and staff began to work remotely. What was initially thought of as a brief disruption to normality became the “new normal” as individuals around the world searched for new ways to function in environments that strongly discouraged or even forbade in-person interactions. Workshop organizers turned to virtual tools to continue planning the event.

In early April 2020, Piola, Weatherwax, Verplanck, and Henry Luce curatorial fellow Kinaya Hassane met via Zoom to select twelve attendees and five alternates from a pool of thirty-eight candidates to attend a workshop scheduled for late July 2020. The workshop attracted a broad array of participants at all stages of their careers—graduate students as well as more advanced academics, museum curators, librarians, archivists, and others. All shared responsibility for collections or otherwise engaged with graphic collections in their research. Although some held the title of print curator or the equivalent, all applicants wanted to expand their ability to identify and interpret images. The organizers still hoped for an in-person event but acknowledged in the acceptance notification email “that given the current national health crisis all our future plans are uncertain.”[^10]

Discussions between the Library Company’s administration and the workshop organizers continued; this dialogue centered around the feasibility of postponing an in-person workshop until either fall 2020 or summer 2021 or switching to a virtual format and keeping the July 2020 schedule. None of the options seemed ideal. Many of the presenters and attendees were affiliated with a college or university and would only be able to attend an in-person workshop during the summer months, and it was growing increasingly unlikely that pandemic conditions were going to improve substantially by the fall. How long funders would maintain their support for the project was unknown. Organizers also questioned the effectiveness of a workshop predicated on close looking and interacting with original graphic items when presented in an online environment. One might address the content of the graphics virtually, but conveying the physicality of the graphics as objects would be much more difficult. The virtual world, for example, often masks the comparative size of items. Likewise, looking at a screen deprives attendees of the experience of holding a paper item in their hands, feeling its weight, examining its recto and verso, or observing it in raking and direct light while looking for clues about the type of paper used or whether color was printed or applied by hand. Could the available interactive online graphic tools overcome these obstacles?

By mid-May, organizers decided to postpone the workshop until the summer of 2021 when it could be held on-site at the Library Company. With this premise, planning continued, albeit at a slower pace. Responding to the extraordinary conditions imposed by the pandemic, the workshop’s funder, the Center for American Art at the Philadelphia Museum of Art, agreed to carry its financial support for the project over to the following summer. We also enjoyed support from the Terra Foundation for American Art. All presenters and attendees confirmed their interest and availability to attend a summer 2021 workshop in Philadelphia. As Weatherwax suggested to Verplanck in the fall of 2020, “I think we need to assume that participants will have to wear masks, social distance, etc., but I am cautiously optimistic that having had a year of having to follow these practices will enable us to organize a safe (and worthwhile) experience for everyone. We won’t all be able to gather around looking at the same prints or photos, but I am willing to try to be creative to make this work.”[^11]

Unfortunately, as 2021 commenced, the world remained mired in an unrelenting pandemic, and the organizers realized they would need to employ more creativity than they anticipated previously to make the event, now scheduled for June 28–30, 2021, succeed. Piola, Weatherwax, and Verplanck developed a survey to send to attendees and presenters to gauge their comfort level in traveling to Philadelphia and participating in the scheduled workshop activities, including a dinner and a field trip (Appendix 3).

Organizers received fifteen responses, and the results indicated an overall hesitation to engage in a group activity such as a dinner and to use mass transit or a ride-share service for the field trip.[^12] While survey respondents expressed some frustration at trying to predict conditions six months in the future with unknowable factors such as vaccination availability and the rate of COVID-19 transmission in Philadelphia, the results reinforced conversations already taking place between Piola, Weatherwax, and Verplanck. Despite a strong preference to hold the visual literacy workshop as an in-person event, it would be necessary to devise a virtual workshop, or cancel the event.

Wanting to meet the expectations of the funders and participants and firmly believing in the value of holding a visual literacy workshop, the organizers faced the challenge of holding a virtual event centered on interactions with and examinations of original materials while attendees remained physically separated from both one another and the collections of the Library Company. As the pandemic entered its second year, the organizers were well aware of the opportunities and the limitations of online platforms. Whether Zoom, Cisco Webex, or Microsoft Teams, videoconferencing software use exploded in 2020.[^13] Individuals around the world may have turned in droves to Zoom and other similar tools to stay connected, but the resulting “Zoom fatigue” phenomenon factored as a significant challenge for workshop organizers.[^14] If the attendees were typical, their online pandemic-era experiences were not entirely satisfying. Studies released in July 2020 surveying one thousand undergraduate students and four thousand instructors from fifteen hundred colleges revealed that students reporting high levels of satisfaction in a course that was important to them dropped from 51 percent to 19 percent after switching to virtual learning during the pandemic, and three-fifths of the instructors admitted to struggling to keep their students engaged.[^15] As a senior writer for the *Chronicle of Higher Education* suggests, “Feeling comfortable with classmates, wanting to engage in debates and share ideas, having a sense of belonging—these are all critical components of a vibrant classroom, and something particularly challenging to create online.”[^16]

As a result, in early 2021 workshop organizers faced an important question: how does one reconceptualize and structure a visual literacy workshop to attract burned-out professors, students, curators, librarians, and archivists and tempt them to participate in three more days of screen time? Without the size limitations imposed by a physical space, organizers opened the workshop to everyone on the waiting list; ultimately, fifteen individuals attended the workshop. Hoping to foster a sense of community, despite the physical separation, attendees wrote brief autobiographies to share on Google Drive prior to the workshop. All the instructors agreed to present virtually, and by eliminating travel time for a planned field trip, the organizers made room for one more speaker on the schedule. To help combat fatigue, the online schedule was designed to move regularly among the various presenters with ample time built in for question-and-answer periods and breaks between sessions. Because attendees would no longer experience the opportunity to conduct research on-site at the Library Company, as envisioned originally, Piola and Weatherwax selected digitized items from the collection for which some research material was available, created a Google spreadsheet with links to these images, and requested that each attendee choose an item of interest to present about briefly at the beginning and conclusion of the workshop. Attendees also used the Google spreadsheet to sign up for an optional one-on-one fifteen-minute mentoring session with Piola, Weatherwax, or Verplanck. Prior to these meetings, the organizers had scanned relevant materials and provided links to additional resources that could guide attendees’ research and interpretation of their selected graphics.

The real reconceptualization breakthrough for the workshop, however, occurred when Piola suggested mailing packets filled with examples of different historic printing and photographic processes to each attendee (Fig. 1). Even if attendees could not gather around a table to examine the same item under the hands-on guidance of an instructor, they would at least possess a physical example of the particular process under review and could learn virtually from a series of questions and observation exercises that related to the item. Similarly, organizers diverted money originally budgeted for the workshop’s catered events to the purchase of both packet items and magnifiers enabling close viewing (Table 2).[^17] Since organizers acquired these items, attendees would be able to handle them freely and in ways that might not have been permitted with collection items. Workshop participants would now be able to observe the thickness and squashed ink of the printed lines with their magnifier, compare the sizes of different formats and genres, and hold items up to the light to look for watermarks or the sheen of various photographic processes. Attendees could actively interact with the graphics instead of passively receiving information about them. The items would also be theirs to keep for future study and reference.

![Figure 1: Anne Verplanck and Sarah Weatherwax prepare process packets to send to visual literacy workshop attendees.](../../../images/issues/40/piola/figure1.jpg)

<table><tbody><tr><td><p><strong>Final Budget (2021)</strong></p><p><em><strong>Urban In-sights: A Workshop in American Visual Culture and Literacy</strong></em></p></td><td></td></tr><tr><td>Student stipends (15 students at about $350 per student)</td><td>$5,300</td></tr><tr><td>Professor’s honorarium</td><td>$2,000</td></tr><tr><td>Guest instructors (three instructors at $450 per instructor)</td><td>$1,350</td></tr><tr><td>Postage, historical packet supplies, miscellaneous</td><td>$1,350</td></tr><tr><td></td><td></td></tr><tr><td>Total</td><td>$10,000</td></tr></tbody></table>
<figcaption>Table 2: <em>Urban In-sights: A Workshop in American Visual Culture and Literacy</em>—Final Budget, 2021.</figcaption>

Keeping in mind the modest sum of money available to spend, Piola, Weatherwax, and Verplanck devised a list of the most important printing and photographic processes to include in the packets and scoured the online auction site eBay looking for sellers with appropriate material at the right price. Ultimately, nineteen different sellers provided materials for the packets. By early June 2021, each of the attendees received a package containing an identified albumen photograph, a chromolithograph, a gelatin silver photograph, a line engraving, a lithograph, a mezzotint, a photomechanical print, a tintype, and a wood engraving, as well as a 60×–120× LED lighted pocket microscope. As the workshop date moved closer, instructors completed their PowerPoint presentations, Piola, Weatherwax, and Verplanck held Zoom practice sessions, and the workshop’s funders and guest instructors received invitations to join any or all of the sessions.

## Executing the Workshop

In revising the workshop for an online synchronous environment, Piola, Weatherwax, and Verplanck deliberately varied the presenters and types of activities each day, kept sessions relatively brief, and included frequent breaks. The organizers also provided course materials in multiple formats. Because they organized and made available in a Google Drive folder handouts that described print and photographic processes, terminology, and relevant resources for research, during the sessions participants could focus on the presentation or activity—rather than note-taking—and refer to information on complex topics such as print processes. Throughout the workshop, the organizers used PowerPoint presentations to illustrate main ideas, reinforce or summarize information, and account for varied learning styles.[^18] Each speaker created his or her own presentation; the organizers made no attempt to impose a consistent style among presentations. For example, Jeffrey Cohen, in his talk “Finding the Lost City, on Paper,” employed maps, charts, and prints to place Philadelphia’s late nineteenth-century urbanization and industrialization in an international context. He noted the interrelated social, economic, and technological changes that had an impact upon image-related businesses. Centrally, the images he chose demonstrated the ways in which graphics such as maps and prints reinforced the vitality of the city of Philadelphia and its industries at the time. Cohen was one of the few presenters who used a lecture-like format, which he chose in order to cover extensive material in a short period of time. As we detail below, visual and oral components of instructors’ presentations complemented one another and ensured that participants with different learning styles comprehended the material. Following the workshop, the organizers provided attendees with PDFs of the PowerPoints, links to the recorded Zoom sessions, and transcripts of session chats that included questions, answers, and comments among the presenters and participants.

The first day, after general introductions to the Library Company, its Visual Culture Program, and the instructors and attendees, we launched into tools for research, covering such critical online sites as Graphics Atlas, Greater Philadelphia GeoHistory Network, and the Getty’s United List of Artists’ Names.[^19] Next, Verplanck led a short session on identifying condition issues in works of art on paper. The slide deck included information about paper chronology and close-up samples of key types of paper. Participants were then prompted to scrutinize their prints and compare the paper in the engraving to that in the *Harper’s Weekly* wood engraving example. The presentation attended to common condition problems such as light and water damage, foxing, and fading. Attendees were also asked to examine images in their packages and volunteer to describe condition issues they identified. In PowerPoint, Verplanck offered examples of damage to cased photographic images, such as tarnish and weeping glass. Links in the PowerPoint presentation guided participants to sources containing additional information related to specific topics, including paper-processing history and iron gall ink.

After a break and an overview of the Library Company’s graphics collection, we asked attendees to introduce themselves by describing the image they had chosen to research and state why that particular item resonated with them; a slide deck allowed the others to see the images. This approach gave participants the opportunity to address their research interests and find further points of connection over the sometimes-awkward medium of Zoom.

In the afternoon, the workshop addressed photographic images. The organizers used the same format through the workshop: relatively short presentations and opportunities for close-looking inspection interspersed with “deep dives,” often in the form of case studies, to amplify relevant points relating to visual analysis. The instructors tried to balance survey-type introductions with more focused sessions that were related to their own intensive research on specific topics.

As we note above, participants viewed videos of photographic and print processes prior to the workshop’s commencement (Appendix 1). Weatherwax presented a brief history of photography in Philadelphia, one of her areas of specialty; this discussion reinforced the progression of techniques and processes as well as the particularities of the city’s practitioners and patrons. Verplanck then spoke about the daguerreotypists T. P. and D. C. Collins and explained the ways in which extensive documentation of the firm allows her to reconstruct its successes and challenges, to understand the role of competition among local firms, and to place the Collinses in the context of other small businesses.[^20] Following questions and dialogue on these topics—including a lively exchange in the chat feature of Zoom—the instructors proceeded to consider late nineteenth-century photographic processes. Weatherwax and Piola provided close examinations of photographers John Moran and John Frank Keith. During these presentations, participants were invited to look at examples in their packets. Deeming the remaining time insufficient for an optimal learning experience, the leaders moved Piola’s session on postcards and halftone processes to Tuesday morning.

A highlight of the first day included a demonstration of the wet collodion process by visual artist, educator, and documentarian Lisa Elmaleh. The original plan was for participants to travel three miles from the Library Company to the Halide Project, a nonprofit organization dedicated to supporting the continued practice and appreciation of film and historic process photography. Instead, Elmaleh presented the workshop from her studio in West Virginia via Zoom.[^21] In addition to demonstrating artists’ methods of taking and developing tintypes, Elmaleh showed attendees her portable wet-plate darkroom. This session allowed participants to understand the limits and possibilities contemporary photographers face when working with historical processes. The remote nature and location of the demonstration and attendant broadband problems limited opportunities for continuous questions and remarks; this issue would not have surfaced had the demonstration occurred live in Philadelphia, as originally planned.

Prints were the primary topic on the second day. As was the case with photographic images, the instructors started by covering the history of relevant processes. While Philadelphia engravers, etchers, and other artists were connected to activities and movements both nationally and internationally, a focus on this city allowed for more specificity. These conclusions could, in turn, be contrasted with printed work in other places in the United States and abroad, materials that our participants often noted. Using the chat feature, attendees could share links to relevant online resources, such as New York Heritage (https://nyheritage.org/). Following the format of the section devoted to photographic images, the instructors proceeded chronologically, starting with wood engraving and other relief processes. After Weatherwax’s overview, Piola provided a case study of nineteenth-century periodical illustrations, one of the key uses of prints at the time. She pointed attendees to their packets containing examples of these images; Piola asked them to consider the size, location on a given page, printing technique, as well as content of the periodicals.

Verplanck then covered intaglio processes. PowerPoint permitted the dissemination of high-quality close-up images that showed some of the differences among etching, engraving, and other techniques. As instructors presented on each technique, participants were directed to look at examples of particular techniques in their packets with their high-quality magnifiers. The instructors had chosen prints that clearly demonstrated the physical manifestations of processes, such as the depth and curve of a burin, needle, or other tool or the interactions between ink and paper. Moreover, they had ensured that each participant possessed similar examples, such that their observations and questions would reinforce everyone’s learning.

After a break, Piola introduced lithography; in this instance, a contemporary lithograph, one found in their packets, provided participants with an example for close examination. Next followed commentary on two important case studies, lithographic printer P. S. Duval (Weatherwax) and printsellers James S. Earle and William Smith (Piola).[^22] In addition to amplifying the study at hand in the production, distribution, and reception of prints, these case studies highlighted the research methods, techniques, and sources employed by the instructors and thus suggested productive models for attendees to adapt to their own work. The case studies demonstrated means of using the object-rich and document-rich collections to which participants may gain access at their own or other institutions. The instructors toggled between larger questions—the impact of technological changes such as steam-powered presses and economic events like the Panic of 1839—and tools for in-depth research on specific visual and documentary elements that informed their conclusions. As an introduction to additional research tools, Verplanck spoke briefly about engraver James Barton Longacre, whose bankruptcy correspondence housed at the Library Company sheds light on his career and finances. With the other instructors contributing to this discussion, she then presented online research tools related to prints and other works of art, including genealogy, portrait databases, and newspapers; participants had been given bibliographies and site lists in advance (Appendix 4).

Following this morning session, attendees enjoyed an hour-and-a-half lunch break, which allowed all to recharge and attend to professional and personal duties. The break also provided “wiggle room” should a presentation or conversation exceed the scheduled time. In keeping with the pattern of varying presentation styles, Wendy Woloson, professor of history at Rutgers University, Camden, delivered a broad, interdisciplinary application of graphics research. In part deriving from her scholarship and her 2012 Library Company of Philadelphia exhibition, *Capitalism by Gaslight: The Shadow Economies of Nineteenth-Century America*, Woloson emphasized the “triangulation” process of bringing together visual culture, material culture, and textual sources in order to understand more fully the dynamics of these materials and the ways in which they can help individuals in the twenty-first century better understand how gamblers, prostitutes, thieves, counterfeiters, and the impoverished lived their urban lives.[^23]

Optional fifteen-minute individual meetings between attendees and workshop leaders on preselected graphic items concluded the day (Figs. 2 and 3). The instructors used this time to answer questions, suggest and/or provide further research avenues and sources, and enhance close-looking skills to help participants prepare for their five-minute presentations the following afternoon. Although this exercise would have proven more effective in person and with more individual interactions and time for research, the instructors believed it honed attendees’ skills, knowledge, and comfort levels in analyzing and interpreting graphic materials. Had we met in the Library Company’s Graphic Arts Department reading room and had access to original materials, secondary literature, and participants’ and instructors’ insights, this experience would have been more meaningful in unexpected and welcome ways.

![Figure 2: <em>The Follies of the Age, Vive La Humbug!!</em> (Philadelphia, 1855\[?\]). Lithograph. Library Company of Philadelphia, P.9624. https://digital.librarycompany.org/islandora/object/Islandora%3A65087.](../../../images/issues/40/piola/figure2.jpg)

![Figure 3: Attributed to James B. Shaw, \[Sallie Venning Holden\], ca. 1893. Tintype. Library Company of Philadelphia, P.9367.7. https://digital.librarycompany.org/islandora/object/digitool%3A130696.](../../../images/issues/40/piola/figure3.jpg)

The second day ended with an optional happy hour on Zoom, guided by the question “What would you like to have done if you were in Philadelphia?” Our initial plan had consisted of an in-person dinner that provided opportunities to interact with sponsors, guest speakers, and other local individuals with a deep interest in graphic arts. Our secondary strategy for the virtual realm was to allow all contributors to make dinner or use part of their stipend for a takeout dinner of their choice. Realizing that some participants would be exhausted, the organizers devised an optional social hour on Zoom and invited representatives from the funding organizations and guest speakers to join. All the attendees participated, and a collegial atmosphere developed despite the virtual environment.

The workshop deliberately started “late”—10:00 a.m.—the following day (Fig. 4). Our intention was to broaden the range of materials that displayed the physical and functional aspects of Philadelphia as an urban social environment and concentrated on maps, bird’s-eye views, and portraiture. Piola began the day with an introduction and then focused on printed views, while Weatherwax addressed photographic images. Jeffrey Cohen, Bryn Mawr College term professor in the growth and structure of cities, delivered a presentation entitled “Seeing the Nineteenth-Century City, on Paper.” He demonstrated various ways to use visual culture to expand our knowledge of urban places by delving into prints, drawings, and photographs that recorded old landmarks, lost landscapes, and transformed settings. Cohen discussed illustrated panoramic directories, including streetscapes, which allow modern scholars to recover the placement of and relationships among businesses.[^24] His long-term study and assemblage of accessible data offer a template for the study of other cities and towns individually or in comparison. Weatherwax and Piola followed with a presentation on ways of analyzing and interpreting advertising prints and photographic images. Verplanck then spoke about urban portrait choices, a subject that detailed the wide range of mediums available as well as the venues for viewing and acquiring art in Philadelphia.

![Figure 4: Screenshot of *Urban In-sights* workshop Zoom session, June 30, 2021.](../../../images/issues/40/piola/figure4.jpg)

The workshop concluded with five-minute presentations by the participants. Our initial plan had imagined that attendees would create blog entries, social media posts, and the like. Given various constraints, including time, this plan did not materialize. Nonetheless, participants brought fresh eyes and perspectives that can be incorporated into future interpretations of the collection. Many spoke at length and with great enthusiasm. Regina Blaszczyk, for example, found a missing portion of the Centennial advertisement in the Library Company collections (\#11423.F.14) online in another repository, a discovery that enabled her to reconstruct more fully the circulation of the image and its meaning. Graduate student Samuel Backer analyzed a late nineteenth-century trade card (\#1975.F.18) and situated the image advertising “Aromatic Pino-Palmine Mattress” in the transnational context of an equatorial product marketed by a Boston manufacturer and graphically designed by a Philadelphia printer.

A survey, completed by participants within a week following the end of the workshop, provided valuable feedback (Appendix 5). Key survey questions included: What was your biggest takeaway from the workshop? What aspects of this workshop helped you learn? What impact do you think this workshop will have on your work? For those in museums, libraries, teaching or other profession: How might what you gleaned from this workshop affect the public and/or your students?

The overall tone of the comments was positive. The organizers learned that participants appreciated our strategy of breaking up the days into small chunks with varied speakers and topics. Attendees also praised the pre-readings and videos. Constructive comments included a preference for more direct references to the readings in the presentations and having access to the PowerPoints in advance to facilitate note-taking and learning. Also, several participants commented that they would have preferred more active learning experiences. Suggestions included breakout sessions in which small groups with a shared interest could focus on particular images or types of images together, including those in their packets, with an instructor. Also, individuals addressed the desirability of taking additional time to concentrate on the materials in the packets as a way to strengthen learning as well as allow for more active learning experiences. Attendees’ access to packets of material that they can handle (in addition to the collection materials they might observe closely) will be useful for future in-person workshops, regardless of whether participants take them home or they stay with the institution. These ideas will be beneficial in planning future workshops, whether in person, online, or hybrid.

Workshop organizers reduced the initial five-day plan to three days prior to the pandemic because the funding, while very generous, could not accommodate that length of a program. Had the workshop encompassed five days with an in-person modality, it would have provided a richer experience for attendees. Additional time would have permitted extended, firsthand examination of graphics, intensive dialogues about the readings, more in-depth research by participants, additional consultation time with the instructors, and field trips to other institutions and artists’ studios. However, a five-day workshop would not have been feasible online. Although increased time would have allowed time for many of the activities already noted, the intensiveness of the activities and what might be termed “cumulative burnout” from the prior fifteen months of the pandemic would likely have resulted in diminishing returns after the third day of the workshop.

Networking is a final facet of the workshop worth noting. One participant, for example, was quick to enlarge everyone’s network with LinkedIn invitations. It is our hope that the attendees, instructors, and guest speakers continue conversations and rely on one another for advice. The organizers also hope that this activity could expand users of the Library Company’s collections, especially those in the Graphic Arts Department. Indeed, in the months that followed the workshop, several participants consulted with Weatherwax and Piola, both in person and electronically, regarding overlapping collections and research interests. The instructors have worked with individual attendees to develop opportunities to present work to specialist and general audiences as well as to enable site visits by specialist groups. The diversity of individuals’ research interests ensures that relationships formed in the workshop will have an impact on several fields. The instructors surmise that an in-person workshop would have allowed for more productive networking during sessions, meals, and breaks as well as conversations at other times. Nonetheless, we were impressed by the rapidity of constructive exchanges through email and in the Zoom chat function during the workshop and after it had ended.

Unfortunately, there was limited scholarly literature or practical information to guide the organizers in online learning for a specialized adult audience. The instructors were able to account for and adapt to a changing e-learning environment in which adult users were increasingly adept at employing Zoom and other tools but were often “burned out” by both the digital platform and the pandemic generally. Yet there was—and remains—little guidance in the scholarly literature or elsewhere for this type of learning, irrespective of the pandemic. Potential comparables include conservatory-based music education, where researchers found a need “to compensate for the reciprocal lack of physical presence” as well as “novel collaborative interactions with peers” that parallel the experiences in the *Urban In-sights* workshop.[^25] A range of subject specialists looking at uncommon two-dimensional visual materials occurs in relatively few venues, whether in person or online.[^26]

The workshop organizers accomplished the goals they had set regarding expanding participants’ visual literacy skills. These individuals will, in turn, deploy their new skills interpreting printed and photographic materials to a broad range of audiences. The organizers were especially pleased that the curators, librarians, graduate students, professors, and others who partook in the workshop ranged from emerging to experienced professionals; their synergy added much energy to this virtual event. Finally, the organizers believe that the lessons learned from pivoting the workshop from a face-to-face gathering to a virtual setting will benefit their own and others’ planning of future learning opportunities related to visual resources and their close reading.

## Appendix 1: *Urban In-sights: A Workshop in American Visual Culture and Literacy from the Eighteenth through the Early Twentieth Century*—Pre-Reading List

<div class="noindent">

**Eastman House Photographic Process Series**: 

https://www.eastman.org/processvideos.

**Minneapolis Institute of Arts and YouTube Printmaking Processes**:

Relief: https://www.youtube.com/watch?v=O0skLwaFpn0.

Intaglio: https://www.youtube.com/watch?v=SNKn4PORGBI&t.

Mezzotints: https://www.youtube.com/watch?v=jikAwCcc0uI.

Lithography: http://www.youtube.com/watch?v=JHw5_1Hopsc.

**Historical Primary Sources:**

M. A. Root, *The Camera and the Pencil: or, The Heliographic Art* (Philadelphia: M. A. Root, 1864; facsimile ed., Pawlet \[VT\]: Helios, 1971), 32–48.

“Lithography,” *Analectic Magazine,* July 1819, 67–73.

**Secondary Sources:**

Jessica Lahey, “Relearning the Lost Skill of Patience,” *The Atlantic*, November 15, 2013. https://www.theatlantic.com/education/archive/2013/11/relearning-the-lost-skill-of-patience/281482/.

Michael Leja, “Fortified Images for the Masses,” *Art Journal,* 2011, 61–83.

Brian Maidment, *Reading Popular Prints, 1790–1870* (New York: St. Martin’s Press, 1996), 1–26.

Elizabeth Milroy, *The Grid and the River:* *Philadelphia’s Green Places, 1682–1876* (University Park: Pennsylvania State University Press, 2016), 155–79.

Erika Piola, introduction to *Philadelphia on Stone*, ed. Erika Piola (University Park: Pennsylvania State University Press in association with the Library Company of Philadelphia, 2012), 12–33.

Vanessa R. Schwartz and Jeannene M. Pryzblyski, “Visual Culture’s History: Twenty-First Century Interdisciplinarity and Its Nineteenth-Century Objects,” in *Nineteenth-Century Visual Culture Reader*, ed. Vanessa R. Schwartz and Jeannene M. Przyblyski (New York: Routledge, 2004), 3–14.

Anne Verplanck, “‘The shadow of your noble self’: The Reception and Use of Daguerreotypes,” *West 86th: A Journal of Decorative Arts, History of Design, and Material Culture* 24, no. 1 (Spring/Summer 2017): 47–73.

**Online Sources:**

Getty: Atlas of Analytical Signatures of Photographic Processes. http://www.getty.edu/conservation/publications_resources/pdf_publications/atlas.html.

Graphics Atlas. http://www.graphicsatlas.org/.

The MET Timelines. https://www.metmuseum.org/toah/essays/.

**Graphics and Digital Collection Catalogs:**

Library Company of Philadelphia. https://digital.librarycompany.org/islandora/object/islandora%3Aroot.

American Antiquarian Society. http://www.americanantiquarian.org/catalog.htm.

Library of Congress. http://www.loc.gov/pictures/.

New York Public Library. https://digitalcollections.nypl.org/.

</div>

## Appendix 2: *Urban In-sights: A Workshop in American Visual Culture and Literacy from the Eighteenth through the Early Twentieth Century*—Program

<div class="noindent">

### Urban In-sights: Program

#### Overview of Workshop

Sessions will concentrate on learning about different graphic processes; examining the social and cultural history of popular art genres; and interpreting graphics, including ephemera, with urban subject matter to be used as examples and models in teaching.

#### Schedule

##### Day One: Photographic Philadelphia

9:00 AM–10:30 AM
* Welcome, introduction, goals of workshop
* Backgrounds of participants and instructors
* Visual Culture Program
* Online tools for close looking of graphics; evaluating condition issues
* Q & A

10:30 AM–10:45 AM
* Break

10:45 AM–12:30 PM
* Graphics collections at Library Company of Philadelphia
* Participants describe why they selected a particular graphic item for research.

12:30 PM–1:45 PM
* Lunch

1:45 PM–3:00 PM
* Introduction to history of photography in Philadelphia
* Daguerreotypes/Ambrotypes/Tintypes
* Case study: Collins Daguerreotype Studio
* Q & A
* Albumen/Gelatin silver/Photomechanical
* Case studies: John Moran; John Frank Keith; postcards
* Q & A

3:00 PM–3:15 PM
* Break

3:15 PM–4:30 PM
* The Halide Project Presentation:
* Wet collodion process talk and demonstration by Lisa Elmaleh
* Q & A

##### Day Two: Philadelphia in Print and “Urban World in the Parlor”

9:00 AM–10:30 AM
* Introduction to Day Two schedule
* History of printing processes in Philadelphia
* Wood engraving/relief
* Case study: eighteenth-century and nineteenth-century periodical illustrations
* Q & A
* Intaglio
* Case study: James Barton Longacre
* Q & A

10:30 AM–10:45 AM
* Break

10:45 AM–12:30 PM
* Lithography
* Case study: P. S. Duval, lithographic printer
* Case study: James S. Earle/William Smith as printsellers
* Models and tools for content research
* Q & A

12:30 PM–2:00 PM
* Lunch

2:00 PM–3:00 PM
* *“Triangulations, and Viewing the Urban World in the Parlor”*
  <br/>Dr. Wendy Woloson, guest lecturer.
  
  This session will emphasize the “triangulation” process of bringing together visual culture, material culture, and textual sources to help us understand various pasts, and particularly pasts of the subaltern. While the underclass tended to escape documentation in mainstream source material, their history can still be recovered if we take a more expansive view of the archives and utilize more creative interpretive approaches. Using her 2012 Library Company exhibition, *Capitalism by Gaslight: The Shadow Economies of* *Nineteenth-Century America* as a case study, Dr. Woloson will discuss how we can recover the lives of those who otherwise might escape the historical record by putting various historical sources in conversation with each other and reading those sources against the grain. The session will illustrate how visual culture—bolstered by other primary sources—can be leveraged to help us better understand how gamblers, prostitutes, thieves, counterfeiters, the impoverished, and their ilk lived their lives. Children’s book illustrations, trade cards, caricatures, police manuals, portrayals in reform literature, graphic exposés, and other contemporary works will be considered.

3:00 PM–3:15 PM
* Break

3:15 PM–5:00 PM
* Optional tutorial time (individual 15-minute meetings)
* Scheduled one-on-one time with workshop leader to suggest further research possibilities and further build on close-looking skills for selected graphic item to help prepare for a five-minute presentation on Day Three.

6:00 PM–6:45 PM
* Social “hour”
* Join us for relaxed conversation. Feel free to snack and drink as we talk.

##### Day Three: “Urban In-Site”: The Physical and Vernacular City

10:00 AM–12:30 PM
* Introduction to Day Three schedule
* Maps
* Bird’s-eye views (prints and photographs)
* Q & A

* *“Seeing the Nineteenth-Century City, on Paper”*
  <br/>Dr. Jeff Cohen, guest lecturer
  
  Philadelphia has been blessed with an extraordinarily rich body of images recording old landmarks and transformed settings, which can be classified as pieces of a lost landscape captured in drawings, prints, and photographs. A few nineteenth-century artists were especially prolific, notably David J. Kennedy, Benjamin R. Evans, and Frank H. Taylor. Each of them produced hundreds of views, sometimes with the active patronage of collectors whose interest substantially amplified this iconographic legacy. Digital images and detailed cataloguing have now made these items more discoverable than ever.

  These individual drawings are joined by a special type of long, flat-on print that tracks whole blocks, often in series, through the central business districts—the parts of the city most susceptible to the greatly enlarged building scales of the twentieth century. Usually published as wide lithographs, these long views showing mid- and late-nineteenth-century streetscapes appeared from Boston to New Orleans, Manhattan to San Francisco, and, of course, Philadelphia, most often as a form of collective advertising.

  This session will explore both of these graphic realms that vividly recall lost pieces of the city.

* Advertising prints and photographs
* Q & A

11:30 AM–11:45 AM
* Break
* Urban portrait choices
* Q & A

12:30 PM–1:30 PM
* Lunch

1:30 PM–3:00 PM
* Attendee presentations

3:00 PM–3:30 PM
* Wrap-up and surveys

</div>

## Appendix 3: *Urban In-sights Workshop* (June 28–June 30, 2021)—Attendee Survey

### *Urban In-sights Workshop* (June 28–June 30, 2021) Attendee Survey

<div class="noindent">

<span style="color: red">*</span> Required Question(s)

<span style="color: red">*</span> 1. Name:
<br/>\[text field\]

<span style="color: red">*</span> 2. The workshop is now scheduled for Monday, June 28–Wednesday, June 30, 2021. Are you available to attend on those dates?
<br/>\[Yes/No\]

<span style="color: red">*</span> 3. If you responded “No” to the above question, are there work-imposed travel restrictions that will prevent you from attending?
<br/>\[Yes/No\]

<span style="color: red">*</span> 4. Do you anticipate staying in the block of hotel rooms reserved by the Library Company? (We may not be able to use the hotel we had contacted for last summer’s workshop but will look for a nearby facility with rooms costing approximately $150 or less per night.)
<br/>\[Yes/No\]

5\. If you are traveling from out of state, are you willing to follow recommended quarantine guidelines, if they are still in place? (https://www.health.pa.gov/topics/disease/coronavirus/Pages/Travelers.aspx)
<br/>\[Yes/No\]

<span style="color: red">*</span> 6. Are you willing to wear a mask and follow established social distancing guidelines at all times during the workshop?
<br/>\[Yes/No\]

<span style="color: red">*</span> 7. Would you feel comfortable using a ride-share service as part of a field trip?
<br/>\[Yes/No\]

<span style="color: red">*</span> 8. Would you feel comfortable riding on mass transit?
<br/>\[Yes/No\]

</div>

## Appendix 4: *Urban In-sights: A Workshop in American Visual Culture and Literacy from the Eighteenth through the Early Twentieth Century—*Bibliography and Online Resources Guide**

### Visual Culture Workshop: Where to Start with Your Object?

<div class="noindent">

Look at the data on the object. In the lower corners you will often find the names of printers, engravers, lithographers, photographers, publishers, artists, and even dates.

Resource:
<br/>Stijnman, Ad. “Terms in Print Addresses: Abbreviations and Phrases on Printed Images 1500–1900,” 413–18. In *Engraving and Etching, 1400–2000: A History of the Development of Manual Intaglio Printmaking Processes.* London: Archetype Publications, 2012. https://www.delineavit.nl/wp-content/uploads/Terms-in-print-addresses.pdf.

Now look at the data sent by Sarah and Erika. How might it help you determine any or all of the following:

-   Engraver, printer, publisher, artist, and/or date

-   Can the data above help you figure out when the object was made within the maker’s career?

Look at the object itself. Is your vantage point as viewer above, below, or “in” the picture? How does this perspective amplify your interpretation and perceptions of the maker’s intent?

Online sites:

-   WorldCat.org, <https://worldcat.org>: books and manuscripts

-   Library of Congress, <http://catalog.loc.gov/>: broad range of books, images/pictures, including:

    - Historic American Buildings Survey (HABS/HAER), <https://www.loc.gov/pictures/collection/hh/>
    - Electronic Resources, <https://eresources.loc.gov/>
    - National Union Catalog of Manuscript Collections, <https://www.loc.gov/coll/nucmc/>

-   City Directories, e.g., <https://www.philageohistory.org/rdic-images/index2.cfm#9>

-   Making of America, <http://quod.lib.umich.edu/m/moagrp/>: digitized materials 1840 onward

-   Hathi Trust, <https://www.hathitrust.org/>

-   Digital Public Library of America, <https://dp.la/browse-by-topic>

-   ArchiveGrid, <https://researchworks.oclc.org/archivegrid/>: manuscripts

Google Books for nineteenth-century and early-twentieth-century books, such as:

-   Genealogies

-   Nineteenth-century histories, e.g., Scharf and Wescott, *History of Philadelphia*

-   Compendia, e.g., Ritter, *Philadelphia and Her Merchants*; *The Lives of Eminent Philadelphians*; Freedley, *Philadelphia and Its Manufactures*

-   Appleton’s *Cyclopaedia of American Biography*

-   Period books, e.g., Diderot’s *Encyclopedia* (trades)

If you have access to a university’s library (or ask one of us for help):

-   *American National Biography* (aka ANB, formerly DAB): focuses on well-known figures 

-   Archive of Americana (especially for newspapers)

-   Early American Imprints

-   Biography and Genealogy Master Index

-   Historic newspapers

-   Ancestry.com library edition (also check your local library)

Genealogy:

-   Archives.gov (National Archives), <https://www.archives.gov/research/genealogy/start-research>

-   Ancestry.com, <https://www.ancestry.com/>

-   Geni.com, <https://www.geni.com/>

-   GenealogyBank.com, <https://www.genealogybank.com/>

-   Roots Web Review, <http://www.rootsweb.ancestry.com/~rwguide/index.html#GENERAL>

-   Social Security Death Index, <https://www.ancestry.com/search/collections/3693/>

-   Census records, including <http://www.archives.gov/research/census/1790-1840.html>

Philadelphia in particular:

-   comparable sites may be available for other cities; start with the major area—and state—museum and library

-   The Encyclopedia of Greater Philadelphia, <https://philadelphiaencyclopedia.org/>

-   Philadelphia Architects and Buildings, http://www.philadelphiabuildings.org/pab/

-   Free Library of Philadelphia, <https://www.freelibrary.org/>: image (and other) databases

-   Phillyhistory.org, <https://www.phillyhistory.org/PhotoArchive/Home.aspx>: (especially for images)

-   Greater Philadelphia GeoHistory Network, <http://www.philageohistory.org/geohistory/>

</div>

## Appendix 5: *Urban In-sights: A Workshop in American Visual Culture and Literacy from the Eighteenth through the Early Twentieth Century*—Evaluation

<div class="noindent">

Thank you for participating in the *Imperfect History* Visual Culture Workshop! Please help us by responding to any or all of these questions, then emailing your responses to printroom@librarycompany.org.

1.  What was your biggest takeaway from the workshop? What did you get out of it that surprised you?

2.  What aspects of this workshop helped you learn? What impact do you think this workshop will have on your work?

3.  For those in museums/libraries/teaching/other: how might what you gleaned from this workshop affect the public and/or your students?

4.  If we were to offer this or a similar workshop in the future, what changes (besides meeting in person!) could improve your learning?

5.  What visual culture seminars or workshops do you wish the Library Company of Philadelphia offered?

6.  Do you have any other comments you would like to share?

7.  We may be including comments from the workshop in a story about the *Imperfect History* project.
    <br/>May we include yours?
    <br/>Would you be open to speaking with a reporter about the workshop?

![](../../../images/issues/40/piola/figure6.png)
<br/>*Imperfect History* is supported by the Henry Luce Foundation, Walter J. Miller Trust, Center for American Art, Philadelphia Museum of Art, Jay Robert Stiefel, and Terra Foundation for American Art.

</div>

[^1]: The Library Company of Philadelphia was founded in 1731 by Benjamin Franklin and his Junto, a society for mutual improvement, to purchase and steward a collection of books to aid in intellectual discussions.

[^2]: The VCP was established in 2008 with a mission to promote visual literacy and to foster the creative research and interpretation of historical visual materials for the study of the past. For further information about the program, see <https://librarycompany.org/academic-programs/vcp/>.

[^3]: The exhibition and programming were supported by the Henry Luce Foundation, Walter J. Miller Trust, Center for American Art, Philadelphia Museum of Art, Jay Robert Stiefel, and Terra Foundation for American Art. The project website is accessible at <https://librarycompany.org/imperfect-history/>.

[^4]: Maximílano Durón, “‘The Arts Have Long Walked Us Toward Justice’: Sarah Lewis on Her Special Issue of *Aperture*, ‘Vision & Justice,’” *Art News*, June 17, 2016, <https://www.artnews.com/art-news/news/the-arts-have-long-walked-us-toward-justice-sarah-lewis-on-her-special-issue-of-aperture-vision-justice-6511/>.

[^5]: Relevant works and initiatives by these scholars include Jennifer Roberts, *Transporting Visions: The Movement of Images in Early America* (Berkeley: University of California Press, 2014); Shawn Michelle Smith, *At the Edge of Sight: Photography and the Unseen* (Durham, NC: Duke University Press, 2013); Makeda Best, “ReFraming Photographic Histories at the Harvard Art Museum,” Reframing the Collections at the Harvard Art Museum, https://vimeo.com/channels/1719263; and La Tanya Autry and Mike Murawski, “Museums Are Not Neutral: We Are Stronger Together,” *Panorama* 5, no. 2 (Fall 2019), https://editions.lib.umn.edu/panorama/article/public-scholarship/museums-are-not-neutral/. See also the essays in the “Archive” section in *American Art* 31, no. 2 (Summer 2017): 4–31.

[^6]: Jennifer Roberts, “The Power of Patience: Teaching Students the Value of Deceleration and Immersive Attention,” *Harvard Magazine*, November–December 2013, https://www.harvardmagazine.com/2013/11/the-power-of-patience</span>.

[^7]: Eve Tavor Bannet, *Eighteenth-Century Manners of Reading: Print Culture and Popular Instruction in the Anglophone Atlantic World* (Cambridge: Cambridge University Press, 2017), 55.

[^8]: See, for instance, Anne Verplanck, “Making American Art an Engaging General Education Course,” *Art History Pedagogy and Practice* 6, no. 1 (2021), https://academicworks.cuny.edu/ahpp/vol6/iss1/2.

[^9]: Michael Leja, professor of art history and director of the Program in Visual Arts at the University of Pennsylvania, designated this funding as part of his philanthropic role as a member of the board of directors of the Terra Foundation.

[^10]: Sarah Weatherwax, email to successful *Urban In-sights* candidates, April 8, 2020.

[^11]: Sarah Weatherwax, email to Anne Verplanck, October 6, 2020.

[^12]: The specific responses were mixed: six attendees were unwilling to use ride-share; five attendees were unwilling to take mass transit; and six attendees were not comfortable with participating in a group dinner. All attendees agreed to wear masks, remain socially distant from each other, and follow whatever quarantine guidelines were in place if traveling from out of state. One attendee also stated that, as a state employee, she was currently forbidden from traveling outside the state.

[^13]: Between December 2019 and early April 2020, Zoom usage rose from 10 million daily users to 200 million. See Drake Bennett and Nico Grant, “Zoom Goes from Conferencing App to the Pandemic’s Social Network,” *Bloomberg Businessweek*, April 9, 2020, https://www.bloomberg.com/news/features/2020-04-09/zoom-goes-from-conferencing-app-to-the-pandemic-s-social-network.

[^14]: For a discussion of the factors contributing to Zoom fatigue, see Jeremy N. Bailenson, “Non-Verbal Overload: A Theoretical Argument for the Causes of Zoom Fatigue,” *Technology, Mind, and Behavior*, February 23, 2021, https://doi.org/10.1037/tmb0000030.

[^15]: Doug Lederman, “What Worked This Spring? Well-Designed and Delivered Courses,” *Inside Higher Ed*, July 8, 2020, https://www.insidehighered.com/digital-learning/article/2020/07/08/what-kept-students-studying-remotely-satisfied-spring-well.

[^16]: Beth McMurtrie, “The New Rules of Engagement,” *Chronicle of Higher Education*, October 7, 2020, https://www.chronicle.com/article/the-new-rules-of-engagement.

[^17]: Purchasing the pocket microscopes, gathering examples of the historic print processes, and mailing the packets to fifteen attendees cost $1,191.00. Most of that expense ($1,110.00) was covered by eliminating the “Meals, Catering, & Supplies” budget line. The remaining balance was taken by reallocating money from the attendees’ stipends intended to help defray the cost of traveling to Philadelphia along with food and lodging expenses. The original $500.00 stipend for twelve attendees was adjusted to $353.00 for fifteen attendees.

[^18]: For more information on this topic, see David Roberts, “Active Learning Precursors in Multidisciplinary Large Lectures: A Longitudinal Trial on the Effect of Imagery in Higher Education Lectures,” *College Teaching* 66, no. 4 (2018): 199–210.

[^19]: These resources are located at the following web addresses: http://www.graphicsatlas.org/; https://www.philageohistory.org/geohistory/; and https://www.getty.edu/research/tools/vocabularies/ulan/.

[^20]: For more information on this topic, see Anne Verplanck, “The Business of Daguerreotypy: Strategies for a New Medium,” *Enterprise & Society: The International Journal of Business History* 16, no. 4 (July 2015): 1–40.

[^21]: On the Halide Project, see https://www.thehalideproject.org. Lisa Elmaleh’s work can be seen at <http://www.lisaelmaleh.com>.

[^22]: On Philadelphia lithography, see Erika Piola and Jennifer Ambrose, “The First Fifty Years of Commercial Lithography in Philadelphia: An Overview of the Trade, 1828–1878,” 1–48, and Sarah Weatherwax, “Peter S. Duval, Philadelphia’s Leading Lithographer,” 97–117, in *Philadelphia on Stone*, ed. Erika Piola (University Park: Pennsylvania State University Press in association with the Library Company of Philadelphia, 2012).

[^23]: See also Brian P. Luskey and Wendy A. Woloson, *Capitalism by Gaslight: Illuminating the Economy of Nineteenth-Century America* (Philadelphia: University of Pennsylvania Press, 2015).

[^24]: For related scholarship, see Jeffrey A. Cohen, “Corridors of Consumption: Mid-Nineteenth-Century Commercial Space and the Reinvention of Downtown,” in *Visual Merchandising: The Image of Selling*, ed. Louisa Iarocci (London: Ashgate, 2013), 19–36.

[^25]: Andrea Schiavio, Michele Biasutti, and Roberta Antonini Philippe, “Creative Pedagogies in the Time of Pandemic: A Case Study with Conservatory Students,” *Music Education Research* 23, no. 2 (2021): 167–78. For a study related to continuing medical education online, see Ismail Ibrahim Ismail, Ahmed Abdelkarim, and Jasem Y. Al-Hashel, “Physicians’ Attitude towards Webinars and Online Education amid COVID-19 Pandemic: When Less Is More,” *PloS* *ONE* 16, no. 4 (April 16, 2021): e0250241.

[^26]: The American Antiquarian Society’s Center for Historic American Visual Culture summer seminars and the Rare Book School are two venues for close looking at two-dimensional objects; other opportunities include graduate coursework in the fine arts and short courses developed for collectors, dealers, and appraisers.

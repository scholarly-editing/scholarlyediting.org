---
title: Recovering Indic Literatures
subtitle: >-
  An Interview with Dr. Sunil Sharma about the Harvard University Press’s Murty
  Classical Library of India
authors:
  - first: Ateeb
    last: Gul
    affiliations:
      - Boston University
    orcid: 0000-0003-4528-090X
issue: Volume 40
group: 'Voices and Perspectives: Interviews and Conversations'
group_order: 2
doi: 10.55520/Y6FPXVGH
---

## Introductory Note

Dr. Sunil Sharma is Professor of Persianate and Comparative Literature, Associate Chair, and Convener of Comparative Literature (Hindi-Urdu and Persian) at Boston University and until recently a member of the Editorial Board of the Murty Classical Library of India (MCLI), published by Harvard University Press. MCLI publishes facing page translations of classical as well as pre-modern and modern texts in Indic and South Asian languages. Dr. Sharma’s research interests include poetry and court cultures in Sultanate and Mughal India, the history of the book, and travel writing. His latest publication is the edited volume (with Siobhan Lambert-Hurley and Daniel Majchrowicz) titled *Three Centuries of Travel Writing by Muslim Women* (Indiana University Press, 2022), which is an exercise in the recovery, translation, and analysis of Muslim women’s travelogues from a range of languages. Ateeb Gul is a PhD student in Islamic studies at Boston University. He studies and works on Islam in South Asia, especially Sirah literature in the Urdu language and in other Islamicate and regional languages.

## Interview
<div class="noindent">

**Ateeb Gul**: How would you, as a founding member of the editorial committee of the MCLI, explain the scope of the series? What purpose(s) do you think the series serves?

**Dr. Sunil Sharma**: I would begin by quoting from the MCLI website, “To present the greatest literary works of India from the past two millennia to the largest readership in the world is the mission of the Murty Classical Library of India. The series aims to reintroduce these works, a part of world literature’s treasured heritage, to a new generation.” My own role in this ambitious venture was to facilitate the inclusion of important works from the Indo-Persian textual tradition, which are mainly in the form of historical chronicles and poetry, in the series. The idea was to reach both general and specialized audiences at the same time. Translations become outdated and there is a constant need to have new ones, especially of canonical works of literature; including the text in the original script makes the reading experience even more exciting. For South Asian literary texts there are readers who only want to read the translation, but others with a smattering of the language who want to know which particular word or phrase was used in the original, and then others who may be inspired to learn a new script. With Indo-Persian prose or poetry, since so many Persian words are familiar for South Asians, the pleasure of having a translation with the original text is doubled. Of course, some translations are classics in their own right and have not lost their charm, such as Annette Beveridge’s *The History of Humayun* (*Humayunnama*), or Ralph Russell’s translations of Urdu literature in his anthology.

**AG**: What was the inspiration behind your personal involvement in the series?

**SS**: I was invited to join the editorial board by Prof. Sheldon Pollock, who has always taken a serious interest in Indo-Persian literature as an important piece of the mosaic of premodern South Asian culture. Over two decades ago, as a graduate student at the University of Chicago, I was at the first of a series of conferences that led to the publication of the monumental *Literary Cultures in History: Reconstructions from South Asia,* where the need for good translations of premodern texts was realized by many of us. I was involved in the planning stage of the earliest volumes and drew on my network of scholar-translators to see what they could contribute. Of course, a desiderata list is very different from reality, i.e., what can actually be translated given people’s commitments, interest, and abilities!

**AG**: In conversations about the MCLI, several people compare it with another series published by Harvard University Press—the Loeb Classical Library that publishes facing page translations of ancient Greek and Roman texts. Is this a fair comparison? How would you explain the MCLI to people who may only be familiar with the Loeb Classical Library?

**SS**: Yes, it is a fair comparison in some respects, though there are significant differences too. The Loeb series focuses on only two languages and goes up to the early medieval period. The MCLI series has a wider scope and includes ancient, medieval, and early modern works, while covering India’s multiple languages and literary traditions, and this makes it a different kind of project. Hence the need for several sub-editors with specialties in different areas. In some ways, the discontinued Clay Sanskrit Library is more comparable to the Loeb, also given the pocket size of the books! Actually, MCLI is the equivalent of the Loeb Classical Library, Dumbarton Oaks Medieval Library, and The I Tatti Renaissance Library all rolled into one series.

**AG**: While some of the volumes published in the series feature Sanskrit texts from several centuries ago, others focus on Urdu texts from before 1800. Do you see this grand scope of the series as a problem or an opportunity?

**SS**: There has been a great deal of discussion by the editors about the 1800 cutoff date and the meaning of “classical” in this context. It is true that there is a big discrepancy in the antiquity of various textual traditions, with Urdu being one of the youngest major literary languages in South Asia, although its broader literary culture of Hindavi is older. There are many Dakhni (Old Urdu) texts from the sixteenth and seventeenth centuries that are waiting to be translated and introduced to readers, but the number of scholars who work on these is very small. The eighteenth century was a flourishing era for classical Urdu and there was a prolific production of poetic literary works, and it would be wonderful if translations of the poems by Hatim, Sauda, Rangin, etc., were available for students, scholars, and the vast communities of fans of Urdu poetry around the globe, who largely focus on ghazal poetry from Ghalib onwards.[^1] Therefore, there is a lot of opportunity to learn about the classical tradition of Urdu literature and its organic connections with Indo-Persian and other vernaculars.

**AG**: The volumes in the series feature facing-page texts and translations. Are those texts (in the original Indic languages) critically edited? Or do the translations rely on already available and edited texts? Would you agree that the first step in the “recovery” of these texts is to publish their critical editions? And is that an additional direction that the MCLI might take in the future?

**SS**: The texts in the original languages are not critical editions. The translators rely both on already available excellent editions, whether critical or not, or produce their own based on a combination of editions, even consulting manuscripts in some instances. In any case, the idea of a critical edition is not always valid for some literary texts in the classical Indian tradition due to the existence of multiple recensions of the same work in different regions, as well as the strong role that orality played in their transmission. In my view, having an accurate and readable edition without too many variants, as many critical editions would have, is better suited for the readers of these books.

**AG**: Do you invite unsolicited translations, or do you commission them?

**SS**: Any translator can send in a proposal without being solicited, but corresponding with one of the editors to discuss potential ideas for a translation and the format of the proposal is the more efficient way to go about it. Although there is no one model of translation, it would be useful for translators to get to know what’s already been published in the series, and also to get feedback on their draft translations from others before submitting a proposal. The late A.K. Ramanujan, who was my teacher in a translation workshop, gave us sage advice. He said that if you want to translate poetry, then read a lot of poetry in translation or in the target language. I try to do this as I work on my own translations. In recent years I have worked on translations of women’s travel writing in Persian and Urdu, for which I read travelogues authored by men in the same period. Although the styles that men and women employed were quite different, discerning the various registers of English or use of idioms for a nineteenth century traveler was very helpful. One must also attempt to provide adequate support to the reader through annotations and notes without either being too intrusive or holding back, which is a bit more tricky when aiming for both a general and specialist audience.

**AG**: The facing-page format of the series allows scholars as well as lay-readers to benefit from them. And while the feedback from scholars is easier to record and find (for instance, through book reviews published in academic journals), have you received feedback from lay-readers about the volumes you have published so far? What has been the nature of this feedback?

**SS**: Readers of the MCLI books have responded in many ways to the titles we have published. Apart from appreciative comments, there have been some valuable hints and suggestions. What I have heard from some non-specialists is that in addition to providing the texts in the original scripts, perhaps they could also appear in Latin script. One frequently sees this in some translations that have appeared in India, such as Kanda’s various translations of Urdu literature. It is true that not everyone knows or wants to learn all the scripts, but reading the work in the Roman version one can pick out recognizable words and there is a pleasure in that. Many aficionados of Urdu poetry enjoy reading or listening to Persian poetry even when they don’t know Persian precisely because of the familiarity with key words and phrases. But having three scripts is not practical for every text anyway, and could only work for short poems. Another useful suggestion was to have the Punjabi works in Shahmukhi script as well. In the end though, the publication and selling of fine quality books is a complicated business, and not everything is possible.

**AG**: Would you like to share with us your personal favorites, if any, from the available catalog of the MCLI?

**SS**: I would have to say that the eight-volume *History of Akbar* (*Akbarnama*), a sixteenth-century Indo-Persian chronicle by Abu’l Fazl and translated by Wheeler M. Thackston, is my personal favorite, but then I was deeply involved in seeing it through publication! This massive text is a biography, dynastic and social history, romantic epic, all at once, and provides a brilliant view into Mughal courtly life and society. Anyone who enjoys sagas and metaphoric language will derive pleasure from this text. My students have been moved by and enjoyed Charles Hallisey’s translation, *Poems of the First Buddhist Women: A Translation of the Therigatha.* Even if I don’t read every published MCLI volume, it is great fun to dip into one and start reading a few pages, and if I happen to know the script in that work then the pleasure is doubled.

</div>

## Additional Resources

### Series Referenced

Dumbarton Oaks Medieval Library: <https://www.hup.harvard.edu/collection.php?cpk=1320>.
<br/>The I Tatti Renaissance Library: <https://www.hup.harvard.edu/collection.php?cpk=1145>.
<br/>Loeb Classical Library: <https://www.loebclassics.com>.
<br/>Murty Classical Library of India (MCLI): <https://www.murtylibrary.com>.

### Works Referenced

Abu’l-Fazl. *The History of Akbar*. Edited and translated by Wheeler M. Thackston. 8 vols. Murty Classical Library of India. Cambridge, MA: Harvard University Press, 2015–2022. https://www.hup.harvard.edu/results-list.php?collection=2016.
<br/>Hallisey, Charles, trans. *Poems of the First Buddhist Women: A Translation of the Therigatha*.
<br/>Murty Classical Library of India. Cambridge, MA: Harvard University Press, 2021. https://www.hup.harvard.edu/catalog.php?isbn=9780674251359.
<br/>Lambert-Hurley, Siobhan, Daniel Majchrowicz, and Sunil Sharma, eds. *Three Centuries of Travel Writing by Muslim Women*. Bloomington, IN: Indiana University Press, 2022. https://iupress.org/9780253062062/three-centuries-of-travel-writing-by-muslim-women/.
<br/>Pollock, Sheldon, ed. *Literary Cultures in History: Reconstructions from South Asia*. Berkeley and Los Angeles, CA: University of California Press, 2003. https://www.ucpress.edu/book/9780520228214/literary-cultures-in-history.

[^1]: Shaykh Zahuruddin Hatim (1699–1791) was an Urdu poet popularly associated with the Delhi school of poets. Mirza Sauda (1713–1781), a student of Hatim, was a highly celebrated Urdu poet from Delhi. Sa’adat Yar Khan Rangin (1757–1835), another student of Hatim, is most famous for creating a form of Urdu poetry called *rekhti*.

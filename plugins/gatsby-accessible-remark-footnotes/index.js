const visit = require("unist-util-visit");
const toHAST = require(`mdast-util-to-hast`);
const hastToHTML = require(`hast-util-to-html`);

function formatMarkdown(node) {
  const hast = toHAST(node);
  if (hast) {
    return hastToHTML(hast.children, {
      allowDangerousHTML: true
    });
  }
  return (
    node.value ||
    (node.children &&
      node.children
        .map(child =>
          formatMarkdown(child)
        )
        .join("")) ||
    ""
  );
}

module.exports = ({ markdownAST }) => {
  // Manipulate AST
  const footnoteBackrefs = [];
  visit(markdownAST, "footnoteDefinition", backrefNode => {
    footnoteBackrefs.push(backrefNode);
  })

  for (var index = 0; index < footnoteBackrefs.length; index++) {
    const node = footnoteBackrefs[index];
    const innerText = formatMarkdown(node).replace(/^<p>/, "").replace(/<\\p>$/, "");

    const pTag = innerText;

    const anchorTag = `
      <a href="#fnref-${
        node.identifier
      }" aria-label="Back to footnote ${node.identifier}" class="footnote-backref">↩</a>
    `;

    const listItem = `
    <li id="fn-${node.identifier}">
      ${pTag + anchorTag}
    </li>
    `;

    const openingTag = `
      <div class="footnotes"><hr/>
        <ol>
    `;
    const closingOl = `</ol></div>`;

    let html = index === 0 ? openingTag + listItem : listItem;
    html = index === footnoteBackrefs.length - 1 ? html + closingOl : html;

    node.type = "html";
    node.children = undefined;
    node.value = html;

  }

  return markdownAST
}
async function sourceNodes({ actions, createNodeId, createContentDigest }, pluginOptions) {
  const { createNode } = actions
  const {microeditions, basePath} = pluginOptions;

  for (const microedition of microeditions) {
    const metadataURL = new URL(`${microedition}/metadata.json`, basePath).href;
    const metadataRequest = await fetch(metadataURL).catch(err => {console.error(err, `Could not retireve metadata file for micro-edition ${microedition}.`)})
    const metadata = await metadataRequest.text()
    const nodeContent = metadata
    const data = JSON.parse(metadata)
    data.path = microedition
  
    const nodeMeta = {
      id: createNodeId(data.doi), 
      parent: null,
      children: [],
      internal: {
        type: `MicroEditionMeta`,
        content: nodeContent,
        contentDigest: createContentDigest(data)
      }
    }
  
    const node = Object.assign({}, data, nodeMeta)
    createNode(node)
  }
}


exports.sourceNodes = sourceNodes
exports.onPreInit = () => console.log("Loaded se-microeditions-metadata")

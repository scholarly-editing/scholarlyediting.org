# Scholarly Editing website

## Pre-publication and public sites

The site exists in two repositories:

* [`scholarlyediting.org-prepub`](https://gitlab.com/scholarly-editing/scholarlyediting.org-prepub/) contains the code for the pre-publication site, which is only accessible to the editors. All development should be done in this private repository.
* [`scholarlyediting.org`](https://gitlab.com/scholarly-editing/scholarlyediting.org) contains the code for the public site and should only be changed for new journal issues or scheduled updates.

## Cloning this repository on Windows

The website contains some files with long filenames. In order to successfully clone this repository on Windows, you may need to enable long paths on git like this:

```sh
git config --system core.longpaths true
```

## Develop

The website is built using [Gatsby](https://www.gatsbyjs.org/). To develop the site, make sure you have [NodeJS](https://nodejs.org) and a node package manager installed (we recommend [yarn](https://classic.yarnpkg.com/)).

To install:

```bash
yarn install
```

To develop:

```bash
yarn develop
```

and open your browser at `http://localhost:8000`.

## Deploying

**Before publication, make sure to increase the version in package.json.** Changes in content should be considered a patch. Content restructuring (e.g. new pages, alterations to navigation, etc.) and technical improvements (e.g. a new plugin) should be consider a minor change. Changes in technical architecture (e.g. upgrading to a new Gatsby version, or switching to a different system) should be considered a major change.

**To update the public site** with the latest changes from pre-publication, merge changes from [`scholarlyediting.org-prepub`](https://gitlab.com/scholarly-editing/scholarlyediting.org-prepub/). To do so, start by adding `scholarlyediting.org-prepub` as an `upstream` repository; you only need to do this once.

```bash
git remote add upstream https://gitlab.com/scholarly-editing/scholarlyediting.org-prepub.git
```

Then pull from `upstream` every time you're ready to update the site.
```bash
git pull upstream prepub
```

### Micro-Editions

Micro-editions are coded in their own repositories. Built assets are served from the Scholarly Editing static server (currently on Reclaim Hosting). To include a micro-edition in the main Scholarly Editing website, add its slug to the options of the plugin `se-microeditions-metadata` in `gatsby-config.js`.

### Publishing on the Scholarly Editing static server

Run `yarn build` and simply `rsync` to the Reclaim Hosting server. Both for prepub and live site.

## 2012-2017 Archive

`static` contains the HTML for the old scholarlyediting.org site. Other assets for the archive are hosted on [reclaimhosting.org](http://reclaimhosting.org).

<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:teiE="http://www.tei-c.org/ns/Examples">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="x-ua-compatible" content="IE=Edge">
      <link href="http://fonts.googleapis.com/css?family=Gentium+Basic:400,700,400italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
      <title>Scholarly Editing: The Annual of the Association for Documentary
         Editing
      </title>
      <link href="https://archive.scholarlyediting.org/se-archive/template_css/style.css" rel="stylesheet" type="text/css">
      <link href="https://archive.scholarlyediting.org/se-archive/template_js/jquery-lightbox-0.5/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css">
      <link rel="icon" href="https://archive.scholarlyediting.org/se-archive/2016/reviews/favicon.ico" type="image/x-icon"><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">   </script><script src="https://archive.scholarlyediting.org/se-archive/template_js/jquery-lightbox-0.5/js/jquery.lightbox-0.5.min.js">   </script><script src="https://archive.scholarlyediting.org/se-archive/template_js/javascript.js">   </script><link href="https://archive.scholarlyediting.org/se-archive/template_css/style.css" rel="stylesheet" type="text/css"></head>
   <body><div style="background-color: #52634c; padding: 1rem; color: #e4dcc0; text-align: center">You are viewing the archived content of Scholarly Editing, Volumes 33 – 38 issued between 2012 and 2017.  <a href="https://scholarlyediting.org" style="color: #e4dcc0; text-decoration: underline;">Go to the new site</a>.</div>
      <div id="content">
         <div class="nav">
            <ul>
               <li id="nav1"><a href="../../se.index.issues.html">Issues</a></li>
               <li id="nav2"><a href="../../se.index.editions.html">Editions</a></li>
               <li id="nav3"><a href="../../se.index.essays.html">Essays</a></li>
               <li id="nav4"><a href="../../se.index.reviews.html">Reviews</a></li>
               <li id="about"><a href="https://scholarlyediting.org/about/">About</a></li>
            </ul>
         </div>
         <div class="masthead">
            <h1><a href="../../index.html"><span>Scholarly Editing</span></a></h1>
            <h4>The Annual of the Association for Documentary Editing</h4>
            <h5>2016, Volume 37</h5>
         </div>
         <div class="essayHeader">
            <h1 class="essaysTitle">Reviews</h1>
         </div><span class="essayNav">
            <ul>
               <li id="essayNav1"><a href="https://archive.scholarlyediting.org/se-archive/2016/pdf/review.ovid.pdf"><img src="https://archive.scholarlyediting.org/se-archive/images/pdf.png" alt="PDF icon">&nbsp;PDF</a></li>
            </ul></span><div class="essay">
            
            
            
            
            <h3><em>Ovid’s Revisions: The Editor as Author.</em>
               Francesca K. A. Martelli. Cambridge: Cambridge University Press, 2014. 260 pp. ISBN:
               9781107037717. $95.
            </h3>
            
            <p>The relationship between editor and author is often fraught with tension, raising
               questions of who controls the published text and what authorship actually means. In
               <em>Ovid’s Revisions: The Editor as Author</em>, Francesca K. A.
               Martelli, an assistant professor of classics at UCLA, engages issues of editorial
               practice and authorial identity by considering a body of work by Publius Ovidius
               Naso (43 BCE–ca. 18 CE), the Roman poet most of us know today as Ovid. As Martelli
               demonstrates, a striking feature of Ovid’s work is his career-long practice of
               revising his own work and drawing readers’ attention to those revisions. Through
               looking across five of Ovid’s major works, Martelli makes visible a wide range of
               editorial practice still quite relevant today, as well as the import of that work
               for authors and readers alike.
            </p>
            
            <p>Martelli opens her volume with a substantive discussion of revision as transformative
               for texts and authors, recognizing trends in authorship studies and related fields
               that stress such concepts as distributed, or collaborative, authorship and the
               socially constructed aspects of the author. Yet Ovid’s work, she notes, offers a
               window into the complexity of even a so-called single author, revealing ways in
               which authorial revision serves to multiply the author’s identity, making the
               singular into a plural by having the author fill multiple roles, certainly, and by
               revising who the author “is” over the course of a career, due to maturation and
               construction.
            </p>
            
            <p>Challenging assumptions, too, that revision “always aims at improvement” (4),
               Martelli claims that Ovid’s work shows a sense of value in revision for revision’s
               sake. This view of revision as play, of multiple drafts as equally valid, unsettles
               an editorial approach based on determining either an author’s original or final
               intention. The text does not originate in one singular moment, in other words, but
               across time. In this sense, revision is not a corrective but an extension. Such a
               view promotes efforts to produce collected parallel works or edited volumes that
               preserve, rather than collapse or redact, differences among drafts.
            </p>
            
            <p>Martelli notes Ovid’s transparency about the revisions of his own works through, for
               example, prefaces that contextualize not only the individual works they frame but
               also their place in Ovid’s larger body of work, and through making available
               overlapping written material. This project allows Martelli to “plot some of the
               narratives of revision that run through” his work (29), looking at diverse editorial
               approaches—“the <em>Amores</em> foreshortens, the <em>Ars Amatoria</em> extends, the <em>Fasti</em> supplements <em>and</em> foreshortens, the <em>Tristia</em>
               re-routes, the <em>Ex Ponto</em> collates” (33)—and the effects of
               such approaches on texts and readers.
            </p>
            
            <p>Martelli devotes each of chapters 2–6 to Ovid’s revisions in connection with one of
               the works listed above. Chapter 2 observes that the <em>Amores</em>
               (the work scholars usually date as the chronological first of Ovid’s enduring works)
               opens with a preface that describes the work as a revised (and shortened) version.
               Today’s readers have no access to a previous version—if one truly existed. Martelli,
               though, considers that even the <em>idea</em> that the work is a
               revision influences ways of reading the text, including building admiration for the
               author (who can both create a longer work and do the difficult work of editing the
               work down). In addition, seeing the work as an abridged second edition promotes
               reader curiosity about what exists in the extended version. Ovid’s editorial choices
               and Martelli’s observations reveal rhetorical power in prefatory and other ancillary
               materials, which shape readers’ expectations and interpretations of the central
               text.
            </p>
            
            <p>Ancillary content, though, is not the only editorial material that wields
               hermeneutical influence, as evidenced in Martelli’s third chapter. There, she turns
               to considering editorial additions to the main text (as opposed to reductions). The
               <em>Ars Amatoria</em> is, she notes, “a text that appears to have
               some difficulty reaching or finding its ending” (68). The “original” poem is a
               two-book work, but Ovid added two addendums—the text known as <em>Ars
                  3</em> and the <em>Remedia Amoris</em>. Martelli argues that these
               “mobile” endings build narrative tension in this work devoted to sexual desire (the
               text is essentially instructions on seduction and resisting seduction) by weaving
               between desire and the death of desire.
            </p>
            
            <p>Chapter 3 discusses additions that extend a work’s ending, and chapter 4 demonstrates
               that texts that seem unfinished may actually be self-contained. Chapter 4 focuses on
               <em>Fasti</em>, a six-book poem built around the recently modified
               Roman calendar, yet glaring for its treatment of only the first six months. Common
               interpretation sees the truncated poem as unfinished, and this incompletion is
               usually attributed to Emperor Augustus’s banishing Ovid from Rome to Tomis in 8 CE
               for causes unknown. Martelli counters this interpretation, observing that since the
               poem shows indications of revision throughout with no attempts to produce additional
               content, we have ample reason to view the poem as complete. In doing so, we can
               recognize a significant function of the poem—Ovid’s challenge to the Roman calendar
               itself and the power structures that determine even public experiences of time.
               Although editors face pressures to produce works that appear complete according to
               genre expectations, those same editors may take note of the effects texts can create
               in readers by breaking those expectations. The <em>Fasti</em> example
               can also serve to reinforce the opposite claim, given widely accepted
               interpretations that the poem <em>is</em> incomplete. To expand
               possibilities, we can consider the <em>Fasti</em> in connection with
               Martelli’s earlier examples—that is, allowing authors and editors to experiment with
               meaningful form, but providing at least some hints toward interpretation in
               ancillary materials such as prefaces or addenda.
            </p>
            
            <p>Chapter 5 analyzes <em>Tristia</em>, which, Martelli argues, “brings to
               the fore Ovid’s role as editor, not just of this particular text, but also of his
               larger textual output and identity” (146). Each of the five books comprising the
               poem—written during Ovid’s exile—is presented as a stand-alone work, and the
               differing revisions lead to variations in the authorial vision among the books. Of
               particular interest to those who may be most familiar with Ovid in connection to his
               most famous work, the <em>Tristia</em> overlaps with the epilogue of
               the <em>Metamorphoses</em>, rewriting that epilogue multiple times,
               yet standing independent. This practice, Martelli writes, “breaks down the
               distinctions between” Ovid’s works, “as the revision of one work is made to overlap
               with the composition of another, and the different authorial identities that these
               two works produce are made to coexist within the ‘same’ textual space” (32).
            </p>
            
            <p>For editors working with bodies of independent works that overlap in
               content—repeating ideas or even passages—the intersection between Ovid’s <em>Tristia</em> and <em>Metamorphoses</em> provides an
               example of how revising and modifying one work inescapably influences the meaning of
               others, challenging editors to read any revisions they make in a more varied context
               than a single volume or collection.
            </p>
            
            <p>The <em>Tristia</em> also complicates Ovidian authorship while raising
               questions about authorial self-presentation and editorial transparency. This poem
               functions in many ways as a monument or monuments to prominent names (from Ennius,
               Lucretius, and Horace, to Homer, et al.). At the same time, it is a rare exception
               in Ovid’s own body of work because he leaves his name off the title pages, while
               instead incorporating himself into the text (making references, for example, to the
               image of his face on a ring and to an epitaph for his tombstone). “In exile,”
               Martelli writes, “Ovid empties his authorial name of its performative force, in the
               knowledge that that name now has a new referent” (165). As we near the end of
               Martelli’s volume, then, we see significant developments in the function of Ovid’s
               authorship. And although the point of Martelli’s project is considering Ovid’s
               self-editing, more traditional editors also face decisions about how much to expose
               or conceal their participation in the “final” text. Ovid’s example of how and why he
               alters his approach to transparency in the <em>Tristia</em> can serve
               as an entry point for other editors in selecting their own approaches to this
               persistent challenge.
            </p>
            
            <p>Martelli next examines the <em>Epistulae ex Ponto</em>, a collection of
               letters from Ovid to a series of readers. The evident editorializing and revising of
               this collection—despite Ovid’s claims to the contrary—show him crafting the audience
               with whom he wishes to participate, a strategy that all editors must consider when
               selecting textual interventions. He chooses, for instance, to address private
               individuals in domestic realms, rather than reaching out to a public audience, even
               while knowing that this private/public distinction was a fiction, since he edited
               the collection for publication. Ovid was attempting to secure immortality not
               through his literary fame but through the “continuing support of his friends in
               Rome” (224), seeking to extend his identity through the world outside the text.
               Whether editing standard volumes (i.e., intervening directly in texts prior to their
               publication as ostensibly cohesive single-authored works) or producing editions that
               note alterations and/or provide commentary, traditional textual editors also make
               their decisions based on audience—and not necessarily obvious audiences, but
               audiences they select through their own approaches.
            </p>
            
            <p>In all these chapters, Martelli grounds her arguments thoroughly in the texts
               themselves, pursuing close readings of her texts with attention to the features and
               functions of specific lines. In her epilogue, she returns to her broader inquiry
               into authorship, citing authorial revision as both “reinforc[ing] the identity of
               the author” and “mark[ing the] author’s disappearance” (230). She also considers the
               editorial moves Ovid makes in the above five works in the context of his other works
               (the <em>Metamorphoses</em>, <em>Heroides</em>, and <em>Ibis</em>).
            </p>
            
            <p>Taken as a whole, Martelli’s work in this volume marks a valuable contribution to
               classical studies while simultaneously furthering urgent conversations in the realms
               of editorial theory, studies in authorship and publishing, and textual studies. Work
               in these latter fields that draw on this book might return to consider differences
               in editorial practice when revising one’s own work versus the work of others; the
               shifting identity of any given author; the possibilities and limitations of
               representing multiple versions of a text in edited editions; how making editorial
               revision transparent to readers may influence reading interpretation and experience;
               how authors shape their own audiences; and how authors endure or disappear. Such
               queries, tied to Martelli’s thoughtful scholarship in these pages, hold potential
               for lively and transformative discussion in the decades to come.
            </p>
            
            <div class="signature"><span class="name">Heidi Nobles</span><span class="affiliation">Texas Christian University</span></div>
            
            
            
         </div>
         <div class="footer"><span class="creativecommons"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"></a><br>This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative
                  Commons Attribution-NonCommercial-ShareAlike 3.0 Unported
                  License</a>.</span><span class="adelogo"><a href="http://www.documentaryediting.org"><img src="https://archive.scholarlyediting.org/se-archive/template_images/adelogo.png" alt="Logo of the Association for Documentary Editing"></a></span><span class="cdrh">Sponsored by the <a href="http://cdrh.unl.edu">Center for
                  Digital Research in the Humanities at the University of
                  Nebraska-Lincoln</a>.</span><span class="issn">ISSN 2167-1257</span></div>
      </div>
   <script>
  
  function gaOptout(){document.cookie=disableStr+'=true; expires=Thu, 31 Dec 2099 23:59:59 UTC;path=/',window[disableStr]=!0}var gaProperty='UA-174640285-1',disableStr='ga-disable-'+gaProperty;document.cookie.indexOf(disableStr+'=true')>-1&&(window[disableStr]=!0);
  if(true) {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  }
  if (typeof ga === "function") {
    ga('create', 'UA-174640285-1', 'auto', {});
      ga('set', 'anonymizeIp', true);
      
      
      
      
      }</script></body>
</html>
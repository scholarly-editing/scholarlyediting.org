<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:teiE="http://www.tei-c.org/ns/Examples">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="x-ua-compatible" content="IE=Edge">
      <link href="http://fonts.googleapis.com/css?family=Gentium+Basic:400,700,400italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
      <title>Scholarly Editing: The Annual of the Association for Documentary
         Editing
      </title>
      <link href="https://archive.scholarlyediting.org/se-archive/template_css/style.css" rel="stylesheet" type="text/css">
      <link href="https://archive.scholarlyediting.org/se-archive/template_js/jquery-lightbox-0.5/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css">
      <link rel="icon" href="https://archive.scholarlyediting.org/se-archive/2015/reviews/favicon.ico" type="image/x-icon"><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">   </script><script src="https://archive.scholarlyediting.org/se-archive/template_js/jquery-lightbox-0.5/js/jquery.lightbox-0.5.min.js">   </script><script src="https://archive.scholarlyediting.org/se-archive/template_js/javascript.js">   </script><link href="https://archive.scholarlyediting.org/se-archive/template_css/style.css" rel="stylesheet" type="text/css"></head>
   <body><div style="background-color: #52634c; padding: 1rem; color: #e4dcc0; text-align: center">You are viewing the archived content of Scholarly Editing, Volumes 33 – 38 issued between 2012 and 2017.  <a href="https://scholarlyediting.org" style="color: #e4dcc0; text-decoration: underline;">Go to the new site</a>.</div>
      <div id="content">
         <div class="nav">
            <ul>
               <li id="nav1"><a href="../../se.index.issues.html">Issues</a></li>
               <li id="nav2"><a href="../../se.index.editions.html">Editions</a></li>
               <li id="nav3"><a href="../../se.index.essays.html">Essays</a></li>
               <li id="nav4"><a href="../../se.index.reviews.html">Reviews</a></li>
               <li id="about"><a href="https://scholarlyediting.org/about/">About</a></li>
            </ul>
         </div>
         <div class="masthead">
            <h1><a href="../../index.html"><span>Scholarly Editing</span></a></h1>
            <h4>The Annual of the Association for Documentary Editing</h4>
            <h5>2015, Volume 36</h5>
         </div>
         <div class="essayHeader">
            <h1 class="essaysTitle">Reviews</h1>
         </div><span class="essayNav">
            <ul>
               <li id="essayNav1"><a href="https://archive.scholarlyediting.org/se-archive/2015/pdf/review.twain.pdf"><img src="https://archive.scholarlyediting.org/se-archive/images/pdf.png" alt="PDF icon">&nbsp;PDF</a></li>
            </ul></span><div class="essay">
            
            
            
            
            <h3><em>Autobiography of Mark Twain, Volume 1: The Complete
                  and Authoritative Edition</em>. Edited by Harriet Elinor Smith et al. Berkeley:
               University of California Press, 2010. 736 pp. $45. ISBN 978-00520-26719-0.
            </h3>
            
            <h3><em>Autobiography of Mark Twain, Volume 2: The Complete
                  and Authoritative Edition</em>. Edited by Benjamin Griffin, Harriet Elinor Smith
               et al. Berkeley: University of California Press, 2013. 776 pp. $45. ISBN
               978-00520-27275-1.
            </h3>
            
            <p>With two of three projected volumes now in print, the University of California Press
               edition of Mark Twain's autobiography has been a publishing phenomenon with few
               parallels in the world of scholarly editing. Although the first volume contains
               little material not already published, it shot to <em>New York
                  Times</em> bestseller status in 2010, remained there half a year, and ended up
               selling more than a half million copies. That is an astounding figure for any volume
               of edited documents and is even more impressive when one considers that an online
               version of its text has been available for free since the book came out.
            </p>
            
            <p>The impressive sales of the first volume doubtless had much to do with public
               fascination with Mark Twain, arguably the most revered and widely read of
               nineteenth-century American writers. Many readers apparently expected the volume to
               be filled with shocking material that had been long suppressed. That was a mistaken
               assumption that had grown out of the story that Mark Twain did not want his
               autobiography published until a century after his death. The story was basically
               true, but his wish was not honored.
            </p>
            
            <p>Many things connected with Samuel L. Clemens—better known to the world as Mark
               Twain—are complicated and full of surprises, and that is certainly true of his
               autobiography. To begin with, what he called his "autobiography" is not what most
               people mean by that term—namely an introspective history of one's life related in
               chronological order. Clemens consciously rejected that approach in favor of a
               discursive collection of reminiscences of events and acquaintances from his past,
               arranged in order of the moments when he recorded them. Moreover, he wanted his
               autobiography to "become a model for all future autobiographies" and be admired
               "because of its form and method . . . whereby the past and the present are
               constantly brought face to face, resulting in contrasts which newly fire up the
               interest all along, like contact of flint with steel." Spontaneity was one of his
               goals, and he predicted he would never run of out subject matter: 
               <blockquote>I
                  shall talk about the matter which for the moment interests me, and cast it aside
                  and talk about something else the moment its interest for me is exhausted. It is
                  a system which follows no charted course and is not going to follow any such
                  course. It is a system which is a complete and purposed jumble—a course which
                  begins nowhere, follows no specified route, and can never reach an end while I
                  am alive.
               </blockquote>Not surprisingly, the resulting "jumble" has not been universally
               admired. When a large portion of it was published in book form in 1924, a critic for
               the <em>Dallas Morning News</em> said it was 
               <blockquote>surely
                  the most strangely put together of all memoirs. It is a work of shreds and
                  patches, a hodge-podge of miscellaneous papers and dictations written in various
                  parts of the world . . . and in as many different manners as could well be found
                  in a book not an anthology. Still worse, there is no sort of chronology; each
                  new chapter begins with whatever subject happens to come into the muser's
                  mind.
               </blockquote>What concerns us here, however, is not the merits of Clemens's method,
               but rather its relevance to the challenges faced by his editors, of whom there have
               been many over the years (including the present reviewer).
            </p>
            
            <p>Although Clemens began consciously experimenting with autobiographical writing when
               he was in his early forties, he did not get down to serious composition until three
               decades later, by which time he had settled on dictation to stenographers as his
               preferred method of composition. During a two-year period beginning in early 1906,
               he conducted a series of about 250 dictation sessions that produced the 500,000-word
               manuscript he regarded as his formal autobiography. He had written other
               autobiographical pieces earlier but did not regard them as parts of that
               autobiography. All published versions of the autobiography go back to the pages
               transcribed from the dictations, but several versions have also incorporated parts
               of the other material.
            </p>
            
            <p>One of Clemens’s goals was to be completely candid in the descriptions and opinions
               he expressed. To help make that possible, he wished to delay publication of the
               autobiography until a century after his death, so he would be, in a sense, "speaking
               from the grave." He eventually concluded that complete honesty was still impossible
               but nevertheless remained determined not to have his autobiography published in book
               form until long after he died.
            </p>
            
            <p>Clemens was serious about delaying publication of his autobiography, but he was also
               a pragmatist who recognized the work had a pecuniary value. He needed money, so he
               assented to having selected passages published in magazine form even before he had
               completed his dictations. In September 1906 the fortnightly <em>North
                  American Review</em>, edited by George Harvey, began serializing twenty-five
               installments under the title "Chapters from My Autobiography"; the installments were
               later syndicated in newspapers. Selected by Harvey with Clemens's help, those
               articles are the only parts of Clemens's autobiography published under his direct
               supervision.
            </p>
            
            <p>Part of the impetus behind Clemens's undertaking to dictate his autobiography in 1906
               was the entry into his life of Albert Bigelow Paine, an author and magazine editor
               who asked to write his biography. Impressed by Paine's recent biography of
               cartoonist Thomas Nast, Clemens not only assented to Paine's request but also
               invited him into his home so he would have ready access to his papers. At Paine's
               suggestion, he hired a stenographer to transcribe his dictations and welcomed Paine
               to sit in on the sessions, in which Paine helped provide focus. After Clemens died
               in 1910, Paine became his literary executor. As sole editor of what became known as
               the Mark Twain Papers, he exercised almost complete control over the voluminous
               manuscripts, including the dictations. Through the remaining quarter century of his
               own life, Paine would go on to publish a monumental biography of Clemens, several
               shorter biographies, and edited collections of Clemens's letters, speeches,
               notebooks, and previously unpublished and uncollected literary works.
            </p>
            
            <p>Perhaps the most notable of Paine's later editions was <em>Mark Twain's
                  Autobiography</em>, a 195,000-word, two-volume work published by Harper in 1924.
               It incorporated about 40 percent of the dictations, including many portions that had
               already appeared in the <em>North American Review</em>. It also
               included scraps of Clemens's earlier attempts at autobiography. Paine more or less
               adhered to Clemens's wish to arrange passages in the order in which they had been
               dictated, but he also included material Clemens had not considered part of his
               autobiography proper. Moreover, as was typical of Mark Twain books Paine edited, he
               made little effort to explain how he selected the material, what was left out, or
               what editorial principles he applied to it. Among the omitted portions were
               diatribes that might have offended Clemens's targets or their descendants.
            </p>
            
            <p>Paine's introduction assured readers "that positive mistakes of date and occurrence
               have been corrected" but said nothing about the numerous substantive changes he made
               in the texts. So far as anyone knew, <em>Mark Twain's
                  Autobiography</em> was definitive because Paine allowed no one else to see
               Clemens's original manuscripts.
            </p>
            
            <p>After Paine died in 1937, Bernard DeVoto succeeded him as editor of the Mark Twain
               Papers. One of DeVoto's first priorities was to publish parts of the autobiography
               Paine had omitted. Selecting only what he regarded as the most interesting material,
               he published the 110,000-word volume <em>Mark Twain in Eruption:
                  Hitherto Unpublished Pages About Men and Events</em> in 1940. This book included
               some material that had appeared in the <em>North American Review</em>
               but nothing from <em>Mark Twain's Autobiography</em>. In contrast to
               Paine, DeVoto ignored Clemens's preferred arrangement, instead organizing passages
               under topical headings such as "Theodore Roosevelt," "Andrew Carnegie," "The
               Plutocracy," and "Various Literary People." As these titles might suggest, the book
               included many of the vitriolic passages about people Paine had left out.
            </p>
            
            <p>By DeVoto's estimate, his and Paine's editions used three-quarters of Clemens's
               dictations. In 1959 a third editor, Charles Neider, published <em>The
                  Autobiography of Mark Twain</em>, a completely reorganized volume that
               incorporated material from all earlier editions, adding about 30,000 words not
               previously used. Neider arranged the material in the traditional form of a
               cradle-to-grave narrative. This was the antithesis of what Clemens had wanted, but
               it pleased most readers and has remained in print almost continuously for nearly six
               decades. In 1999 the board of the Modern Library ranked it as one of the greatest
               nonfiction books of the twentieth century.
            </p>
            
            <p>Additional autobiography editions that appeared in later years were mostly reprints
               of the old <em>North American Review</em> articles. The most notable
               of these was edited by Michael Kiskis, who titled his edition <em>Mark
                  Twain's Own Autobiography</em> (1990 and 2010) to differentiate it from earlier
               editions that Clemens did not oversee. The 29-volume Oxford Mark Twain edition of
               1996 included a volume titled <em>Chapters from My Autobiography</em>,
               which contains facsimile reproductions of the original <em>Review</em>
               pages.
            </p>
            
            <p>After all these autobiography editions, the question now to be asked is what is
               different about the University of California Press's <em>Autobiography
                  of Mark Twain</em>. The obvious first answer, of course, is that the volumes are
               being prepared with the same high level of scholarly exactitude that its editors at
               the University of California's Mark Twain Project apply to all the volumes they
               edit. None of the earlier editions adhered to standards remotely comparable. Indeed,
               DeVoto even boasted about having modernized Clemens's punctuation by deleting
               thousands of commas and dashes.
            </p>
            
            <p>Given the large portions of the autobiography that were already published, the first
               two volumes of the California edition do not contain a great deal not previously
               seen. What makes their text more important is our knowing that we are reading
               Clemens's precise words, not a timid or opinionated editor's approximation of them.
               Most of the edition's corrections are technical, but occasional zingers jump off the
               page. An example is a scathing remark about James W. Paige, the inventor of a
               typesetting machine in which Clemens invested and lost a large fortune. Paine's <em>Mark Twain's Autobiography</em> quotes Clemens as saying, "Paige
               and I always meet on effusively affectionate terms, and yet he knows perfectly well
               that if I had him in a steel trap I would shut out all human succor and watch that
               trap till he died." The new edition's corrected text reads a little differently: "he
               knows perfectly well that if I had his nuts in a steel trap I would shut out all
               human succor and watch that trap till he died."
            </p>
            
            <p>In sharp contrast to the earlier autobiography editors, those of the Mark Twain
               Project follow several bedrock principles. The first of these is to publish
               Clemens's works as close to what he intended as possible. The trick, of course, is
               knowing exactly what he intended. This is especially difficult to know in the case
               of the autobiography, as he left few explicit instructions for it. One clear
               instruction, however, was to publish the autobiography a century after his death,
               which occurred in 1910. The Project complied with that wish. That obvious issue
               aside, Clemens's intentions always include such matters as leaving his punctuation
               and word choices as he wrote them. They also include publishing his works in their
               entirety and not omitting anything he himself did not mark for deletion. This
               principle alone sets the California edition far apart from its predecessors.
            </p>
            
            <p>Another bedrock principle of the Mark Twain Project is achieving complete
               transparency. Through various methods, its editors make it possible for readers to
               understand virtually every editing decision made in the texts. This strategy ties
               into the Project's overriding goal to ensure that future scholars will never need to
               edit the same texts again. Whenever anything is left out of a text or an alteration
               is not explained, readers cannot be sure how the printed text differs from the
               original. Readers of the Project's autobiography need not wonder about such
               questions, as nothing is ever left out, and Clemens's writing is never altered
               without clear and full explanations.
            </p>
            
            <p>Though it may not quite rank as a bedrock principle, another of the Project's aims is
               to provide all its complex editorial apparatus in ways that do not interfere with
               reading the texts. That requires leaving pages uncluttered and putting annotations
               and editorial explanations elsewhere—either in appendixes or on the Project's
               website.
            </p>
            
            <p>A unique challenge to the editors of the autobiography has been determining the
               authority of multiple typescript versions of the dictations. Four typescript copies
               of almost every session have survived, so it has been necessary to determine the
               sequence in which each was typed to know which of them reflect Clemens's final
               intentions. The problem is complicated by handwritten notes made on the pages by
               stenographers, Clemens himself, and later generations of editors. In her excellent
               introduction to the first volume, Harriet Elinor Smith discusses how the Project
               editors learned to decipher the typescripts, supporting the discussion with numerous
               facsimiles of sample pages. The Project's website at <a href="http://www.marktwainproject.org/">www.marktwainproject.org</a>
               contains detailed explanations of every sequence of typescripts for the
               autobiography's first two published volumes.
            </p>
            
            <p>Despite placing most of the technical apparatus for the autobiography online, the
               printed volumes themselves are heavily loaded with explanatory material. In volume
               1, for example, only 468 of the book's 736 pages contain Clemens's own writing.
               There are 58 pages of introduction and about 180 pages of explanatory notes. The
               rest of the pages contain technical notes, bibliography, and a detailed index. The
               proportions in the second volume are about the same, except that in the absence of
               an introduction, there are more pages of Clemens's writing. Also, because the first
               volume contains about 140 pages of Clemens's predictation writings, the proportion
               of dictated texts in the second volume is much higher.
            </p>
            
            <p>Marvels of scholarly thoroughness, the endnotes concisely explain almost every
               subject in the texts that would benefit from additional information. Of particular
               interest are identifications of persons Clemens named or merely alluded to. Paine
               once described Clemens’s dictated reminiscences as bearing “only an atmospheric
               relation to history.” It is thus not surprising that many annotations address
               contradictions between what Clemens said and what is actually known about his
               life.
            </p>
            
            <p>When the third volume of <em>Autobiography of Mark Twain</em> is
               published next year, all earlier editions will cease to have any value. Some
               readers, however, will find the new edition's three bulky volumes overly cumbersome.
               To meet their needs, the University of California Press is also publishing
               scaled-down paperback "reader's editions," shorn of much of the scholarly editorial
               apparatus and weighing less than a third of the hardback editions. This move will
               doubtless ensure that the Mark Twain Project’s unparalleled edition will reach the
               widest possible audience.
            </p>
            
            <div class="signature"><span class="name">R. Kent Rasmussen</span></div>
            
            
            
         </div>
         <div class="footer"><span class="creativecommons"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"></a><br>This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative
                  Commons Attribution-NonCommercial-ShareAlike 3.0 Unported
                  License</a>.</span><span class="adelogo"><a href="http://www.documentaryediting.org"><img src="https://archive.scholarlyediting.org/se-archive/template_images/adelogo.png" alt="Logo of the Association for Documentary Editing"></a></span><span class="cdrh">Sponsored by the <a href="http://cdrh.unl.edu">Center for
                  Digital Research in the Humanities at the University of
                  Nebraska-Lincoln</a>.</span><span class="issn">ISSN 2167-1257</span></div>
      </div>
   <script>
  
  function gaOptout(){document.cookie=disableStr+'=true; expires=Thu, 31 Dec 2099 23:59:59 UTC;path=/',window[disableStr]=!0}var gaProperty='UA-174640285-1',disableStr='ga-disable-'+gaProperty;document.cookie.indexOf(disableStr+'=true')>-1&&(window[disableStr]=!0);
  if(true) {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  }
  if (typeof ga === "function") {
    ga('create', 'UA-174640285-1', 'auto', {});
      ga('set', 'anonymizeIp', true);
      
      
      
      
      }</script></body>
</html>
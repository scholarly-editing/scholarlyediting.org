<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns:tei="http://www.tei-c.org/ns/1.0" xmlns:teiE="http://www.tei-c.org/ns/Examples">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="x-ua-compatible" content="IE=Edge">
      <link href="http://fonts.googleapis.com/css?family=Gentium+Basic:400,700,400italic,700italic&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
      <title>Scholarly Editing: The Annual of the Association for Documentary
         Editing
      </title>
      <link href="https://archive.scholarlyediting.org/se-archive/template_css/style.css" rel="stylesheet" type="text/css">
      <link href="https://archive.scholarlyediting.org/se-archive/template_js/jquery-lightbox-0.5/css/jquery.lightbox-0.5.css" rel="stylesheet" type="text/css">
      <link rel="icon" href="https://archive.scholarlyediting.org/se-archive/2015/essays/favicon.ico" type="image/x-icon"><script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js">   </script><script src="https://archive.scholarlyediting.org/se-archive/template_js/jquery-lightbox-0.5/js/jquery.lightbox-0.5.min.js">   </script><script src="https://archive.scholarlyediting.org/se-archive/template_js/javascript.js">   </script><link href="https://archive.scholarlyediting.org/se-archive/template_css/style.css" rel="stylesheet" type="text/css"></head>
   <body><div style="background-color: #52634c; padding: 1rem; color: #e4dcc0; text-align: center">You are viewing the archived content of Scholarly Editing, Volumes 33 – 38 issued between 2012 and 2017.  <a href="https://scholarlyediting.org" style="color: #e4dcc0; text-decoration: underline;">Go to the new site</a>.</div>
      <div id="content">
         <div class="nav">
            <ul>
               <li id="nav1"><a href="../../se.index.issues.html">Issues</a></li>
               <li id="nav2"><a href="../../se.index.editions.html">Editions</a></li>
               <li id="nav3"><a href="../../se.index.essays.html">Essays</a></li>
               <li id="nav4"><a href="../../se.index.reviews.html">Reviews</a></li>
               <li id="about"><a href="https://scholarlyediting.org/about/">About</a></li>
            </ul>
         </div>
         <div class="masthead">
            <h1><a href="../../index.html"><span>Scholarly Editing</span></a></h1>
            <h4>The Annual of the Association for Documentary Editing</h4>
            <h5>2015, Volume 36</h5>
         </div>
         <div class="essayHeader">
            <h1 class="essaysTitle">Essays</h1>
         </div><span class="essayNav">
            <ul>
               <li id="essayNav1"><a href="https://archive.scholarlyediting.org/se-archive/2015/pdf/essay.gordon.pdf"><img src="https://archive.scholarlyediting.org/se-archive/template_images/pdf.png" alt="PDF icon">&nbsp;PDF</a></li>
            </ul></span><div class="essay">
            
            
            
            <div class="frontmatter">
               
               <h1>Editing Death</h1>
               
               <h2>An Element of Craft</h2>
               <span class="byline">Ann D. Gordon</span>
               
            </div>
            
            
            <p>When it came time to design the sixth and final volume of the <em>Selected Papers of Elizabeth Cady Stanton and Susan B. Anthony</em>, I realized
               that I knew nothing at all about how editors handled the deaths of their principal
               subjects and concluded their editions. I had never been on an editorial team at the
               subject’s moment of death; moreover, I had not encountered talks or essays by
               colleagues about editing death. With two deaths upcoming in the volume, I needed
               models for the end of life in editions of a person’s papers. The whole staff set out
               to survey the field. Quite unexpectedly, a query about death opened windows onto
               editorial styles. In their handling of death, editors can only create an ending from
               finite and disparate compilations of sources. But through their varied designs, as
               we read the work, editors manifested decisions about the arts of narrative, claims
               about the significance of their subject’s life, and emotions about ending a long
               partnership and collaboration. This is an attempt to start the conversation we once
               needed.<a href="#note1" name="nr1"><span class="noteRef">[1] </span></a></p>
            
            <p>Take a case in point. Elizabeth Cady Stanton died on October 26, 1902, in her
               apartment on Ninety-Fourth Street in New York City. In her last surviving private
               letter, dated September 30, Stanton proposed a long-range plan: “As I was wide awake
               last night for hours, when I should have been asleep, I thought of <u>you</u>,” she told the journalist Ida Harper, as the person
               best qualified to “give the finishing touch” to a volume of her speeches. “Now tell
               me,” she wrote, “if you think you will be able to edit my book.”<a href="#note2" name="nr2"><span class="noteRef">[2] </span></a> Though Stanton, approaching her eighty-seventh birthday,
               found it extremely difficult to move around and could no longer see, there were few
               indications to people outside her family that her end was nigh. In the public
               sphere, readers of William Randolph Hearst’s <em>New York American and
                  Journal</em> found Stanton’s byline on articles published in July, August,
               September, and even on October 13.<a href="#note3" name="nr3"><span class="noteRef">[3] </span></a> In other
               words, narrative in Stanton’s own documents runs against the current that carries an
               omniscient editor to the conclusion.
            </p>
            
            <p>Had I edited books comprised solely of Stanton’s papers, these would be sources from
               which to construct an ending to my book and Stanton’s life. At the September 30
               missive to Ida Harper, I would wrestle with whether to foreshadow death by noting
               that it was the last known private letter. At the October 13 newspaper article, the
               question of when she wrote it would loom, if it were to be my book’s final text.
               Then what? How would Stanton die? Some editors introduce their subject’s last will
               and testament at this point. Though surely written earlier than its placement
               indicates, the will at its moment of legal activation signifies its author’s
               passing. My sources fail me: Stanton left no will that might become the final text.
               At that point, many editors step forward either to recount the death themselves or
               to introduce historical witnesses. Stanton could die in a footnote, maybe anchored
               awkwardly on the article of October 13: “in thirteen days, she would be dead.” She
               could die in an editorial note that stood apart from the texts. She got married that
               way: in the surviving texts of 1840 she broke off her engagement and then sailed on
               her honeymoon. Editors bridged that gap and made it legal. Here at the end of life,
               when Stanton’s papers omit her life’s turns and endings, the editor might step in to
               put finis to the narrative. As a matter of fact, in a solo edition of Stanton’s
               papers, death and texts cannot be made to reach a common end point. Stanton wrote so
               fast and furiously in 1902 that her papers flowed on seamlessly beyond her death. If
               Stanton’s papers were allowed to run their course—fall into their chronological
               slots until the supply ran out—her death would disappear into an inflexible
               progression of papers. Something should be said.
            </p>
            
            <p>At the Stanton and Anthony Papers, we decided to take our questions about editing
               death to a list of completed historical editions. Staff members asked, what is the
               final document and why? How does a reader learn that the subject died and when death
               arrived? Are the circumstances of the death regarded as worthy of description? Are
               the boundaries of what constitute the subject’s “papers” expanded for the occasion
               in order to incorporate new points of view, through memorials, condolences, and
               obituaries? They looked at some two dozen editions, both selective and comprehensive
               in scope. Though it was possible to draw up clear descriptions of what the editors
               put into print, we could only infer what governed their choices.
            </p>
            
            <p>From this informal survey, they discovered that readers learn about deaths in a
               variety of ways, sometimes by more than one way in a single volume. It might be that
               the date and cause of death are given in an introductory essay. If the device of a
               chronology is employed, death usually makes its appearance in that list. If the
               volume is conceived as a series of chapters each opened with a preface, death may be
               foreshadowed at the final chapter’s start. What seems the most obvious way—to face
               the fact of death in the book’s core of texts and annotation—is not the most popular
               way. By and large, editors seem to cluster near one of two stylistic poles. On the
               one hand, they end their volumes with the last letter or memorandum or other paper
               of the subject, no matter how long before (or after) its author’s death; on the
               other hand, they admit new witnesses—a wife or daughter, an aide, an official’s
               announcement, a newspaper’s obituary—through which to narrate the final weeks or
               days or moments.
            </p>
            
            <p>At one pole, to pick but a single example, the staff observed that the two-volume <em>Selected Letters of Charles Sumner</em> stops with a letter dated
               March 9, 1874, without explanation. Is this the last known letter by Sumner or the
               last one selected by the editor? How much time passed before Sumner died? In fact,
               the date of Sumner’s death (March 11) is nowhere reported in the volume where texts
               of 1874 appear.<a href="#note4" name="nr4"><span class="noteRef">[4] </span></a> At an extreme of this
               style, not even the editor bears witness to death. At the opposite pole, consider
               the <em>Letters of Eugene V. Debs</em>. The final book of this three
               volume edition winds down slowly. The last letter by Debs is dated June 3, 1926,
               four and a half months before he died. Rather than stop there, the editors selected
               a series of letters written over the summer by well-wishers and followed them with
               letters of condolence to Eugene’s brother Theodore Debs. With the first letter of
               condolence, a footnote places Eugene’s death at October 20, 1926.<a href="#note5" name="nr5"><span class="noteRef">[5] </span></a> In the middle ground, one could look at <em>The Selected Letters of Louisa May Alcott</em>. The last selection
               is also Alcott’s last text, written on the morning of her fatal stroke, facts the
               reader learns in a concise footnote.<a href="#note6" name="nr6"><span class="noteRef">[6] </span></a></p>
            
            <p>Narrative is a word of variable stature and fashion among historians but kind of
               basic to the chronological scholarship of texts and documents and papers. Most
               editors are quite clear that they are not <em>the</em> narrator of
               their edition but rather work in collaboration with the principal subject of their
               work. The editor’s narrative is more akin to the biographer’s than to the
               storyteller’s.<a href="#note7" name="nr7"><span class="noteRef">[7] </span></a> There is, first, the
               objective limitation of available material, though that limit is not hard and fast,
               especially in a selective edition or one that admits witnesses. As the <em>Papers of Joseph Henry</em> draws to a close and the last of his
               letters is placed in the volume, the editors add a text so perfect for the purpose
               that any editor will envy their find. The scientist and college professor Maria
               Mitchell visits Henry in the last weeks of his life and records in her journal their
               conversation about facing death. Their book then closes with a single letter between
               two of Henry’s friends that efficiently recounts Henry’s final days and announces
               plans for a funeral.<a href="#note8" name="nr8"><span class="noteRef">[8] </span></a></p>
            
            <p>Another influence on an editor’s style of narration emerges from the professional but
               subjective measure of the importance of the life edited. It will surprise no one to
               learn that Arthur S. Link edited death on the grandest scale when he reached the
               conclusion of the <em>Papers of Woodrow Wilson</em>. The ending is
               consistent with Link’s passionate loyalty to Woodrow Wilson, and it suggests ways
               that rational measures of importance become entangled in the difficult emotions of
               parting company with one’s life work. After all, Wilson dies in the sixty-eighth
               volume, every one of them edited by Link. The former president dictated letters on
               January 25, 1924, and a secretary noted on the carbon copy (and the editors quoted
               in a footnote) that the text was one of the last letters dictated by a man too ill
               to sign the sent copies. The secretary’s notation, augmented perhaps by the exact
               date of death, would conclude many an edition in a concise and graceful way. But
               Link and his team were not content to conclude their work at that anticlimactic
               point. They admit several witnesses and include daily bulletins about Wilson’s
               decline that were sent out to the public until he died on February 3. Befitting the
               importance of the occasion, the story extends to selected letters about the funeral,
               to indicate that President Coolidge will attend and that Wilson’s widow instructed
               Henry Cabot Lodge to stay away.<a href="#note9" name="nr9"><span class="noteRef">[9] </span></a></p>
            
            <p>Because the <em>Selected Papers of Stanton and Anthony</em> is a joint
               edition of two friends, one woman was bound to witness the death of the other. As
               things turned out, Susan B. Anthony became the resident witness to the death of her
               friend. Through the papers of Anthony, Stanton’s decline and death are documented as
               a matter of course, as a disturbing event in her own life, without editorial
               intervention or quests for external witnesses. The edition’s narrative style stays
               intact. A letter to Anthony from Stanton’s daughter Harriot Blatch in September 1902
               tempers the hopeful note of Stanton’s own letter in the same week. After describing
               her mother as greatly weakened and in constant need of a daughter’s attention,
               Harriot urges Anthony to visit New York City for Stanton’s birthday in November, “as
               I’m sure there wont be another.”<a href="#note10" name="nr10"><span class="noteRef">[10] </span></a> Readers
               of the <em>Selected Papers</em> learn of Elizabeth Cady Stanton’s
               death in the same way Susan B. Anthony did, by telegram from Harriot Blatch: “Mother
               passed away today.”<a href="#note11" name="nr11"><span class="noteRef">[11] </span></a> With her arrival in
               New York City on October 27, Anthony becomes the witness to the family’s grief, the
               private funeral in their apartment, and a larger ceremony at Woodlawn Cemetery. As
               an editor, I could not possibly improve upon the witness Anthony became. On the day
               of the funeral, she described her loss in terms of science and human reason. 
               <blockquote>Well, it is an awful hush—it seems impossible—that the voice is
                  hushed—that I have longed to hear for 50 years—longed to get her opinion of
                  things—before I knew exactly where I stood— It is all at sea—but the Laws of
                  Nature are still going on—with no shadow or turning— What a world it is—it goes
                  right on &amp; on—no matter who lives or who dies!!
               </blockquote>In the same letter, she
               also directs readers to aspects of the public and political response to Stanton’s
               death, critiquing how poorly journalists understood Stanton’s cause and marveling,
               after telegrams arrived from England, that “The whole world knows of the fact!”<a href="#note12" name="nr12"><span class="noteRef">[12] </span></a></p>
            
            <p>But what happens when the <em>Selected Papers</em> reaches the moment
               of Susan B. Anthony’s death on March 13, 1906, at her home on Madison Street in
               Rochester? In this death, the more usual laws of editing apply, and the papers of
               Susan B. Anthony, when narrowly defined, offer nothing to document her death. For
               more than a month, her decline was national news. She reached Baltimore on February
               4 with a terrible cold, and reports of her ill health were incorporated into daily
               coverage of the annual convention of the National-American Woman Suffrage
               Association there. In the care of a private nurse, she managed a trip to Washington
               on February 15 and sat on the stage for her eighty-sixth birthday celebration; a few
               words uttered on that occasion bring an end to her papers.<a href="#note13" name="nr13"><span class="noteRef">[13] </span></a> Traveling north toward home, with a nurse still at her
               side, Anthony broke an engagement to celebrate her birthday again with suffragists
               in New York City. That too made the news. Susan B. Anthony’s silence after leaving
               Washington caught the attention of the national press corps and a crowd of reporters
               gathered on Madison Street. Daily briefings for them in front of her house tracked
               her decline, as these headlines illustrate. 
               <blockquote><em>Pawtucket Times</em>, March
                  6: “Susan B. Anthony May Not Recover”
               </blockquote>
               
               <blockquote><em>Dallas Morning News</em>, March 7: “Miss Susan B. Anthony Ill”
               </blockquote>
               
               <blockquote><em>Albuquerque Journal</em>, March 11: “Susan B. Anthony Worse”
               </blockquote> From
               inside the house, a niece kept close friends apprised of her condition in neatly
               typed letters. In the <em>Selected Papers</em>, we admitted new
               witnesses during Anthony’s month of silent decline. Except insofar as we had
               admitted journalists’ accounts of their speeches and interviews, the concluding
               pages of the sixth volume depart from a strict commitment to using only words sent
               to or authored by Stanton and Anthony. Having decided to open that door, it was a
               matter of selecting texts by women who had access to Anthony’s bedroom, who may have
               written for an effect of their own but who had firsthand information. Two letters by
               the niece trace false hopes of improvement and reveal how family members edited the
               news allowed to reach Anthony. Those are followed by her doctor’s statement to the
               press on March 6, 1906.<a href="#note14" name="nr14"><span class="noteRef">[14] </span></a> And for the
               moment of death, we had an unusual source. In 1981, when the project announced its
               search for the papers of Stanton and Anthony in <em>Yankee
                  Magazine</em>, a family sent us a letter from a young nurse named Mabel Nichols
               who trained in Massachusetts. While in Rochester visiting friends at the nursing
               school, Nichols was hired as Susan B. Anthony’s night nurse. On the morning of her
               patient’s death, she described the events for her sister back home, ending her
               letter (and our volume) with disputed claims for proximity to the dead. “The paper
               gives the names of the parties that were at the death bed, but to tell the truth
               Maude I was all alone with the dear old soul.”<a href="#note15" name="nr15"><span class="noteRef">[15] </span></a></p>
            
            <p>When I first wrote a short piece about editing death for the <em>Project Newsletter</em> of the Stanton and Anthony Papers, one editor sent me a
               defense of “the abrupt end to the letters” at an edition’s conclusion, meaning to
               end an edition without reference to the fact of death. I inferred that because Clara
               Barton (let’s pretend we speak of her) makes no mention of her own death, the style
               does not permit Barton’s editor to step in with information deemed extraneous to the
               text of each letter or other document. The kind of narrative that frames a life—her
               birth, her death—is segregated into editorial apparatus in essays, chronologies, and
               chapter headings. It is as if Clara Barton wrote one narrative in a life of letters
               and her editor wrote another. Such a bifurcation assumes an impossible distance
               between editor and subject, imagines that an editorial voice is not expressed in the
               arrangement of Clara Barton’s narrative. The editor’s hand and voice is all over the
               editions, working in collaboration with the subject, whether overtly or not. An
               intervention to let the subject die in train seems no more than to extend a slight
               courtesy.
            </p>
            
            <p>While I contemplated how to edit the deaths of Stanton and Anthony in ways consistent
               with the edition, I forgot to consult <em>The Letters of Virginia
                  Woolf</em>, though the six volumes occupy a shelf at eye level near my most
               comfortable chair. Woolf’s dramatic death introduces new considerations. As a
               textual artifact, the suicide note overturns my original premise, that a person’s
               papers are pretty useless for documenting his or her own death. Such a note brings
               the reader closer to the end of the author’s life than whatever random letter or
               note gets the editor’s attention for an ending. In the nearly thirty years since I
               last read the final volume of the <em>Letters</em>, I forgot how
               tightly the narratives of Woolf and her editors are woven at the end. It happens
               that in the papers of Virginia Woolf, there is not one suicide note but three. For
               Nigel Nicolson and Joanne Trautmann, editing death entailed careful textual
               scholarship.<a href="#note16" name="nr16"><span class="noteRef">[16] </span></a></p>
            
            <p>Virginia Woolf had written two notes to her husband, Leonard, and one to her sister,
               Vanessa Bell. None was dated, though two of them indicated (different) days of the
               week. Two different tablets of paper had been used. The family—her husband Leonard
               and Quentin Bell, Virginia’s nephew and biographer—settled on a chronology and
               explanation for the multiple notes that were all found at once. Her editors
               questioned the chronology, assigned three different tentative dates to the notes,
               and in their words “dat[ed] the stages by which she reached her decision” (491). If
               they are correct (and I do not pretend to evaluate their work), their chronology
               introduces evidence of a failed suicide. In the simple act of editing death, the
               editors potentially revised the narrative of death, that most certain of things.
            </p>
            
            
            
            <div class="notesList">
               <h2>Notes</h2>
               
               <div class="note"><a name="note1"></a>1.&nbsp;Ann D. Gordon, Michael David Cohen, Sara Rzeszutek Haviland,
                  Andy Bowers, and Katharine Lee, eds., <em>Selected Papers of
                     Elizabeth Cady Stanton and Susan B. Anthony</em>, vol. 6, <em>An Awful Hush, 1895 to 1906</em> (New Brunswick, NJ: Rutgers University
                  Press, 2013). Kathleen E. Manning was also part of the team during this
                  exercise.<a href="#nr1"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note2"></a>2.&nbsp;Gordon, <em>An Awful Hush, 1895 to 1906</em>,
                  435–37.<a href="#nr2"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note3"></a>3.&nbsp;For her articles of April 20 and October 13, see Gordon, <em>An Awful Hush, 1895 to 1906</em>, 430–32, 441–46. For all
                  articles published in 1902, see Patricia G. Holland and Ann D. Gordon, eds., <em>Papers of Elizabeth Cady Stanton and Susan B. Anthony</em>
                  (Wilmington, DE: Scholarly Resources Inc., 1991, microfilm).<a href="#nr3"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note4"></a>4.&nbsp;Beverly Wilson Palmer, ed., <em>The Selected
                     Letters of Charles Sumner</em>, 2 vols. (Boston: Northeastern University
                  Press, 1990), 2:631.<a href="#nr4"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note5"></a>5.&nbsp;J. Robert Constantine, ed., <em>Letters of Eugene
                     V. Debs</em>, vol. 3, <em>1919–1926</em> (Urbana: University
                  of Illinois Press, 1990), 581–82, 601.<a href="#nr5"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note6"></a>6.&nbsp;Joel Myerson and Daniel Shealy, eds., <em>The
                     Selected Letters of Louisa May Alcott</em> (Boston: Little Brown and
                  Company, 1987), 337.<a href="#nr6"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note7"></a>7.&nbsp;For ideas about biographical narration, see Leon Edel, <em>Literary Biography</em> (1959; reprint, Bloomington: Indiana
                  University Press, 1973), especially chap. 5, “Time.”<a href="#nr7"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note8"></a>8.&nbsp;Marc Rothenberg et al., eds., <em>The Papers of
                     Joseph Henry</em>, vol. 11, <em>January 1866–May 1878, The
                     Smithsonian Years</em> (Washington, DC: Smithsonian Institution, 2007),
                  653–55.<a href="#nr8"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note9"></a>9.&nbsp;Arthur S. Link, John E. Little, and L. Kathleen Amon, eds., <em>The Papers of Woodrow Wilson</em>, vol. 68, <em>April 8, 1922–February 6, 1924</em> (Princeton, NJ: Princeton University
                  Press, 1993), 547ff.<a href="#nr9"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note10"></a>10.&nbsp;September 25, 1902, Gordon, <em>An Awful Hush,
                     1895 to 1906</em>, 434–35.<a href="#nr10"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note11"></a>11.&nbsp;October 26, 1902, Gordon, <em>An Awful Hush,
                     1895 to 1906</em>, 452.<a href="#nr11"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note12"></a>12.&nbsp;To Ida H. Harper, October 28, 1902, in Gordon, <em>An Awful Hush, 1895 to 1906</em>, 455–56.<a href="#nr12"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note13"></a>13.&nbsp;Gordon, <em>An Awful Hush, 1895 to 1906</em>,
                  xxix–xxx, 577–78.<a href="#nr13"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note14"></a>14.&nbsp;Lucy E. Anthony to Ida H. Harper, February 28, 1906, and to
                  Elizabeth Smith Miller, March 2, 1906, and Statement by Dr. Marcena Sherman
                  Ricker, March 6, 1906, all in Gordon, <em>An Awful Hush, 1895 to
                     1906</em>, 579–81.<a href="#nr14"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note15"></a>15.&nbsp;Mabel Nichols to Maude Nichols, March 13, 1906, in Gordon, <em>An Awful Hush, 1895 to 1906</em>, 581–82.<a href="#nr15"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
               <div class="note"><a name="note16"></a>16.&nbsp;“The Dating of Virginia Woolf’s Last Letters,” appendix A in
                  <em>The Letters of Virginia Woolf</em>, ed. Nigel Nicolson and
                  Joanne Trautmann, vol. 6, <em>1936–1941</em> (New York: Harcourt
                  Brace Jovanovich, 1980), 489–91.<a href="#nr16"><img src="https://archive.scholarlyediting.org/se-archive/2015/essays/template_images/goback.png" alt="Go back"></a></div>
               
            </div>
            
            
            
         </div>
         <div class="footer"><span class="creativecommons"><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/88x31.png"></a><br>This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">Creative
                  Commons Attribution-NonCommercial-ShareAlike 3.0 Unported
                  License</a>.</span><span class="adelogo"><a href="http://www.documentaryediting.org"><img src="https://archive.scholarlyediting.org/se-archive/template_images/adelogo.png" alt="Logo of the Association for Documentary Editing"></a></span><span class="cdrh">Sponsored by the <a href="http://cdrh.unl.edu">Center for
                  Digital Research in the Humanities at the University of
                  Nebraska-Lincoln</a>.</span><span class="issn">ISSN 2167-1257</span></div>
      </div>
   <script>
  
  function gaOptout(){document.cookie=disableStr+'=true; expires=Thu, 31 Dec 2099 23:59:59 UTC;path=/',window[disableStr]=!0}var gaProperty='UA-174640285-1',disableStr='ga-disable-'+gaProperty;document.cookie.indexOf(disableStr+'=true')>-1&&(window[disableStr]=!0);
  if(true) {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  }
  if (typeof ga === "function") {
    ga('create', 'UA-174640285-1', 'auto', {});
      ga('set', 'anonymizeIp', true);
      
      
      
      
      }</script></body>
</html>